module.exports = function (sequelize, DataTypes) {
  return sequelize.define('agencyHost', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    dreamlive_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    agency_name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    agency_code: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    contact: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    address: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    city: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    state: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    country: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    national_id: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    id_type: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    payment_type: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    nation_id_pic_front: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    national_id_pic_back: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    selfie_pic: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    selfie_national_pic: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    payment_method: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'agency_host',
    timestamps: false,
    indexes: [{
      name: 'index',
      using: 'BTREE',
      fields: [
        { name: 'id' }
      ]
    },
    {
      name: 'dreamlive_id',
      using: 'BTREE',
      fields: [
        { name: 'dreamlive_id' }
      ]
    }
    ]
  })
}
