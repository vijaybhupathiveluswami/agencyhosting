module.exports = function (sequelize, DataTypes) {
  return sequelize.define('agency', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true
    },
    dreamlive_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    agent_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    agency_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    agency_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    contact: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    state: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    country: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    pincode: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    adhaar_number: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    pan_number: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    id_type: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    agency_pic: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nation_id_pic_back: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    national_id_pic: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    selfie_pic: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    selfie_national_pic: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'agency',
    timestamps: false,
    indexes: [{
      name: 'PRIMARY',
      unique: true,
      using: 'BTREE',
      fields: [
        { name: 'dreamlive_id' }
      ]
    },
    {
      name: 'Index',
      using: 'BTREE',
      fields: [
        { name: 'id' }
      ]
    }
    ]
  })
}
