const DataTypes = require('sequelize').DataTypes
const _agency = require('./agency')
const _agencyHost = require('./agency_host')
const _role = require('./role')

function initModels () {
  const sequelize = require('../config/db')

  const agency = _agency(sequelize, DataTypes)
  const agencyHost = _agencyHost(sequelize, DataTypes)
  const role = _role(sequelize, DataTypes)

  return {
    agency,
    agencyHost,
    role
  }
}
module.exports = initModels
module.exports.initModels = initModels
module.exports.default = initModels
