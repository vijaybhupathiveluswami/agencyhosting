require('dotenv').config()
const path = require('path')
const fastify = require('fastify')({
  logger: true
  // http2: true,
//  rewriteUrl (req) {
//    return req.url.replace('/dreamlivecms', '')
//  }
})

const Sentry = require('@sentry/node')
// const Tracing = require('@sentry/tracing')
Sentry.init({
  dsn: 'https://285e6ff38a9746d5ac753af437fe0865@o949515.ingest.sentry.io/5899010',

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0
})
const transaction = Sentry.startTransaction({
  op: 'test',
  name: 'My First Test Transaction'
})

fastify.register(require('fastify-static'), {
  root: path.join(__dirname, 'public'),
  prefix: '/public/' // optional: default '/'
})

fastify.register(require('fastify-redis'), {
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
  password: process.env.REDIS_AUTH
})
fastify.register(require('fastify-cookie'), {})

fastify.register(require('point-of-view'), {
  engine: {
    handlebars: require('handlebars')
  },
  layout: '/layouts/main',
  templates: 'views/',
  includeViewExtension: true,
  options: {
    partials: {
      // head: '/views/partials/head.hbs',
      topMenu: '/partials/topMenu.hbs',
      sideMenu: '/partials/sideMenu.hbs'
    }
  }
})

require('./views/helpers/helpers')

require('dotenv')

const AutoLoad = require('fastify-autoload')

fastify.register(require('fastify-cors'), {
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
})

fastify.register(require('fastify-formbody'))
fastify.register(require('fastify-multipart'), {
  attachFieldsToBody: true,
  addToBody: true
})

const metricsPlugin = require('fastify-metrics')

fastify.register(metricsPlugin, { endpoint: '/metrics' })

fastify.register(AutoLoad, {
  dir: path.join(__dirname, '/routes/v1/'),
  dirNameRoutePrefix: '/api/v1'
})
// fastify.register(require('@now-ims/fastify-firebase'), {
//   name: process.env.FIREBASENAME,
//   cert: process.env.GOOGLE_APPLICATION_CREDENTIALS,
//   storageBucket: process.env.FIREBASEPACKAGE // 'dreamlivestg-1b40c'
// })
fastify.register(require('@now-ims/fastify-firebase'), {
  name: 'stgFirebase',
  cert: './common/firebase_json/firebaseJson.json',
  storageBucket: 'com.topclass.audiolive.stg' // 'dreamlivestg-1b40c'
})

fastify.register(require('fastify-swagger'), {
  routePrefix: '/dreamlive',
  swagger: {
    info: {
      title: 'Dream Live CMS',
      description: 'Dream Live CMS ',
      version: '1.0.0'
    },
    host: process.env.SWAGARHOST + ':' + process.env.SERVER_PORT,
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
    // tags: [
    //   { name: 'user', description: 'User related end-points' },
    //   { name: 'code', description: 'Code related end-points' }
    // ],
    // definitions: {
    //   User: {
    //     type: 'object',
    //     required: ['id', 'email'],
    //     properties: {
    //       id: { type: 'string', format: 'uuid' },
    //       firstName: { type: 'string' },
    //       lastName: { type: 'string' },
    //       email: { type: 'string', format: 'email' }
    //     }
    //   }
    // },
    securityDefinitions: {
      basicAuth: {
        type: 'basic',
        name: 'Basic Auth',
        in: 'header'
      }
    }
  },
  uiConfig: {
    docExpansion: 'full',
    deepLinking: false
  },
  staticCSP: true,
  transformStaticCSP: (header) => header,
  exposeRoute: true
})

fastify.register(require('fastify-curl'), {
  httpclient: {
    request: {
      timeout: 5000
    },
    httpAgent: {
      keepAlive: true,
      freeSocketKeepAliveTimeout: 4000,
      maxSockets: 9007199254740991,
      maxFreeSockets: 256
    },
    httpsAgent: {
      keepAlive: true,
      freeSocketKeepAliveTimeout: 4000,
      maxSockets: 9007199254740991,
      maxFreeSockets: 256
    }
  }
})
// Database
// const db = require('./config/db')

// // Test DB
// db.authenticate()
//   .then(() => console.log('Database connected...'))
//   .catch(err => console.log('Error: ' + err))
// fastify.use(bodyParser.urlencoded({ extended: true }));
const start = async () => {
  try {
    await fastify.listen(process.env.SERVER_PORT)
  } catch (error) {
    Sentry.captureException(error)
    fastify.log.error(error)
    console.log(error)
  } finally {
    transaction.finish()
  }
}

start()
