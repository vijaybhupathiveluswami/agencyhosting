const { Sequelize } = require('sequelize')
module.exports = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  dialect: 'mysql',
  port: process.env.DB_PORT,
  operatorsAliases: 1,
  define: {
    timestamps: false
  },
  // replication: {
  //     read: [
  //         { host: process.env.DB_SLAVE_HOST }
  //     ],
  //     write: { host: process.env.DB_HOST }
  // },
  pool: {
    max: 20,
    min: 0,
    acquire: 30000,
    idle: 30000
  },
  dialectOptions: {
    dateStrings: true,
    typeCast: true
  },
  timezone: '+05:30'
})
