const admin = require('firebase-admin')
const serviceAccount = require('../common/targetcoins.json')
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
}, 'agency')
module.exports.admin = admin
