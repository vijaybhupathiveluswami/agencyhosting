const handlebars = require('handlebars')
require('handlebars-helpers')({
  handlebars: handlebars
})

handlebars.registerHelper('switch', function (value, options) {
  this.switch_value = value
  return options.fn(this)
})

handlebars.registerHelper('case', function (value, options) {
  if (value === this.switch_value) {
    return options.fn(this)
  }
})

handlebars.registerHelper('toJSON', function (value, options) {
  return JSON.stringify(value)
})

handlebars.registerHelper('inc1', function (value, options) {
  return parseInt(value) + 1
})
handlebars.registerHelper('ifeq', function (a, b, options) {
  console.log(a)
  console.log(b)
  if (a === b) {
    return options.fn(this)
  } else {
    return options.inverse(this)
  }
})

handlebars.registerHelper('ifneq', function (a, b, options) {
  console.log(a)
  console.log(b)
  if (a !== b) {
    return options.fn(this)
  } else {
    return options.inverse(this)
  }
})
