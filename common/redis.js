const { redisStoreUserInfo } = require('./user_info_store')
const { redisStoreRoomInfo } = require('./room_info_store')
const { modelExcute } = require('./model_excute')
// get all redis data
const getAllData = async function (fastify, key, index) {
  const { redis } = fastify
  await redis.select(index)
  // return redis data
  return await redis.hgetall(key)
}
// get only user data to check
const getUserData = async function (fastify, key, headers) {
  const { redis } = fastify
  await redis.select(1)
  // user data to check redis exit or not
  if (!await redis.exists(key)) {
    const myData = (await modelExcute({}, 'select', 'users', { user_id: key })).data
    console.log(myData)
    if (myData.length > 0) {
      await redisStoreUserInfo(fastify, myData[0], key, headers)
    }
  }
  return await getAllData(fastify, key, 1)
}
// get only user data to check
const updateUserData = async function (fastify, key, headers) {
  const { redis } = fastify
  await redis.select(1)
  const myData = (await modelExcute({}, 'select', 'users', { user_id: key })).data
  console.log(myData)
  if (myData.length > 0) {
    await redisStoreUserInfo(fastify, myData[0], key, headers)
  }
  return await getAllData(fastify, key, 1)
}
// get only room data to check
const getRoomData = async function (fastify, key, headers) {
  const { redis } = fastify
  await redis.select(2)
  // room data to check redis exit or not
  if (!await redis.exists('room-' + key)) {
    const roomData = (await modelExcute({}, 'select', 'room', { room_id: key })).data
    if (roomData.length > 0) {
      await redisStoreRoomInfo(fastify, roomData[0], key, headers)
    }
  }
  return await getAllData(fastify, 'room-' + key, 2)
}
// get gift list in redis check and not yet then store to redis
const getGiftData = async function (fastify) {
  const { redis } = fastify
  await redis.select(3)
  // check the redis
  if (!await redis.exists('giftList')) {
    const giftData = (await modelExcute({}, 'select', 'gift_list', {}, [], [
      ['price', 'DESC']
    ])).data
    giftData.forEach(element => {
      const tempGift = {
        id: element.id,
        url: element.url,
        audio: element.audio,
        type: element.type,
        playType: element.playType,
        status: element.status,
        price: element.price,
        name: element.name,
        icon: element.icon,
        combo_flag: element.combo_flag,
        combo_packs: element.combo_packs,
        giftUpdate: element.giftUpdate
      }
      const id = element.id
      const tempList = {}
      tempList[id] = JSON.stringify(tempGift)
      setData(fastify, 'giftList', tempList, 86400, 3)
    })
  }
  return await getAllData(fastify, 'giftList', 3)
}
const getData = async function (fastify, key, key1, index) {
  const { redis } = fastify
  await redis.select(index)
  return await redis.hget(key, key1)
}
const scanData = async function (fastify, searchKey, index, key, key1, key2) {
  const { redis } = fastify
  await redis.select(index)
  const match = ['match', '*' + key + '*' + key1 + '*' + key2 + '*', 'count', 3]
  return await redis.hscan(searchKey, 0, match)
}
const setData = async function (fastify, key, data, expireTime, index) {
  const { redis } = fastify
  await redis.select(index)
  for (const insideKey in data) {
    if (Object.hasOwnProperty.call(data, insideKey)) {
      const element = data[insideKey]
      await redis.hset(key, insideKey, element)
    }
  }
  if (expireTime !== 0) { await redis.expire(key, expireTime) }
}
module.exports = { getAllData, getData, setData, getUserData, getRoomData, scanData, getGiftData, updateUserData }
