const { modelExcute } = require('./model_excute')

const redisStoreRoomInfo = async function (fastify, data, roomId, headers) {
  if (data) {
    const { setData } = require('./redis')
    const { Op } = require('sequelize')
    const userAbuseData = (await modelExcute({}, 'select', 'user_abuse', {
      room_id: roomId,
      type: {
        [Op.or]: ['text', 'kick']
      }
    })).data
    const kickList = []
    const textList = []
    data.textList = ''
    data.kickList = ''
    for (const element of userAbuseData) {
      if (element.type === 'text') { textList.push(element.block_id) }
      if (element.type === 'kick') { kickList.push(element.block_id) }
    }
    if (textList.length > 0) { data.textList = textList.join() }
    if (kickList.length > 0) { data.kickList = kickList.join() }
    await setData(fastify, 'room-' + roomId, data, 3600, 2)
  }
}
module.exports = { redisStoreRoomInfo }
