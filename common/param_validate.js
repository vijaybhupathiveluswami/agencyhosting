const paramValidate = async function (param) {
  let message = 'valid data'
  if (Object.hasOwnProperty.call(param, 'check')) {
    for (const checkValue of param.check) {
      for (const key in param) {
        if (Object.hasOwnProperty.call(param, key)) {
          const value = param[key]
          if (key !== 'check') {
            if ((value === '' || value === null || value === undefined) && key !== checkValue) {
              message = 'Incorrect Data'
            }
          }
        }
      }
    }
  } else {
    for (const value of param) {
      if (value === '' || value === null || value === undefined) {
        message = 'Incorrect Data'
      }
    }
  }
  return message
}
module.exports = { paramValidate }
