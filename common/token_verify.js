const { getAllData, updateUserData } = require('./redis')
const sessionVerify = async function (fastify, auth, token, headers) {
  const redisVerify = await getAllData(fastify, token, 3)
  const response = {}
  console.log('redisVerify')
  console.log(redisVerify)
  response.userId = token
  let tempCall = 0
  if (redisVerify) {
    if (Object.hasOwnProperty.call(redisVerify, 'activation_code')) {
      if (redisVerify.activation_code === auth) {
        response.userData = redisVerify
      } else {
        tempCall = 1
      }
    } else { tempCall = 1 }
  }
  if (tempCall === 1) {
    const myData = await updateUserData(fastify, token, headers)
    if (myData) {
      response.userData = myData
    } else {
      response.message = 'Not authorized'
    }
  }
  if (response.message === 'valid data') {
    const { redis } = fastify
    await redis.select(3)
    await redis.expire(token, 3600)
  }
  return response
}
module.exports = { sessionVerify }
