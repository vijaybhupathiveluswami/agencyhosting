const { s3bucketBuffer } = require('./s3bucket_buffer')
const moment = require('moment')

const webp = async function (Path, Id, url, urlPath) {
  const urlg = await s3bucketBuffer(Path, Id, url, urlPath)
  console.log('((((((((((((((((((((((((((((((((((((url)))))))))))))))))))))))))))))')
  console.log(urlg.Location)
  console.log('((((((((((((((((((((((((((((((((((((url)))))))))))))))))))))))))))))')
  const timeStamp = moment().valueOf()
  if (process.env.DB_HOST === 'live.dreamlive.co.in') {
    // locationUrl = url.Location
    const locationUrl = 'https://d338yng2n0d2es.cloudfront.net/family/image/' + Id + '-' + timeStamp + '.webp'
    return locationUrl
  }
  return urlg.Location
}

module.exports = { webp }
