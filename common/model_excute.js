const modelExcute = async function (data, type, table, condition, attributes, order, group, limit, offset, onelyOne, include, raw, nested) {
  let response = {}
  try {
    console.log(table)
    const object = await modelObjectGet(table)
    response.data = null
    response.message = ''
    console.log(type)
    console.log(condition)
    console.log(data)
    console.log(order)
    console.log(group)
    console.log(attributes)
    console.log(limit)
    console.log(offset)
    console.log(onelyOne)
    console.log(include)
    const contirionJson = { raw: typeof raw === 'undefined' ? true : raw, nest: typeof nested === 'undefined' ? true : nested }
    switch (type) {
      case 'update':
        response.message = 'Updated successfully'
        await object.update(data, { where: condition })
        break
      case 'delete':
        response.message = 'Deleted successfully'
        await object.destroy({ where: condition })
        break
      case 'insert':
        response.message = 'Inserted successfully'
        response.data = (await object.create(data))
        break
      case 'insertUpdate':
        response.message = 'successfully'
        response.data = await upsert(data, condition, object)
        break

      case 'select':
        response.message = 'Select successfully'
        if (Object.keys(condition).length > 0) {
          contirionJson.where = condition
        }
        if (typeof attributes !== 'undefined' && attributes !== '') {
          if (attributes.length > 0) {
            contirionJson.attributes = attributes
          }
        }
        if (typeof order !== 'undefined' && order !== '') {
          if (order.length > 0) {
            contirionJson.order = order
          }
        }
        if (typeof group !== 'undefined' && group !== '') {
          if (group.length > 0) {
            contirionJson.group = group
          }
        }
        if (typeof limit !== 'undefined' && limit !== '') {
          if (limit.toString().length > 0) {
            contirionJson.limit = limit
          }
        }
        if (typeof offset !== 'undefined' && offset !== '') {
          if (offset.toString().length > 0) {
            contirionJson.offset = offset
          }
        }
        if (typeof include !== 'undefined' && include !== '') {
          if (include.length > 0) {
            for (const iterator of include) {
              iterator.model = await modelObjectGet(iterator.model)
              if (Object.hasOwnProperty.call(iterator, 'include')) {
                for (const iterator1 of iterator.include) {
                  iterator1.model = await modelObjectGet(iterator1.model)
                  if (Object.hasOwnProperty.call(iterator, 'include')) {
                    console.log(iterator.include)
                    for (const iterator2 of iterator1.include) {
                      iterator2.model = await modelObjectGet(iterator2.model)
                    }
                  }
                }
              }
            }
            contirionJson.include = include
          }
        }

        console.log(contirionJson)
        if ((typeof onelyOne !== 'undefined' && onelyOne !== '')) {
          response.data = await object.findOne(contirionJson)
        } else if (Object.keys(condition).length > 0 && (typeof onelyOne === 'undefined' && onelyOne === '')) {
          response.data = await object.findAll(contirionJson)
        } else {
          response.data = await object.findAll(contirionJson)
        }
        break
    }
  } catch (error) {
    console.log(error)
    response = 'Internal Server Error'
  }
  return response
}

function modelObjectGet (table) {
  const models = require('../models/init-models').initModels()
  let object
  switch (table) {
    case 'users':
      object = models.users
      break
    case 'app_version':
      object = models.appVersion
      break
    case 'admin_blocks':
      object = models.adminBlocks
      break
    case 'user_abuse':
      object = models.userAbuse
      break
    case 'room':
      object = models.room
      break
    case 'audiences':
      object = models.audiences
      break
    case 'notification':
      object = models.notification
      break
    case 'banner':
      object = models.banner
      break
    case 'family':
      object = models.family
      break
    case 'gifts':
      object = models.gifts
      break
    case 'gift_list':
      object = models.giftList
      break
    case 'assest_purchase':
      object = models.assetsPurchase
      break
    case 'block_category':
      object = models.blockCategory
      break
    case 'transactions':
      object = models.transactions
      break
    case 'assets_store':
      object = models.assetsStore
      break
    case 'role':
      object = models.role
      break
    case 'picture_review':
      object = models.pictureReview
      break
  }
  return object
}

async function upsert (values, condition, Model) {
  const obj = await Model
    .findOne({ where: condition })
  if (obj) { return obj.update(values) }
  return Model.create(values)
}

module.exports = { modelExcute }
