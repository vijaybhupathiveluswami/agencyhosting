const { modelExcute } = require('./model_excute')
const adminBlockCheck = async function (userId) {
  if (userId !== '') {
    const adminBlockData = (await modelExcute({}, 'select', 'admin_blocks', { user_id: userId, status: 0 })).data
    if (adminBlockData.length > 0) {
      const expiryMilliSeconds = Date.parse(adminBlockData[0].expiry_at)
      if (Date.now() >= expiryMilliSeconds && adminBlockData[0].category !== 1) {
        await modelExcute({ status: 2 }, 'update', 'admin_blocks', { user_id: userId, status: 0 })
      }
    }
  }
}
module.exports = { adminBlockCheck }
