const replayData = async function (reply, response) {
  return reply
    .code(response.code)
    .header('Content-Type', 'application/json; charset=utf-8')
    .send({
      status: response.statusCode,
      message: response.message,
      body: response.body
    })
}

module.exports = { replayData }
