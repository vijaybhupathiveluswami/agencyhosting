const { modelExcute } = require('./model_excute')
const redisStoreUserInfo = async function (fastify, data, userId, headers) {
  if (data) {
    const moment = require('moment')
    const { leftPad } = require('./left_pad')
    data.created_at = moment(data.created_at).format('YYYY-MM-DD hh:mm:ss')
    data.chat_expiry_at = moment(data.chat_expiry_at).format('YYYY-MM-DD hh:mm:ss')
    data.updated_at = moment(data.updated_at).format('YYYY-MM-DD hh:mm:ss')
    data.date_of_birth = moment(data.date_of_birth).format('YYYY-MM-DD')
    const { setData } = require('./redis')
    const { Op } = require('sequelize')
    const userAbuseData = (await modelExcute({}, 'select', 'user_abuse', {
      [Op.or]: [{ block_id: userId }, { user_id: userId }],
      type: {
        [Op.or]: ['block', 'kick']
      }
    })).data
    const otherKickoutList = []
    const otherBlockList = []
    const otherRoomBlockList = []
    const blockList = []
    for (const element of userAbuseData) {
      if (element.type === 'block') {
        if (element.user_id === userId) {
          blockList.push(element.block_id)
        } else {
          otherRoomBlockList.push(element.room_id)
          otherBlockList.push(element.user_id)
        }
      }
      if (element.type === 'kick') { otherKickoutList.push(element.user_id) }
    }
    data.blockList = blockList.toString()
    data.otherKickoutList = otherKickoutList.toString()
    data.otherBlockList = otherBlockList.toString()
    data.otherRoomBlockList = otherRoomBlockList.toString()
    const roomData = (await modelExcute({}, 'select', 'room', { user_id: userId })).data
    data.rooms = ''
    if (roomData.length > 0) {
      data.rooms = roomData[0].room_id
    }
    const audienceData = (await modelExcute({}, 'select', 'audiences', { user_id: userId })).data
    const visitorsList = []
    const likedList = []
    const matchedList = []
    let visitor = 0
    let liked = 0
    let matched = 0
    for (const element of audienceData) {
      visitor += parseInt(element.visitors)
      liked += parseInt(element.liked)
      matched += parseInt(element.matched)
      if (parseInt(element.visitors) === 1) { visitorsList.push(element.audience_id) }
      if (parseInt(element.liked) === 1) { likedList.push(element.audience_id) }
      if (parseInt(element.matched) === 1) { matchedList.push(element.audience_id) }
    }
    data.visitors = visitor
    data.liked = liked
    data.matched = matched
    data.visitorsList = visitorsList.join()
    data.likedList = likedList.join()
    data.matchedList = matchedList.join()
    data.adminBlock = 'No'
    const adminBlockData = (await modelExcute({}, 'select', 'admin_blocks', { user_id: userId, status: 0 })).data
    if (adminBlockData.length > 0) {
      const expiryMilliSeconds = Date.parse(adminBlockData[0].expiry_at)
      if (Date.now() >= expiryMilliSeconds && adminBlockData[0].category !== 1) {
        await modelExcute({ status: 2 }, 'update', 'admin_blocks', { user_id: userId, status: 0 })
      } else {
        data.adminBlock = 'Yes'
      }
    }
    if (data.profile_pic === '' || data.profile_pic === null) {
      data.profile_pic = 'https://d338yng2n0d2es.cloudfront.net/Bots/avatar.png'
    }
    data.level = await leftPad(data.level, '2')
    await setData(fastify, userId, data, 3600, 1)
    let firebaseID = 'stgFirebase'
    if (headers.host === 'prod.dreamlive.co.in') {
      firebaseID = 'prodFirebase'
    }
    const firestore = await fastify.firebase[firebaseID]
      .firestore()
      .collection('users')
      .doc(data.firebase_uid)
    await firestore.set({
      name: data.name,
      photoUrl: data.profile_pic,
      id: data.firebase_uid,
      pushToken: data.gcm_registration_id,
      user_id: data.user_id,
      chattingWith: null,
      chatExpiryAt: data.chat_expiry_at,
      vip: data.vip,
      frameEffect: data.frame_effect
    })
  }
}
module.exports = { redisStoreUserInfo }
