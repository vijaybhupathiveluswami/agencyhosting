const verifyImage = async function (parameter) {
  const vision = require('@google-cloud/vision')
  // Imports the Google Cloud client library

  // Creates a client
  const client = new vision.ImageAnnotatorClient()

  // Performs label detection on the image file

  // const [result] = await client.labelDetection('./images/swimsuite.jpeg');
  // const labels = result.labelAnnotations;

  const [result] = await client.textDetection(parameter)
  const labels = result.fullTextAnnotation

  console.log('safeSearchAnnotation:')
  console.log(labels)
  // labels.forEach(label => console.log(label.description));
  const response = {}
  response.statusCode = 0
  response.code = 200
  response.message = 'Success'
  response.body = { label: labels }
  return response
}
module.exports = { verifyImage }
