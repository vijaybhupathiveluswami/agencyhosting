const sendNotification = function (registrationIds, title, message, body, type) {
  const fetch = require('node-fetch')
  let serverKey = ''
  serverKey = process.env.FCMTOKEN
  const tempJson = {}
  switch (type) {
    case 'to':
      tempJson.to = registrationIds
      break
    case 'all':
      tempJson.registration_ids = registrationIds
      break
    default:
      tempJson.to = registrationIds
      break
  }
  tempJson.notification = {
    title: title,
    body: message
  }
  tempJson.data = body
  const jsonObject = JSON.stringify(tempJson)
  fetch('https://fcm.googleapis.com/fcm/send', {
    method: 'post',
    body: jsonObject,
    headers: { Authorization: 'key=' + serverKey, 'Content-Type': 'application/json' }
  }).then(() => { console.log('send success') }).catch((e) => console.log(e))
}
module.exports = { sendNotification }
