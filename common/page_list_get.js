const { getUserData } = require('./redis')
const pageList = async function (fastify, headers, myData, type, offset, end, userId) {
  let c = 0
  const visitorsList = myData.visitorsList.split(',')
  const likedList = myData.likedList.split(',')
  const matchedList = myData.matchedList.split(',')
  const otherBlockList = myData.otherBlockList.split(',')
  const blockList = myData.blockList.split(',')
  let list = []
  switch (type) {
    case 'liked':
      list = likedList
      break
    case 'matched':
      list = matchedList
      break
    case 'visitors':
      list = visitorsList
      break
    case 'blocked':
      list = blockList
      break
  }
  const userArrayData = []
  for (const element of list) {
    if (element !== userId) {
      if (!otherBlockList.includes(element)) {
        if (c < end && c >= offset) {
          const userData = await getUserData(fastify, element, headers)
          const tempData = {
            profileName: userData.name,
            profilePic: userData.profile_pic,
            userId: userData.user_id,
            frameEffect: userData.frameEffect,
            level: userData.level,
            vip: userData.vip,
            age: userData.age,
            gender: userData.gender
          }
          if (type === 'block') {
            tempData.userRelation = 0
          } else {
            let userRelation = 0
            if (matchedList.includes(element)) {
              userRelation = 3
            } else if (likedList.includes(element)) {
              userRelation = 1
            } else if (visitorsList.includes(element)) {
              userRelation = 2
            }
            tempData.userRelation = userRelation
          }
          userArrayData.push(tempData)
        }
        c++
      }
    }
  }
  return userArrayData
}
module.exports = { pageList }
