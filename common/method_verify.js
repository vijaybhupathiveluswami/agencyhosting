const { getUserData } = require('./redis')
const { sessionVerify } = require('./token_verify')
const methodVerify = async function (fastify, headers) {
  const response = {}
  const auth = await headers.authorization.split('Basic ')[1]
  const base64DecodeData = Buffer.from(auth, 'base64').toString()
  const userPass = base64DecodeData.split(':')
  response.code = 200
  response.message = 'Success'
  response.statusCode = 0
  response.body = null
  if (userPass[0] === 'callBack') {
    response.userId = userPass[1]
    const myData = await getUserData(fastify, userPass[1], headers)
    response.userData = myData
  } else {
    const sessionResponse = await sessionVerify(fastify, userPass[0], userPass[1], headers)
    if (sessionResponse.message !== 'valid data') {
      response.message = sessionResponse.message
      response.userData = sessionResponse.userData
      response.statusCode = 1
      response.code = sessionResponse.message === 'Not authorized ' ? 406 : 401
    } else {
      response.userData = sessionResponse.userData
      response.userId = sessionResponse.userId
    }
  }
  if (response.statusCode === 0) {
    if (response.userData.adminBlock === 'Yes') {
      response.statusCode = 1
      response.code = 423
      response.body = { myId: response.userId }
      response.message = 'Admin Block'
    }
  }
  return response
}
module.exports = { methodVerify }
