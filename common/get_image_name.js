const { titleCase } = require('../common/title_case')
const getImageName = function (imageUrl) {
  const imageArray = imageUrl !== null && imageUrl !== '' ? imageUrl.split('/') : []
  const image = imageArray[imageArray.length - 1]
  const imageName = image.substring(0, (image.length - 5))
  return titleCase(imageName.split('_').join(' '))
}
module.exports = { getImageName }
