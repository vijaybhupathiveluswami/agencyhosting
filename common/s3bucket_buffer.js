const s3bucketBuffer = async function s3uploadBuffer (tempImgPath, userId, path, urlPath) {
  const fs = require('fs')
  const res = await new Promise((resolve, reject) => {
    fs.readFile(tempImgPath, async function (error, bufferData) {
      console.log(error)
      console.log(bufferData)
      const webp = require('webp-converter')
      webp.grant_permission()
      const AWS = require('aws-sdk')
      AWS.config.update({ accessKeyId: process.env.S3_ACCESS_KEY, secretAccessKey: process.env.S3_SECRET_KEY })
      const fileName = userId + '_' + new Date().getTime() + '.webp'
      const result = await webp.buffer2webpbuffer(bufferData, 'png', '-q 60', __dirname)
      const base64data = result
      const s3 = new AWS.S3()
      s3.upload({
        Bucket: process.env.S3_BUCKET_NAME + urlPath + path,
        Body: base64data,
        Key: fileName,
        ACL: 'public-read' // Make this object public
      }, function (err, data) {
        console.log('err')
        console.log(err)
        console.log('data')
        console.log(data)
        if (err) return reject(err)
        resolve(data)
      })
    })
  })
  return res
}
module.exports = { s3bucketBuffer }
