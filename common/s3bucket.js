const s3bucket = async function s3upload (file, path, urlPath) {
  const webp = require('webp-converter')
  webp.grant_permission()
  const AWS = require('aws-sdk')
  AWS.config.update({
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_KEY
  })

  const dataBase64 = file.data
  const tmpFilename = file.filename.split('.')
  let fileType = 'jpg'
  switch (file.mimetype) {
    case 'image/jpeg':
      fileType = 'jpg'
      break
    case 'image/png':
      fileType = 'png'
      break
  }
  const fileName = tmpFilename[0] + '_' + new Date().getTime() + '.webp'
  const result = await webp.buffer2webpbuffer(dataBase64, fileType, '-q 60', __dirname)
  const base64data = result
  const s3 = new AWS.S3()
  const res = await new Promise((resolve, reject) => {
    s3.upload({
      Bucket: process.env.S3_BUCKET_NAME + urlPath + path,
      Body: base64data,
      Key: fileName,
      ACL: 'public-read' // Make this object public
    }, (err, data) => err == null ? resolve(data) : reject(err))
  })
  return res
}

module.exports = { s3bucket }
