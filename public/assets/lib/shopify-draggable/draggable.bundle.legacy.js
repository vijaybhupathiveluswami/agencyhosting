(function webpackUniversalModuleDefinition (root, factory) {
  if (typeof exports === 'object' && typeof module === 'object') { module.exports = factory() } else if (typeof define === 'function' && define.amd) { define('Draggable', [], factory) } else if (typeof exports === 'object') { exports.Draggable = factory() } else { root.Draggable = factory() }
})(window, function () {
  return /******/ (function (modules) { // webpackBootstrap
    /******/ 	// The module cache
    /******/ 	const installedModules = {}
    /******/
    /******/ 	// The require function
    /******/ 	function __webpack_require__ (moduleId) {
      /******/
      /******/ 		// Check if module is in cache
      /******/ 		if (installedModules[moduleId]) {
        /******/ 			return installedModules[moduleId].exports
        /******/ 		}
      /******/ 		// Create a new module (and put it into the cache)
      /******/ 		const module = installedModules[moduleId] = {
        /******/ 			i: moduleId,
        /******/ 			l: false,
        /******/ 			exports: {}
        /******/ 		}
      /******/
      /******/ 		// Execute the module function
      /******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__)
      /******/
      /******/ 		// Flag the module as loaded
      /******/ 		module.l = true
      /******/
      /******/ 		// Return the exports of the module
      /******/ 		return module.exports
      /******/ 	}
    /******/
    /******/
    /******/ 	// expose the modules object (__webpack_modules__)
    /******/ 	__webpack_require__.m = modules
    /******/
    /******/ 	// expose the module cache
    /******/ 	__webpack_require__.c = installedModules
    /******/
    /******/ 	// define getter function for harmony exports
    /******/ 	__webpack_require__.d = function (exports, name, getter) {
      /******/ 		if (!__webpack_require__.o(exports, name)) {
        /******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter })
        /******/ 		}
      /******/ 	}
    /******/
    /******/ 	// define __esModule on exports
    /******/ 	__webpack_require__.r = function (exports) {
      /******/ 		if (typeof Symbol !== 'undefined' && Symbol.toStringTag) {
        /******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' })
        /******/ 		}
      /******/ 		Object.defineProperty(exports, '__esModule', { value: true })
      /******/ 	}
    /******/
    /******/ 	// create a fake namespace object
    /******/ 	// mode & 1: value is a module id, require it
    /******/ 	// mode & 2: merge all properties of value into the ns
    /******/ 	// mode & 4: return value when already ns object
    /******/ 	// mode & 8|1: behave like require
    /******/ 	__webpack_require__.t = function (value, mode) {
      /******/ 		if (mode & 1) value = __webpack_require__(value)
      /******/ 		if (mode & 8) return value
      /******/ 		if ((mode & 4) && typeof value === 'object' && value && value.__esModule) return value
      /******/ 		const ns = Object.create(null)
      /******/ 		__webpack_require__.r(ns)
      /******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value })
      /******/ 		if (mode & 2 && typeof value !== 'string') for (const key in value) __webpack_require__.d(ns, key, function (key) { return value[key] }.bind(null, key))
      /******/ 		return ns
      /******/ 	}
    /******/
    /******/ 	// getDefaultExport function for compatibility with non-harmony modules
    /******/ 	__webpack_require__.n = function (module) {
      /******/ 		const getter = module && module.__esModule
      /******/ 			? function getDefault () { return module.default }
      /******/ 			: function getModuleExports () { return module }
      /******/ 		__webpack_require__.d(getter, 'a', getter)
      /******/ 		return getter
      /******/ 	}
    /******/
    /******/ 	// Object.prototype.hasOwnProperty.call
    /******/ 	__webpack_require__.o = function (object, property) { return Object.prototype.hasOwnProperty.call(object, property) }
    /******/
    /******/ 	// __webpack_public_path__
    /******/ 	__webpack_require__.p = ''
    /******/
    /******/
    /******/ 	// Load entry module and return exports
    /******/ 	return __webpack_require__(__webpack_require__.s = 254)
    /******/ })
  /************************************************************************/
  /******/ ([
    /* 0 */
    /***/ function (module, exports, __webpack_require__) {
      'use strict'

      exports.__esModule = true

      const _defineProperty = __webpack_require__(211)

      const _defineProperty2 = _interopRequireDefault(_defineProperty)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = (function () {
        function defineProperties (target, props) {
          for (let i = 0; i < props.length; i++) {
            const descriptor = props[i]
            descriptor.enumerable = descriptor.enumerable || false
            descriptor.configurable = true
            if ('value' in descriptor) descriptor.writable = true;
            (0, _defineProperty2.default)(target, descriptor.key, descriptor)
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps)
          if (staticProps) defineProperties(Constructor, staticProps)
          return Constructor
        }
      }())
      /***/ },
    /* 1 */
    /***/ function (module, exports, __webpack_require__) {
      'use strict'

      exports.__esModule = true

      exports.default = function (instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError('Cannot call a class as a function')
        }
      }
      /***/ },
    /* 2 */
    /***/ function (module, exports, __webpack_require__) {
      'use strict'

      exports.__esModule = true

      const _setPrototypeOf = __webpack_require__(171)

      const _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf)

      const _create = __webpack_require__(167)

      const _create2 = _interopRequireDefault(_create)

      const _typeof2 = __webpack_require__(88)

      const _typeof3 = _interopRequireDefault(_typeof2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = function (subClass, superClass) {
        if (typeof superClass !== 'function' && superClass !== null) {
          throw new TypeError('Super expression must either be null or a function, not ' + (typeof superClass === 'undefined' ? 'undefined' : (0, _typeof3.default)(superClass)))
        }

        subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
          constructor: {
            value: subClass,
            enumerable: false,
            writable: true,
            configurable: true
          }
        })
        if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass
      }
      /***/ },
    /* 3 */
    /***/ function (module, exports, __webpack_require__) {
      'use strict'

      exports.__esModule = true

      const _typeof2 = __webpack_require__(88)

      const _typeof3 = _interopRequireDefault(_typeof2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = function (self, call) {
        if (!self) {
          throw new ReferenceError("this hasn't been initialised - super() hasn't been called")
        }

        return call && ((typeof call === 'undefined' ? 'undefined' : (0, _typeof3.default)(call)) === 'object' || typeof call === 'function') ? call : self
      }
      /***/ },
    /* 4 */
    /***/ function (module, exports, __webpack_require__) {
      const store = __webpack_require__(74)('wks')
      const uid = __webpack_require__(41)
      const Symbol = __webpack_require__(5).Symbol
      const USE_SYMBOL = typeof Symbol === 'function'

      const $exports = module.exports = function (name) {
        return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name))
      }

      $exports.store = store
      /***/ },
    /* 5 */
    /***/ function (module, exports) {
      // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
      const global = module.exports = typeof window !== 'undefined' && window.Math == Math
        ? window : typeof self !== 'undefined' && self.Math == Math ? self
          // eslint-disable-next-line no-new-func
            : Function('return this')()
      if (typeof __g === 'number') __g = global // eslint-disable-line no-undef
      /***/ },
    /* 6 */
    /***/ function (module, exports) {
      const core = module.exports = { version: '2.5.7' }
      if (typeof __e === 'number') __e = core // eslint-disable-line no-undef
      /***/ },
    /* 7 */
    /***/ function (module, exports) {
      const core = module.exports = { version: '2.5.7' }
      if (typeof __e === 'number') __e = core // eslint-disable-line no-undef
      /***/ },
    /* 8 */
    /***/ function (module, exports, __webpack_require__) {
      const store = __webpack_require__(58)('wks')
      const uid = __webpack_require__(44)
      const Symbol = __webpack_require__(13).Symbol
      const USE_SYMBOL = typeof Symbol === 'function'

      const $exports = module.exports = function (name) {
        return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name))
      }

      $exports.store = store
      /***/ },
    /* 9 */
    /***/ function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _AbstractEvent = __webpack_require__(212)

      const _AbstractEvent2 = _interopRequireDefault(_AbstractEvent)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _AbstractEvent2.default
      /***/ },
    /* 10 */
    /***/ function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _AbstractPlugin = __webpack_require__(207)

      const _AbstractPlugin2 = _interopRequireDefault(_AbstractPlugin)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _AbstractPlugin2.default
      /***/
    },
    /* 11 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _closest = __webpack_require__(164)

      Object.defineProperty(exports, 'closest', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_closest).default
        }
      })

      const _requestNextAnimationFrame = __webpack_require__(162)

      Object.defineProperty(exports, 'requestNextAnimationFrame', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_requestNextAnimationFrame).default
        }
      })

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }
      /***/
    },
    /* 12 */
    /***/
    function (module, exports, __webpack_require__) {
      const anObject = __webpack_require__(27)
      const IE8_DOM_DEFINE = __webpack_require__(96)
      const toPrimitive = __webpack_require__(66)
      const dP = Object.defineProperty

      exports.f = __webpack_require__(17) ? Object.defineProperty : function defineProperty (O, P, Attributes) {
        anObject(O)
        P = toPrimitive(P, true)
        anObject(Attributes)
        if (IE8_DOM_DEFINE) {
          try {
            return dP(O, P, Attributes)
          } catch (e) { /* empty */ }
        }
        if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!')
        if ('value' in Attributes) O[P] = Attributes.value
        return O
      }
      /***/
    },
    /* 13 */
    /***/
    function (module, exports) {
      // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
      const global = module.exports = typeof window !== 'undefined' && window.Math == Math
        ? window : typeof self !== 'undefined' && self.Math == Math ? self
          // eslint-disable-next-line no-new-func
            : Function('return this')()
      if (typeof __g === 'number') __g = global // eslint-disable-line no-undef
      /***/
    },
    /* 14 */
    /***/
    function (module, exports, __webpack_require__) {
      const isObject = __webpack_require__(20)
      module.exports = function (it) {
        if (!isObject(it)) throw TypeError(it + ' is not an object!')
        return it
      }
      /***/
    },
    /* 15 */
    /***/
    function (module, exports, __webpack_require__) {
      // to indexed object, toObject with fallback for non-array-like ES3 strings
      const IObject = __webpack_require__(200)
      const defined = __webpack_require__(63)
      module.exports = function (it) {
        return IObject(defined(it))
      }
      /***/
    },
    /* 16 */
    /***/
    function (module, exports) {
      const hasOwnProperty = {}.hasOwnProperty
      module.exports = function (it, key) {
        return hasOwnProperty.call(it, key)
      }
      /***/
    },
    /* 17 */
    /***/
    function (module, exports, __webpack_require__) {
      // Thank's IE8 for his funny defineProperty
      module.exports = !__webpack_require__(36)(function () {
        return Object.defineProperty({}, 'a', { get: function () { return 7 } }).a != 7
      })
      /***/
    },
    /* 18 */
    /***/
    function (module, exports, __webpack_require__) {
      const global = __webpack_require__(13)
      const core = __webpack_require__(6)
      const ctx = __webpack_require__(67)
      const hide = __webpack_require__(28)
      const has = __webpack_require__(16)
      const PROTOTYPE = 'prototype'

      var $export = function (type, name, source) {
        const IS_FORCED = type & $export.F
        const IS_GLOBAL = type & $export.G
        const IS_STATIC = type & $export.S
        const IS_PROTO = type & $export.P
        const IS_BIND = type & $export.B
        const IS_WRAP = type & $export.W
        const exports = IS_GLOBAL ? core : core[name] || (core[name] = {})
        const expProto = exports[PROTOTYPE]
        const target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE]
        let key, own, out
        if (IS_GLOBAL) source = name
        for (key in source) {
          // contains in native
          own = !IS_FORCED && target && target[key] !== undefined
          if (own && has(exports, key)) continue
          // export native or passed
          out = own ? target[key] : source[key]
          // prevent global pollution for namespaces
          exports[key] = IS_GLOBAL && typeof target[key] !== 'function' ? source[key]
          // bind timers to global for call from export context
            : IS_BIND && own ? ctx(out, global)
            // wrap global constructors for prevent change them in library
              : IS_WRAP && target[key] == out ? (function (C) {
                const F = function (a, b, c) {
                  if (this instanceof C) {
                    switch (arguments.length) {
                      case 0:
                        return new C()
                      case 1:
                        return new C(a)
                      case 2:
                        return new C(a, b)
                    }
                    return new C(a, b, c)
                  }
                  return C.apply(this, arguments)
                }
                F[PROTOTYPE] = C[PROTOTYPE]
                return F
                // make static versions for prototype methods
              })(out) : IS_PROTO && typeof out === 'function' ? ctx(Function.call, out) : out
          // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
          if (IS_PROTO) {
            (exports.virtual || (exports.virtual = {}))[key] = out
            // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
            if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out)
          }
        }
      }
      // type bitmap
      $export.F = 1 // forced
      $export.G = 2 // global
      $export.S = 4 // static
      $export.P = 8 // proto
      $export.B = 16 // bind
      $export.W = 32 // wrap
      $export.U = 64 // safe
      $export.R = 128 // real proto method for `library`
      module.exports = $export
      /***/
    },
    /* 19 */
    /***/
    function (module, exports, __webpack_require__) {
      // to indexed object, toObject with fallback for non-array-like ES3 strings
      const IObject = __webpack_require__(108)
      const defined = __webpack_require__(73)
      module.exports = function (it) {
        return IObject(defined(it))
      }
      /***/
    },
    /* 20 */
    /***/
    function (module, exports) {
      module.exports = function (it) {
        return typeof it === 'object' ? it !== null : typeof it === 'function'
      }
      /***/
    },
    /* 21 */
    /***/
    function (module, exports, __webpack_require__) {
      const anObject = __webpack_require__(14)
      const IE8_DOM_DEFINE = __webpack_require__(111)
      const toPrimitive = __webpack_require__(75)
      const dP = Object.defineProperty

      exports.f = __webpack_require__(23) ? Object.defineProperty : function defineProperty (O, P, Attributes) {
        anObject(O)
        P = toPrimitive(P, true)
        anObject(Attributes)
        if (IE8_DOM_DEFINE) {
          try {
            return dP(O, P, Attributes)
          } catch (e) { /* empty */ }
        }
        if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!')
        if ('value' in Attributes) O[P] = Attributes.value
        return O
      }
      /***/
    },
    /* 22 */
    /***/
    function (module, exports, __webpack_require__) {
      const dP = __webpack_require__(21)
      const createDesc = __webpack_require__(49)
      module.exports = __webpack_require__(23) ? function (object, key, value) {
        return dP.f(object, key, createDesc(1, value))
      } : function (object, key, value) {
        object[key] = value
        return object
      }
      /***/
    },
    /* 23 */
    /***/
    function (module, exports, __webpack_require__) {
      // Thank's IE8 for his funny defineProperty
      module.exports = !__webpack_require__(42)(function () {
        return Object.defineProperty({}, 'a', { get: function () { return 7 } }).a != 7
      })
      /***/
    },
    /* 24 */
    /***/
    function (module, exports) {
      const hasOwnProperty = {}.hasOwnProperty
      module.exports = function (it, key) {
        return hasOwnProperty.call(it, key)
      }
      /***/
    },
    /* 25 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      exports.__esModule = true

      const _from = __webpack_require__(205)

      const _from2 = _interopRequireDefault(_from)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = function (arr) {
        if (Array.isArray(arr)) {
          for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
            arr2[i] = arr[i]
          }

          return arr2
        } else {
          return (0, _from2.default)(arr)
        }
      }
      /***/
    },
    /* 26 */
    /***/
    function (module, exports) {
      module.exports = function (it) {
        return typeof it === 'object' ? it !== null : typeof it === 'function'
      }
      /***/
    },
    /* 27 */
    /***/
    function (module, exports, __webpack_require__) {
      const isObject = __webpack_require__(26)
      module.exports = function (it) {
        if (!isObject(it)) throw TypeError(it + ' is not an object!')
        return it
      }
      /***/
    },
    /* 28 */
    /***/
    function (module, exports, __webpack_require__) {
      const dP = __webpack_require__(12)
      const createDesc = __webpack_require__(35)
      module.exports = __webpack_require__(17) ? function (object, key, value) {
        return dP.f(object, key, createDesc(1, value))
      } : function (object, key, value) {
        object[key] = value
        return object
      }
      /***/
    },
    /* 29 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.2.14 / 15.2.3.14 Object.keys(O)
      const $keys = __webpack_require__(109)
      const enumBugKeys = __webpack_require__(70)

      module.exports = Object.keys || function keys (O) {
        return $keys(O, enumBugKeys)
      }
      /***/
    },
    /* 30 */
    /***/
    function (module, exports, __webpack_require__) {
      const global = __webpack_require__(5)
      const hide = __webpack_require__(22)
      const has = __webpack_require__(24)
      const SRC = __webpack_require__(41)('src')
      const TO_STRING = 'toString'
      const $toString = Function[TO_STRING]
      const TPL = ('' + $toString).split(TO_STRING)

      __webpack_require__(7).inspectSource = function (it) {
        return $toString.call(it)
      };

      (module.exports = function (O, key, val, safe) {
        const isFunction = typeof val === 'function'
        if (isFunction) has(val, 'name') || hide(val, 'name', key)
        if (O[key] === val) return
        if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)))
        if (O === global) {
          O[key] = val
        } else if (!safe) {
          delete O[key]
          hide(O, key, val)
        } else if (O[key]) {
          O[key] = val
        } else {
          hide(O, key, val)
        }
        // add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
      })(Function.prototype, TO_STRING, function toString () {
        return typeof this === 'function' && this[SRC] || $toString.call(this)
      })
      /***/
    },
    /* 31 */
    /***/
    function (module, exports, __webpack_require__) {
      const global = __webpack_require__(5)
      const core = __webpack_require__(7)
      const hide = __webpack_require__(22)
      const redefine = __webpack_require__(30)
      const ctx = __webpack_require__(48)
      const PROTOTYPE = 'prototype'

      var $export = function (type, name, source) {
        const IS_FORCED = type & $export.F
        const IS_GLOBAL = type & $export.G
        const IS_STATIC = type & $export.S
        const IS_PROTO = type & $export.P
        const IS_BIND = type & $export.B
        const target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE]
        const exports = IS_GLOBAL ? core : core[name] || (core[name] = {})
        const expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {})
        let key, own, out, exp
        if (IS_GLOBAL) source = name
        for (key in source) {
          // contains in native
          own = !IS_FORCED && target && target[key] !== undefined
          // export native or passed
          out = (own ? target : source)[key]
          // bind timers to global for call from export context
          exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out === 'function' ? ctx(Function.call, out) : out
          // extend global
          if (target) redefine(target, key, out, type & $export.U)
          // export
          if (exports[key] != out) hide(exports, key, exp)
          if (IS_PROTO && expProto[key] != out) expProto[key] = out
        }
      }
      global.core = core
      // type bitmap
      $export.F = 1 // forced
      $export.G = 2 // global
      $export.S = 4 // static
      $export.P = 8 // proto
      $export.B = 16 // bind
      $export.W = 32 // wrap
      $export.U = 64 // safe
      $export.R = 128 // real proto method for `library`
      module.exports = $export
      /***/
    },
    /* 32 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _SensorEvent = __webpack_require__(160)

      Object.keys(_SensorEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _SensorEvent[key]
          }
        })
      })
      /***/
    },
    /* 33 */
    /***/
    function (module, exports) {
      module.exports = {}
      /***/
    },
    /* 34 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _Sensor = __webpack_require__(206)

      const _Sensor2 = _interopRequireDefault(_Sensor)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Sensor2.default
      /***/
    },
    /* 35 */
    /***/
    function (module, exports) {
      module.exports = function (bitmap, value) {
        return {
          enumerable: !(bitmap & 1),
          configurable: !(bitmap & 2),
          writable: !(bitmap & 4),
          value: value
        }
      }
      /***/
    },
    /* 36 */
    /***/
    function (module, exports) {
      module.exports = function (exec) {
        try {
          return !!exec()
        } catch (e) {
          return true
        }
      }
      /***/
    },
    /* 37 */
    /***/
    function (module, exports) {
      module.exports = {}
      /***/
    },
    /* 38 */
    /***/
    function (module, exports) {
      exports.f = {}.propertyIsEnumerable
      /***/
    },
    /* 39 */
    /***/
    function (module, exports) {
      const toString = {}.toString

      module.exports = function (it) {
        return toString.call(it).slice(8, -1)
      }
      /***/
    },
    /* 40 */
    /***/
    function (module, exports) {
      module.exports = false
      /***/
    },
    /* 41 */
    /***/
    function (module, exports) {
      let id = 0
      const px = Math.random()
      module.exports = function (key) {
        return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36))
      }
      /***/
    },
    /* 42 */
    /***/
    function (module, exports) {
      module.exports = function (exec) {
        try {
          return !!exec()
        } catch (e) {
          return true
        }
      }
      /***/
    },
    /* 43 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _DragEvent = __webpack_require__(83)

      Object.keys(_DragEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _DragEvent[key]
          }
        })
      })

      const _DraggableEvent = __webpack_require__(82)

      Object.keys(_DraggableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _DraggableEvent[key]
          }
        })
      })

      const _Plugins = __webpack_require__(81)

      Object.keys(_Plugins).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _Plugins[key]
          }
        })
      })

      const _Sensors = __webpack_require__(65)

      Object.keys(_Sensors).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _Sensors[key]
          }
        })
      })

      const _Draggable = __webpack_require__(129)

      const _Draggable2 = _interopRequireDefault(_Draggable)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Draggable2.default
      /***/
    },
    /* 44 */
    /***/
    function (module, exports) {
      let id = 0
      const px = Math.random()
      module.exports = function (key) {
        return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36))
      }
      /***/
    },
    /* 45 */
    /***/
    function (module, exports) {
      module.exports = true
      /***/
    },
    /* 46 */
    /***/
    function (module, exports, __webpack_require__) {
      const def = __webpack_require__(21).f
      const has = __webpack_require__(24)
      const TAG = __webpack_require__(4)('toStringTag')

      module.exports = function (it, tag, stat) {
        if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag })
      }
      /***/
    },
    /* 47 */
    /***/
    function (module, exports) {
      module.exports = function (it) {
        if (typeof it !== 'function') throw TypeError(it + ' is not a function!')
        return it
      }
      /***/
    },
    /* 48 */
    /***/
    function (module, exports, __webpack_require__) {
      // optional / simple context binding
      const aFunction = __webpack_require__(47)
      module.exports = function (fn, that, length) {
        aFunction(fn)
        if (that === undefined) return fn
        switch (length) {
          case 1:
            return function (a) {
              return fn.call(that, a)
            }
          case 2:
            return function (a, b) {
              return fn.call(that, a, b)
            }
          case 3:
            return function (a, b, c) {
              return fn.call(that, a, b, c)
            }
        }
        return function (/* ...args */) {
          return fn.apply(that, arguments)
        }
      }
      /***/
    },
    /* 49 */
    /***/
    function (module, exports) {
      module.exports = function (bitmap, value) {
        return {
          enumerable: !(bitmap & 1),
          configurable: !(bitmap & 2),
          writable: !(bitmap & 4),
          value: value
        }
      }
      /***/
    },
    /* 50 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      exports.__esModule = true

      const _getPrototypeOf = __webpack_require__(123)

      const _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf)

      const _getOwnPropertyDescriptor = __webpack_require__(120)

      const _getOwnPropertyDescriptor2 = _interopRequireDefault(_getOwnPropertyDescriptor)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = function get (object, property, receiver) {
        if (object === null) object = Function.prototype
        const desc = (0, _getOwnPropertyDescriptor2.default)(object, property)

        if (desc === undefined) {
          const parent = (0, _getPrototypeOf2.default)(object)

          if (parent === null) {
            return undefined
          } else {
            return get(parent, property, receiver)
          }
        } else if ('value' in desc) {
          return desc.value
        } else {
          const getter = desc.get

          if (getter === undefined) {
            return undefined
          }

          return getter.call(receiver)
        }
      }
      /***/
    },
    /* 51 */
    /***/
    function (module, exports, __webpack_require__) {
      const pIE = __webpack_require__(52)
      const createDesc = __webpack_require__(35)
      const toIObject = __webpack_require__(15)
      const toPrimitive = __webpack_require__(66)
      const has = __webpack_require__(16)
      const IE8_DOM_DEFINE = __webpack_require__(96)
      const gOPD = Object.getOwnPropertyDescriptor

      exports.f = __webpack_require__(17) ? gOPD : function getOwnPropertyDescriptor (O, P) {
        O = toIObject(O)
        P = toPrimitive(P, true)
        if (IE8_DOM_DEFINE) {
          try {
            return gOPD(O, P)
          } catch (e) { /* empty */ }
        }
        if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P])
      }
      /***/
    },
    /* 52 */
    /***/
    function (module, exports) {
      exports.f = {}.propertyIsEnumerable
      /***/
    },
    /* 53 */
    /***/
    function (module, exports, __webpack_require__) {
      const global = __webpack_require__(13)
      const core = __webpack_require__(6)
      const LIBRARY = __webpack_require__(45)
      const wksExt = __webpack_require__(54)
      const defineProperty = __webpack_require__(12).f
      module.exports = function (name) {
        const $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {})
        if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) })
      }
      /***/
    },
    /* 54 */
    /***/
    function (module, exports, __webpack_require__) {
      exports.f = __webpack_require__(8)
      /***/
    },
    /* 55 */
    /***/
    function (module, exports, __webpack_require__) {
      // 7.1.13 ToObject(argument)
      const defined = __webpack_require__(63)
      module.exports = function (it) {
        return Object(defined(it))
      }
      /***/
    },
    /* 56 */
    /***/
    function (module, exports, __webpack_require__) {
      const def = __webpack_require__(12).f
      const has = __webpack_require__(16)
      const TAG = __webpack_require__(8)('toStringTag')

      module.exports = function (it, tag, stat) {
        if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag })
      }
      /***/
    },
    /* 57 */
    /***/
    function (module, exports) {
      // IE 8- don't enum bug keys
      module.exports = (
        'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
      ).split(',')
      /***/
    },
    /* 58 */
    /***/
    function (module, exports, __webpack_require__) {
      const core = __webpack_require__(6)
      const global = __webpack_require__(13)
      const SHARED = '__core-js_shared__'
      const store = global[SHARED] || (global[SHARED] = {});

      (module.exports = function (key, value) {
        return store[key] || (store[key] = value !== undefined ? value : {})
      })('versions', []).push({
        version: core.version,
        mode: __webpack_require__(45) ? 'pure' : 'global',
        copyright: '© 2018 Denis Pushkarev (zloirock.ru)'
      })
      /***/
    },
    /* 59 */
    /***/
    function (module, exports, __webpack_require__) {
      const shared = __webpack_require__(58)('keys')
      const uid = __webpack_require__(44)
      module.exports = function (key) {
        return shared[key] || (shared[key] = uid(key))
      }
      /***/
    },
    /* 60 */
    /***/
    function (module, exports) {
      const toString = {}.toString

      module.exports = function (it) {
        return toString.call(it).slice(8, -1)
      }
      /***/
    },
    /* 61 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.2.14 / 15.2.3.14 Object.keys(O)
      const $keys = __webpack_require__(91)
      const enumBugKeys = __webpack_require__(57)

      module.exports = Object.keys || function keys (O) {
        return $keys(O, enumBugKeys)
      }
      /***/
    },
    /* 62 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
      const anObject = __webpack_require__(27)
      const dPs = __webpack_require__(201)
      const enumBugKeys = __webpack_require__(57)
      const IE_PROTO = __webpack_require__(59)('IE_PROTO')
      const Empty = function () { /* empty */ }
      const PROTOTYPE = 'prototype'

      // Create object with fake `null` prototype: use iframe Object with cleared prototype
      var createDict = function () {
        // Thrash, waste and sodomy: IE GC bug
        const iframe = __webpack_require__(95)('iframe')
        let i = enumBugKeys.length
        const lt = '<'
        const gt = '>'
        let iframeDocument
        iframe.style.display = 'none'
        __webpack_require__(197).appendChild(iframe)
        iframe.src = 'javascript:' // eslint-disable-line no-script-url
        // createDict = iframe.contentWindow.Object;
        // html.removeChild(iframe);
        iframeDocument = iframe.contentWindow.document
        iframeDocument.open()
        iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt)
        iframeDocument.close()
        createDict = iframeDocument.F
        while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]]
        return createDict()
      }

      module.exports = Object.create || function create (O, Properties) {
        let result
        if (O !== null) {
          Empty[PROTOTYPE] = anObject(O)
          result = new Empty()
          Empty[PROTOTYPE] = null
          // add "__proto__" for Object.getPrototypeOf polyfill
          result[IE_PROTO] = O
        } else result = createDict()
        return Properties === undefined ? result : dPs(result, Properties)
      }
      /***/
    },
    /* 63 */
    /***/
    function (module, exports) {
      // 7.2.1 RequireObjectCoercible(argument)
      module.exports = function (it) {
        if (it == undefined) throw TypeError("Can't call method on  " + it)
        return it
      }
      /***/
    },
    /* 64 */
    /***/
    function (module, exports) {
      // 7.1.4 ToInteger
      const ceil = Math.ceil
      const floor = Math.floor
      module.exports = function (it) {
        return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it)
      }
      /***/
    },
    /* 65 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _Sensor = __webpack_require__(34)

      Object.defineProperty(exports, 'Sensor', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_Sensor).default
        }
      })

      const _MouseSensor = __webpack_require__(189)

      Object.defineProperty(exports, 'MouseSensor', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_MouseSensor).default
        }
      })

      const _TouchSensor = __webpack_require__(159)

      Object.defineProperty(exports, 'TouchSensor', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_TouchSensor).default
        }
      })

      const _DragSensor = __webpack_require__(157)

      Object.defineProperty(exports, 'DragSensor', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_DragSensor).default
        }
      })

      const _ForceTouchSensor = __webpack_require__(155)

      Object.defineProperty(exports, 'ForceTouchSensor', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_ForceTouchSensor).default
        }
      })

      const _SensorEvent = __webpack_require__(32)

      Object.keys(_SensorEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _SensorEvent[key]
          }
        })
      })

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }
      /***/
    },
    /* 66 */
    /***/
    function (module, exports, __webpack_require__) {
      // 7.1.1 ToPrimitive(input [, PreferredType])
      const isObject = __webpack_require__(26)
      // instead of the ES6 spec version, we didn't implement @@toPrimitive case
      // and the second argument - flag - preferred type is a string
      module.exports = function (it, S) {
        if (!isObject(it)) return it
        let fn, val
        if (S && typeof (fn = it.toString) === 'function' && !isObject(val = fn.call(it))) return val
        if (typeof (fn = it.valueOf) === 'function' && !isObject(val = fn.call(it))) return val
        if (!S && typeof (fn = it.toString) === 'function' && !isObject(val = fn.call(it))) return val
        throw TypeError("Can't convert object to primitive value")
      }
      /***/
    },
    /* 67 */
    /***/
    function (module, exports, __webpack_require__) {
      // optional / simple context binding
      const aFunction = __webpack_require__(208)
      module.exports = function (fn, that, length) {
        aFunction(fn)
        if (that === undefined) return fn
        switch (length) {
          case 1:
            return function (a) {
              return fn.call(that, a)
            }
          case 2:
            return function (a, b) {
              return fn.call(that, a, b)
            }
          case 3:
            return function (a, b, c) {
              return fn.call(that, a, b, c)
            }
        }
        return function (/* ...args */) {
          return fn.apply(that, arguments)
        }
      }
      /***/
    },
    /* 68 */
    /***/
    function (module, exports, __webpack_require__) {
      // getting tag from 19.1.3.6 Object.prototype.toString()
      const cof = __webpack_require__(39)
      const TAG = __webpack_require__(4)('toStringTag')
      // ES3 wrong here
      const ARG = cof(function () { return arguments }()) == 'Arguments'

      // fallback for IE11 Script Access Denied error
      const tryGet = function (it, key) {
        try {
          return it[key]
        } catch (e) { /* empty */ }
      }

      module.exports = function (it) {
        let O, T, B
        return it === undefined ? 'Undefined' : it === null ? 'Null'
        // @@toStringTag case
          : typeof (T = tryGet(O = Object(it), TAG)) === 'string' ? T
          // builtinTag case
            : ARG ? cof(O)
            // ES3 arguments fallback
              : (B = cof(O)) == 'Object' && typeof O.callee === 'function' ? 'Arguments' : B
      }
      /***/
    },
    /* 69 */
    /***/
    function (module, exports) {
      exports.f = Object.getOwnPropertySymbols
      /***/
    },
    /* 70 */
    /***/
    function (module, exports) {
      // IE 8- don't enum bug keys
      module.exports = (
        'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
      ).split(',')
      /***/
    },
    /* 71 */
    /***/
    function (module, exports, __webpack_require__) {
      const shared = __webpack_require__(74)('keys')
      const uid = __webpack_require__(41)
      module.exports = function (key) {
        return shared[key] || (shared[key] = uid(key))
      }
      /***/
    },
    /* 72 */
    /***/
    function (module, exports) {
      // 7.1.4 ToInteger
      const ceil = Math.ceil
      const floor = Math.floor
      module.exports = function (it) {
        return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it)
      }
      /***/
    },
    /* 73 */
    /***/
    function (module, exports) {
      // 7.2.1 RequireObjectCoercible(argument)
      module.exports = function (it) {
        if (it == undefined) throw TypeError("Can't call method on  " + it)
        return it
      }
      /***/
    },
    /* 74 */
    /***/
    function (module, exports, __webpack_require__) {
      const core = __webpack_require__(7)
      const global = __webpack_require__(5)
      const SHARED = '__core-js_shared__'
      const store = global[SHARED] || (global[SHARED] = {});

      (module.exports = function (key, value) {
        return store[key] || (store[key] = value !== undefined ? value : {})
      })('versions', []).push({
        version: core.version,
        mode: __webpack_require__(40) ? 'pure' : 'global',
        copyright: '© 2018 Denis Pushkarev (zloirock.ru)'
      })
      /***/
    },
    /* 75 */
    /***/
    function (module, exports, __webpack_require__) {
      // 7.1.1 ToPrimitive(input [, PreferredType])
      const isObject = __webpack_require__(20)
      // instead of the ES6 spec version, we didn't implement @@toPrimitive case
      // and the second argument - flag - preferred type is a string
      module.exports = function (it, S) {
        if (!isObject(it)) return it
        let fn, val
        if (S && typeof (fn = it.toString) === 'function' && !isObject(val = fn.call(it))) return val
        if (typeof (fn = it.valueOf) === 'function' && !isObject(val = fn.call(it))) return val
        if (!S && typeof (fn = it.toString) === 'function' && !isObject(val = fn.call(it))) return val
        throw TypeError("Can't convert object to primitive value")
      }
      /***/
    },
    /* 76 */
    /***/
    function (module, exports, __webpack_require__) {
      const isObject = __webpack_require__(20)
      const document = __webpack_require__(5).document
      // typeof document.createElement is 'object' in old IE
      const is = isObject(document) && isObject(document.createElement)
      module.exports = function (it) {
        return is ? document.createElement(it) : {}
      }
      /***/
    },
    /* 77 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _SortableEvent = __webpack_require__(113)

      Object.keys(_SortableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _SortableEvent[key]
          }
        })
      })
      /***/
    },
    /* 78 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _SwappableEvent = __webpack_require__(116)

      Object.keys(_SwappableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _SwappableEvent[key]
          }
        })
      })
      /***/
    },
    /* 79 */
    /***/
    function (module, exports, __webpack_require__) {
      // most Object methods by ES6 should accept primitives
      const $export = __webpack_require__(18)
      const core = __webpack_require__(6)
      const fails = __webpack_require__(36)
      module.exports = function (KEY, exec) {
        const fn = (core.Object || {})[KEY] || Object[KEY]
        const exp = {}
        exp[KEY] = exec(fn)
        $export($export.S + $export.F * fails(function () { fn(1) }), 'Object', exp)
      }
      /***/
    },
    /* 80 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _DroppableEvent = __webpack_require__(125)

      Object.keys(_DroppableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _DroppableEvent[key]
          }
        })
      })
      /***/
    },
    /* 81 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _Announcement = __webpack_require__(140)

      Object.defineProperty(exports, 'Announcement', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_Announcement).default
        }
      })
      Object.defineProperty(exports, 'defaultAnnouncementOptions', {
        enumerable: true,
        get: function get () {
          return _Announcement.defaultOptions
        }
      })

      const _Focusable = __webpack_require__(138)

      Object.defineProperty(exports, 'Focusable', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_Focusable).default
        }
      })

      const _Mirror = __webpack_require__(136)

      Object.defineProperty(exports, 'Mirror', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_Mirror).default
        }
      })
      Object.defineProperty(exports, 'defaultMirrorOptions', {
        enumerable: true,
        get: function get () {
          return _Mirror.defaultOptions
        }
      })

      const _Scrollable = __webpack_require__(131)

      Object.defineProperty(exports, 'Scrollable', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_Scrollable).default
        }
      })
      Object.defineProperty(exports, 'defaultScrollableOptions', {
        enumerable: true,
        get: function get () {
          return _Scrollable.defaultOptions
        }
      })

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }
      /***/
    },
    /* 82 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _DraggableEvent = __webpack_require__(141)

      Object.keys(_DraggableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _DraggableEvent[key]
          }
        })
      })
      /***/
    },
    /* 83 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _DragEvent = __webpack_require__(142)

      Object.keys(_DragEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _DragEvent[key]
          }
        })
      })
      /***/
    },
    /* 84 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _SnappableEvent = __webpack_require__(146)

      Object.keys(_SnappableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _SnappableEvent[key]
          }
        })
      })
      /***/
    },
    /* 85 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _CollidableEvent = __webpack_require__(151)

      Object.keys(_CollidableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _CollidableEvent[key]
          }
        })
      })
      /***/
    },
    /* 86 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
      const $keys = __webpack_require__(91)
      const hiddenKeys = __webpack_require__(57).concat('length', 'prototype')

      exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames (O) {
        return $keys(O, hiddenKeys)
      }
      /***/
    },
    /* 87 */
    /***/
    function (module, exports) {
      exports.f = Object.getOwnPropertySymbols
      /***/
    },
    /* 88 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      exports.__esModule = true

      const _iterator = __webpack_require__(187)

      const _iterator2 = _interopRequireDefault(_iterator)

      const _symbol = __webpack_require__(181)

      const _symbol2 = _interopRequireDefault(_symbol)

      const _typeof = typeof _symbol2.default === 'function' && typeof _iterator2.default === 'symbol' ? function (obj) { return typeof obj } : function (obj) { return obj && typeof _symbol2.default === 'function' && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? 'symbol' : typeof obj }

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = typeof _symbol2.default === 'function' && _typeof(_iterator2.default) === 'symbol' ? function (obj) {
        return typeof obj === 'undefined' ? 'undefined' : _typeof(obj)
      } : function (obj) {
        return obj && typeof _symbol2.default === 'function' && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? 'symbol' : typeof obj === 'undefined' ? 'undefined' : _typeof(obj)
      }
      /***/
    },
    /* 89 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
      const has = __webpack_require__(16)
      const toObject = __webpack_require__(55)
      const IE_PROTO = __webpack_require__(59)('IE_PROTO')
      const ObjectProto = Object.prototype

      module.exports = Object.getPrototypeOf || function (O) {
        O = toObject(O)
        if (has(O, IE_PROTO)) return O[IE_PROTO]
        if (typeof O.constructor === 'function' && O instanceof O.constructor) {
          return O.constructor.prototype
        }
        return O instanceof Object ? ObjectProto : null
      }
      /***/
    },
    /* 90 */
    /***/
    function (module, exports, __webpack_require__) {
      // 7.1.15 ToLength
      const toInteger = __webpack_require__(64)
      const min = Math.min
      module.exports = function (it) {
        return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0 // pow(2, 53) - 1 == 9007199254740991
      }
      /***/
    },
    /* 91 */
    /***/
    function (module, exports, __webpack_require__) {
      const has = __webpack_require__(16)
      const toIObject = __webpack_require__(15)
      const arrayIndexOf = __webpack_require__(199)(false)
      const IE_PROTO = __webpack_require__(59)('IE_PROTO')

      module.exports = function (object, names) {
        const O = toIObject(object)
        let i = 0
        const result = []
        let key
        for (key in O) { if (key != IE_PROTO) has(O, key) && result.push(key) }
        // Don't enum bug & hidden keys
        while (names.length > i) {
          if (has(O, key = names[i++])) {
            ~arrayIndexOf(result, key) || result.push(key)
          }
        }
        return result
      }
      /***/
    },
    /* 92 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = __webpack_require__(28)
      /***/
    },
    /* 93 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const LIBRARY = __webpack_require__(45)
      const $export = __webpack_require__(18)
      const redefine = __webpack_require__(92)
      const hide = __webpack_require__(28)
      const Iterators = __webpack_require__(33)
      const $iterCreate = __webpack_require__(202)
      const setToStringTag = __webpack_require__(56)
      const getPrototypeOf = __webpack_require__(89)
      const ITERATOR = __webpack_require__(8)('iterator')
      const BUGGY = !([].keys && 'next' in [].keys()) // Safari has buggy iterators w/o `next`
      const FF_ITERATOR = '@@iterator'
      const KEYS = 'keys'
      const VALUES = 'values'

      const returnThis = function () { return this }

      module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
        $iterCreate(Constructor, NAME, next)
        const getMethod = function (kind) {
          if (!BUGGY && kind in proto) return proto[kind]
          switch (kind) {
            case KEYS:
              return function keys () { return new Constructor(this, kind) }
            case VALUES:
              return function values () { return new Constructor(this, kind) }
          }
          return function entries () { return new Constructor(this, kind) }
        }
        const TAG = NAME + ' Iterator'
        const DEF_VALUES = DEFAULT == VALUES
        let VALUES_BUG = false
        var proto = Base.prototype
        const $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT]
        let $default = $native || getMethod(DEFAULT)
        const $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined
        const $anyNative = NAME == 'Array' ? proto.entries || $native : $native
        let methods, key, IteratorPrototype
        // Fix native
        if ($anyNative) {
          IteratorPrototype = getPrototypeOf($anyNative.call(new Base()))
          if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
            // Set @@toStringTag to native iterators
            setToStringTag(IteratorPrototype, TAG, true)
            // fix for some old engines
            if (!LIBRARY && typeof IteratorPrototype[ITERATOR] !== 'function') hide(IteratorPrototype, ITERATOR, returnThis)
          }
        }
        // fix Array#{values, @@iterator}.name in V8 / FF
        if (DEF_VALUES && $native && $native.name !== VALUES) {
          VALUES_BUG = true
          $default = function values () { return $native.call(this) }
        }
        // Define iterator
        if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
          hide(proto, ITERATOR, $default)
        }
        // Plug for library
        Iterators[NAME] = $default
        Iterators[TAG] = returnThis
        if (DEFAULT) {
          methods = {
            values: DEF_VALUES ? $default : getMethod(VALUES),
            keys: IS_SET ? $default : getMethod(KEYS),
            entries: $entries
          }
          if (FORCED) {
            for (key in methods) {
              if (!(key in proto)) redefine(proto, key, methods[key])
            }
          } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods)
        }
        return methods
      }
      /***/
    },
    /* 94 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const $at = __webpack_require__(203)(true)

      // 21.1.3.27 String.prototype[@@iterator]()
      __webpack_require__(93)(String, 'String', function (iterated) {
        this._t = String(iterated) // target
        this._i = 0 // next index
        // 21.1.5.2.1 %StringIteratorPrototype%.next()
      }, function () {
        const O = this._t
        const index = this._i
        let point
        if (index >= O.length) return { value: undefined, done: true }
        point = $at(O, index)
        this._i += point.length
        return { value: point, done: false }
      })
      /***/
    },
    /* 95 */
    /***/
    function (module, exports, __webpack_require__) {
      const isObject = __webpack_require__(26)
      const document = __webpack_require__(13).document
      // typeof document.createElement is 'object' in old IE
      const is = isObject(document) && isObject(document.createElement)
      module.exports = function (it) {
        return is ? document.createElement(it) : {}
      }
      /***/
    },
    /* 96 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = !__webpack_require__(17) && !__webpack_require__(36)(function () {
        return Object.defineProperty(__webpack_require__(95)('div'), 'a', { get: function () { return 7 } }).a != 7
      })
      /***/
    },
    /* 97 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      // 25.4.1.5 NewPromiseCapability(C)
      const aFunction = __webpack_require__(47)

      function PromiseCapability (C) {
        let resolve, reject
        this.promise = new C(function ($$resolve, $$reject) {
          if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor')
          resolve = $$resolve
          reject = $$reject
        })
        this.resolve = aFunction(resolve)
        this.reject = aFunction(reject)
      }

      module.exports.f = function (C) {
        return new PromiseCapability(C)
      }
      /***/
    },
    /* 98 */
    /***/
    function (module, exports, __webpack_require__) {
      const ctx = __webpack_require__(48)
      const invoke = __webpack_require__(228)
      const html = __webpack_require__(104)
      const cel = __webpack_require__(76)
      const global = __webpack_require__(5)
      const process = global.process
      let setTask = global.setImmediate
      let clearTask = global.clearImmediate
      const MessageChannel = global.MessageChannel
      const Dispatch = global.Dispatch
      let counter = 0
      const queue = {}
      const ONREADYSTATECHANGE = 'onreadystatechange'
      let defer, channel, port
      const run = function () {
        const id = +this
        // eslint-disable-next-line no-prototype-builtins
        if (queue.hasOwnProperty(id)) {
          const fn = queue[id]
          delete queue[id]
          fn()
        }
      }
      const listener = function (event) {
        run.call(event.data)
      }
      // Node.js 0.9+ & IE10+ has setImmediate, otherwise:
      if (!setTask || !clearTask) {
        setTask = function setImmediate (fn) {
          const args = []
          let i = 1
          while (arguments.length > i) args.push(arguments[i++])
          queue[++counter] = function () {
            // eslint-disable-next-line no-new-func
            invoke(typeof fn === 'function' ? fn : Function(fn), args)
          }
          defer(counter)
          return counter
        }
        clearTask = function clearImmediate (id) {
          delete queue[id]
        }
        // Node.js 0.8-
        if (__webpack_require__(39)(process) == 'process') {
          defer = function (id) {
            process.nextTick(ctx(run, id, 1))
          }
          // Sphere (JS game engine) Dispatch API
        } else if (Dispatch && Dispatch.now) {
          defer = function (id) {
            Dispatch.now(ctx(run, id, 1))
          }
          // Browsers with MessageChannel, includes WebWorkers
        } else if (MessageChannel) {
          channel = new MessageChannel()
          port = channel.port2
          channel.port1.onmessage = listener
          defer = ctx(port.postMessage, port, 1)
          // Browsers with postMessage, skip WebWorkers
          // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
        } else if (global.addEventListener && typeof postMessage === 'function' && !global.importScripts) {
          defer = function (id) {
            global.postMessage(id + '', '*')
          }
          global.addEventListener('message', listener, false)
          // IE8-
        } else if (ONREADYSTATECHANGE in cel('script')) {
          defer = function (id) {
            html.appendChild(cel('script'))[ONREADYSTATECHANGE] = function () {
              html.removeChild(this)
              run.call(id)
            }
          }
          // Rest old browsers
        } else {
          defer = function (id) {
            setTimeout(ctx(run, id, 1), 0)
          }
        }
      }
      module.exports = {
        set: setTask,
        clear: clearTask
      }
      /***/
    },
    /* 99 */
    /***/
    function (module, exports, __webpack_require__) {
      // 22.1.3.31 Array.prototype[@@unscopables]
      const UNSCOPABLES = __webpack_require__(4)('unscopables')
      const ArrayProto = Array.prototype
      if (ArrayProto[UNSCOPABLES] == undefined) __webpack_require__(22)(ArrayProto, UNSCOPABLES, {})
      module.exports = function (key) {
        ArrayProto[UNSCOPABLES][key] = true
      }
      /***/
    },
    /* 100 */
    /***/
    function (module, exports, __webpack_require__) {
      // 7.1.13 ToObject(argument)
      const defined = __webpack_require__(73)
      module.exports = function (it) {
        return Object(defined(it))
      }
      /***/
    },
    /* 101 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const LIBRARY = __webpack_require__(40)
      const $export = __webpack_require__(31)
      const redefine = __webpack_require__(30)
      const hide = __webpack_require__(22)
      const Iterators = __webpack_require__(37)
      const $iterCreate = __webpack_require__(240)
      const setToStringTag = __webpack_require__(46)
      const getPrototypeOf = __webpack_require__(239)
      const ITERATOR = __webpack_require__(4)('iterator')
      const BUGGY = !([].keys && 'next' in [].keys()) // Safari has buggy iterators w/o `next`
      const FF_ITERATOR = '@@iterator'
      const KEYS = 'keys'
      const VALUES = 'values'

      const returnThis = function () { return this }

      module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
        $iterCreate(Constructor, NAME, next)
        const getMethod = function (kind) {
          if (!BUGGY && kind in proto) return proto[kind]
          switch (kind) {
            case KEYS:
              return function keys () { return new Constructor(this, kind) }
            case VALUES:
              return function values () { return new Constructor(this, kind) }
          }
          return function entries () { return new Constructor(this, kind) }
        }
        const TAG = NAME + ' Iterator'
        const DEF_VALUES = DEFAULT == VALUES
        let VALUES_BUG = false
        var proto = Base.prototype
        const $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT]
        let $default = $native || getMethod(DEFAULT)
        const $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined
        const $anyNative = NAME == 'Array' ? proto.entries || $native : $native
        let methods, key, IteratorPrototype
        // Fix native
        if ($anyNative) {
          IteratorPrototype = getPrototypeOf($anyNative.call(new Base()))
          if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
            // Set @@toStringTag to native iterators
            setToStringTag(IteratorPrototype, TAG, true)
            // fix for some old engines
            if (!LIBRARY && typeof IteratorPrototype[ITERATOR] !== 'function') hide(IteratorPrototype, ITERATOR, returnThis)
          }
        }
        // fix Array#{values, @@iterator}.name in V8 / FF
        if (DEF_VALUES && $native && $native.name !== VALUES) {
          VALUES_BUG = true
          $default = function values () { return $native.call(this) }
        }
        // Define iterator
        if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
          hide(proto, ITERATOR, $default)
        }
        // Plug for library
        Iterators[NAME] = $default
        Iterators[TAG] = returnThis
        if (DEFAULT) {
          methods = {
            values: DEF_VALUES ? $default : getMethod(VALUES),
            keys: IS_SET ? $default : getMethod(KEYS),
            entries: $entries
          }
          if (FORCED) {
            for (key in methods) {
              if (!(key in proto)) redefine(proto, key, methods[key])
            }
          } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods)
        }
        return methods
      }
      /***/
    },
    /* 102 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      // 19.1.3.6 Object.prototype.toString()
      const classof = __webpack_require__(68)
      const test = {}
      test[__webpack_require__(4)('toStringTag')] = 'z'
      if (test + '' != '[object z]') {
        __webpack_require__(30)(Object.prototype, 'toString', function toString () {
          return '[object ' + classof(this) + ']'
        }, true)
      }
      /***/
    },
    /* 103 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
      const $keys = __webpack_require__(109)
      const hiddenKeys = __webpack_require__(70).concat('length', 'prototype')

      exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames (O) {
        return $keys(O, hiddenKeys)
      }
      /***/
    },
    /* 104 */
    /***/
    function (module, exports, __webpack_require__) {
      const document = __webpack_require__(5).document
      module.exports = document && document.documentElement
      /***/
    },
    /* 105 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
      const anObject = __webpack_require__(14)
      const dPs = __webpack_require__(246)
      const enumBugKeys = __webpack_require__(70)
      const IE_PROTO = __webpack_require__(71)('IE_PROTO')
      const Empty = function () { /* empty */ }
      const PROTOTYPE = 'prototype'

      // Create object with fake `null` prototype: use iframe Object with cleared prototype
      var createDict = function () {
        // Thrash, waste and sodomy: IE GC bug
        const iframe = __webpack_require__(76)('iframe')
        let i = enumBugKeys.length
        const lt = '<'
        const gt = '>'
        let iframeDocument
        iframe.style.display = 'none'
        __webpack_require__(104).appendChild(iframe)
        iframe.src = 'javascript:' // eslint-disable-line no-script-url
        // createDict = iframe.contentWindow.Object;
        // html.removeChild(iframe);
        iframeDocument = iframe.contentWindow.document
        iframeDocument.open()
        iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt)
        iframeDocument.close()
        createDict = iframeDocument.F
        while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]]
        return createDict()
      }

      module.exports = Object.create || function create (O, Properties) {
        let result
        if (O !== null) {
          Empty[PROTOTYPE] = anObject(O)
          result = new Empty()
          Empty[PROTOTYPE] = null
          // add "__proto__" for Object.getPrototypeOf polyfill
          result[IE_PROTO] = O
        } else result = createDict()
        return Properties === undefined ? result : dPs(result, Properties)
      }
      /***/
    },
    /* 106 */
    /***/
    function (module, exports, __webpack_require__) {
      // 7.1.15 ToLength
      const toInteger = __webpack_require__(72)
      const min = Math.min
      module.exports = function (it) {
        return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0 // pow(2, 53) - 1 == 9007199254740991
      }
      /***/
    },
    /* 107 */
    /***/
    function (module, exports, __webpack_require__) {
      // false -> Array#indexOf
      // true  -> Array#includes
      const toIObject = __webpack_require__(19)
      const toLength = __webpack_require__(106)
      const toAbsoluteIndex = __webpack_require__(248)
      module.exports = function (IS_INCLUDES) {
        return function ($this, el, fromIndex) {
          const O = toIObject($this)
          const length = toLength(O.length)
          let index = toAbsoluteIndex(fromIndex, length)
          let value
          // Array#includes uses SameValueZero equality algorithm
          // eslint-disable-next-line no-self-compare
          if (IS_INCLUDES && el != el) {
            while (length > index) {
              value = O[index++]
              // eslint-disable-next-line no-self-compare
              if (value != value) return true
              // Array#indexOf ignores holes, Array#includes - not
            }
          } else {
            for (; length > index; index++) {
              if (IS_INCLUDES || index in O) {
                if (O[index] === el) return IS_INCLUDES || index || 0
              }
            }
          }
          return !IS_INCLUDES && -1
        }
      }
      /***/
    },
    /* 108 */
    /***/
    function (module, exports, __webpack_require__) {
      // fallback for non-array-like ES3 and non-enumerable old V8 strings
      const cof = __webpack_require__(39)
      // eslint-disable-next-line no-prototype-builtins
      module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
        return cof(it) == 'String' ? it.split('') : Object(it)
      }
      /***/
    },
    /* 109 */
    /***/
    function (module, exports, __webpack_require__) {
      const has = __webpack_require__(24)
      const toIObject = __webpack_require__(19)
      const arrayIndexOf = __webpack_require__(107)(false)
      const IE_PROTO = __webpack_require__(71)('IE_PROTO')

      module.exports = function (object, names) {
        const O = toIObject(object)
        let i = 0
        const result = []
        let key
        for (key in O) { if (key != IE_PROTO) has(O, key) && result.push(key) }
        // Don't enum bug & hidden keys
        while (names.length > i) {
          if (has(O, key = names[i++])) {
            ~arrayIndexOf(result, key) || result.push(key)
          }
        }
        return result
      }
      /***/
    },
    /* 110 */
    /***/
    function (module, exports, __webpack_require__) {
      exports.f = __webpack_require__(4)
      /***/
    },
    /* 111 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = !__webpack_require__(23) && !__webpack_require__(42)(function () {
        return Object.defineProperty(__webpack_require__(76)('div'), 'a', { get: function () { return 7 } }).a != 7
      })
      /***/
    },
    /* 112 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _get2 = __webpack_require__(50)

      const _get3 = _interopRequireDefault(_get2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _Draggable2 = __webpack_require__(43)

      const _Draggable3 = _interopRequireDefault(_Draggable2)

      const _SortableEvent = __webpack_require__(77)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onDragStart = Symbol('onDragStart')
      const onDragOverContainer = Symbol('onDragOverContainer')
      const onDragOver = Symbol('onDragOver')
      const onDragStop = Symbol('onDragStop')

      /**
                 * Returns announcement message when a Draggable element has been sorted with another Draggable element
                 * or moved into a new container
                 * @param {SortableSortedEvent} sortableEvent
                 * @return {String}
                 */
      function onSortableSortedDefaultAnnouncement (_ref) {
        const dragEvent = _ref.dragEvent

        const sourceText = dragEvent.source.textContent.trim() || dragEvent.source.id || 'sortable element'

        if (dragEvent.over) {
          const overText = dragEvent.over.textContent.trim() || dragEvent.over.id || 'sortable element'
          const isFollowing = dragEvent.source.compareDocumentPosition(dragEvent.over) & Node.DOCUMENT_POSITION_FOLLOWING

          if (isFollowing) {
            return 'Placed ' + sourceText + ' after ' + overText
          } else {
            return 'Placed ' + sourceText + ' before ' + overText
          }
        } else {
          // need to figure out how to compute container name
          return 'Placed ' + sourceText + ' into a different container'
        }
      }

      /**
                 * @const {Object} defaultAnnouncements
                 * @const {Function} defaultAnnouncements['sortable:sorted']
                 */
      const defaultAnnouncements = {
        'sortable:sorted': onSortableSortedDefaultAnnouncement
      }

      /**
                 * Sortable is built on top of Draggable and allows sorting of draggable elements. Sortable will keep
                 * track of the original index and emits the new index as you drag over draggable elements.
                 * @class Sortable
                 * @module Sortable
                 * @extends Draggable
                 */

      const Sortable = (function (_Draggable) {
        (0, _inherits3.default)(Sortable, _Draggable)

        /**
                     * Sortable constructor.
                     * @constructs Sortable
                     * @param {HTMLElement[]|NodeList|HTMLElement} containers - Sortable containers
                     * @param {Object} options - Options for Sortable
                     */
        function Sortable () {
          const containers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : []
          const options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          (0, _classCallCheck3.default)(this, Sortable)

          /**
                         * start index of source on drag start
                         * @property startIndex
                         * @type {Number}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (Sortable.__proto__ || Object.getPrototypeOf(Sortable)).call(this, containers, Object.assign({}, options, {
            announcements: Object.assign({}, defaultAnnouncements, options.announcements || {})
          })))

          _this.startIndex = null

          /**
                         * start container on drag start
                         * @property startContainer
                         * @type {HTMLElement}
                         * @default null
                         */
          _this.startContainer = null

          _this[onDragStart] = _this[onDragStart].bind(_this)
          _this[onDragOverContainer] = _this[onDragOverContainer].bind(_this)
          _this[onDragOver] = _this[onDragOver].bind(_this)
          _this[onDragStop] = _this[onDragStop].bind(_this)

          _this.on('drag:start', _this[onDragStart]).on('drag:over:container', _this[onDragOverContainer]).on('drag:over', _this[onDragOver]).on('drag:stop', _this[onDragStop])
          return _this
        }

        /**
                     * Destroys Sortable instance.
                     */

        (0, _createClass3.default)(Sortable, [{
          key: 'destroy',
          value: function destroy () {
            (0, _get3.default)(Sortable.prototype.__proto__ || Object.getPrototypeOf(Sortable.prototype), 'destroy', this).call(this)

            this.off('drag:start', this[onDragStart]).off('drag:over:container', this[onDragOverContainer]).off('drag:over', this[onDragOver]).off('drag:stop', this[onDragStop])
          }

          /**
                         * Returns true index of element within its container during drag operation, i.e. excluding mirror and original source
                         * @param {HTMLElement} element - An element
                         * @return {Number}
                         */

        }, {
          key: 'index',
          value: function index (element) {
            return this.getDraggableElementsForContainer(element.parentNode).indexOf(element)
          }

          /**
                         * Drag start handler
                         * @private
                         * @param {DragStartEvent} event - Drag start event
                         */

        }, {
          key: onDragStart,
          value: function value (event) {
            this.startContainer = event.source.parentNode
            this.startIndex = this.index(event.source)

            const sortableStartEvent = new _SortableEvent.SortableStartEvent({
              dragEvent: event,
              startIndex: this.startIndex,
              startContainer: this.startContainer
            })

            this.trigger(sortableStartEvent)

            if (sortableStartEvent.canceled()) {
              event.cancel()
            }
          }

          /**
                         * Drag over container handler
                         * @private
                         * @param {DragOverContainerEvent} event - Drag over container event
                         */

        }, {
          key: onDragOverContainer,
          value: function value (event) {
            if (event.canceled()) {
              return
            }

            const source = event.source
            const over = event.over
            const overContainer = event.overContainer

            const oldIndex = this.index(source)

            const sortableSortEvent = new _SortableEvent.SortableSortEvent({
              dragEvent: event,
              currentIndex: oldIndex,
              source: source,
              over: over
            })

            this.trigger(sortableSortEvent)

            if (sortableSortEvent.canceled()) {
              return
            }

            const children = this.getDraggableElementsForContainer(overContainer)
            const moves = move({ source: source, over: over, overContainer: overContainer, children: children })

            if (!moves) {
              return
            }

            const oldContainer = moves.oldContainer
            const newContainer = moves.newContainer

            const newIndex = this.index(event.source)

            const sortableSortedEvent = new _SortableEvent.SortableSortedEvent({
              dragEvent: event,
              oldIndex: oldIndex,
              newIndex: newIndex,
              oldContainer: oldContainer,
              newContainer: newContainer
            })

            this.trigger(sortableSortedEvent)
          }

          /**
                         * Drag over handler
                         * @private
                         * @param {DragOverEvent} event - Drag over event
                         */

        }, {
          key: onDragOver,
          value: function value (event) {
            if (event.over === event.originalSource || event.over === event.source) {
              return
            }

            const source = event.source
            const over = event.over
            const overContainer = event.overContainer

            const oldIndex = this.index(source)

            const sortableSortEvent = new _SortableEvent.SortableSortEvent({
              dragEvent: event,
              currentIndex: oldIndex,
              source: source,
              over: over
            })

            this.trigger(sortableSortEvent)

            if (sortableSortEvent.canceled()) {
              return
            }

            const children = this.getDraggableElementsForContainer(overContainer)
            const moves = move({ source: source, over: over, overContainer: overContainer, children: children })

            if (!moves) {
              return
            }

            const oldContainer = moves.oldContainer
            const newContainer = moves.newContainer

            const newIndex = this.index(source)

            const sortableSortedEvent = new _SortableEvent.SortableSortedEvent({
              dragEvent: event,
              oldIndex: oldIndex,
              newIndex: newIndex,
              oldContainer: oldContainer,
              newContainer: newContainer
            })

            this.trigger(sortableSortedEvent)
          }

          /**
                         * Drag stop handler
                         * @private
                         * @param {DragStopEvent} event - Drag stop event
                         */

        }, {
          key: onDragStop,
          value: function value (event) {
            const sortableStopEvent = new _SortableEvent.SortableStopEvent({
              dragEvent: event,
              oldIndex: this.startIndex,
              newIndex: this.index(event.source),
              oldContainer: this.startContainer,
              newContainer: event.source.parentNode
            })

            this.trigger(sortableStopEvent)

            this.startIndex = null
            this.startContainer = null
          }
        }])
        return Sortable
      }(_Draggable3.default))

      exports.default = Sortable

      function index (element) {
        return Array.prototype.indexOf.call(element.parentNode.children, element)
      }

      function move (_ref2) {
        const source = _ref2.source
        const over = _ref2.over
        const overContainer = _ref2.overContainer
        const children = _ref2.children

        const emptyOverContainer = !children.length
        const differentContainer = source.parentNode !== overContainer
        const sameContainer = over && !differentContainer

        if (emptyOverContainer) {
          return moveInsideEmptyContainer(source, overContainer)
        } else if (sameContainer) {
          return moveWithinContainer(source, over)
        } else if (differentContainer) {
          return moveOutsideContainer(source, over, overContainer)
        } else {
          return null
        }
      }

      function moveInsideEmptyContainer (source, overContainer) {
        const oldContainer = source.parentNode

        overContainer.appendChild(source)

        return { oldContainer: oldContainer, newContainer: overContainer }
      }

      function moveWithinContainer (source, over) {
        const oldIndex = index(source)
        const newIndex = index(over)

        if (oldIndex < newIndex) {
          source.parentNode.insertBefore(source, over.nextElementSibling)
        } else {
          source.parentNode.insertBefore(source, over)
        }

        return { oldContainer: source.parentNode, newContainer: source.parentNode }
      }

      function moveOutsideContainer (source, over, overContainer) {
        const oldContainer = source.parentNode

        if (over) {
          over.parentNode.insertBefore(source, over)
        } else {
          // need to figure out proper position
          overContainer.appendChild(source)
        }

        return { oldContainer: oldContainer, newContainer: source.parentNode }
      }
      /***/
    },
    /* 113 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.SortableStopEvent = exports.SortableSortedEvent = exports.SortableSortEvent = exports.SortableStartEvent = exports.SortableEvent = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractEvent2 = __webpack_require__(9)

      const _AbstractEvent3 = _interopRequireDefault(_AbstractEvent2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * Base sortable event
                 * @class SortableEvent
                 * @module SortableEvent
                 * @extends AbstractEvent
                 */
      const SortableEvent = exports.SortableEvent = (function (_AbstractEvent) {
        (0, _inherits3.default)(SortableEvent, _AbstractEvent)

        function SortableEvent () {
          (0, _classCallCheck3.default)(this, SortableEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SortableEvent.__proto__ || Object.getPrototypeOf(SortableEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(SortableEvent, [{
          key: 'dragEvent',

          /**
                         * Original drag event that triggered this sortable event
                         * @property dragEvent
                         * @type {DragEvent}
                         * @readonly
                         */
          get: function get () {
            return this.data.dragEvent
          }
        }])
        return SortableEvent
      }(_AbstractEvent3.default))

      /**
                 * Sortable start event
                 * @class SortableStartEvent
                 * @module SortableStartEvent
                 * @extends SortableEvent
                 */

      SortableEvent.type = 'sortable'

      const SortableStartEvent = exports.SortableStartEvent = (function (_SortableEvent) {
        (0, _inherits3.default)(SortableStartEvent, _SortableEvent)

        function SortableStartEvent () {
          (0, _classCallCheck3.default)(this, SortableStartEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SortableStartEvent.__proto__ || Object.getPrototypeOf(SortableStartEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(SortableStartEvent, [{
          key: 'startIndex',

          /**
                         * Start index of source on sortable start
                         * @property startIndex
                         * @type {Number}
                         * @readonly
                         */
          get: function get () {
            return this.data.startIndex
          }

          /**
                         * Start container on sortable start
                         * @property startContainer
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'startContainer',
          get: function get () {
            return this.data.startContainer
          }
        }])
        return SortableStartEvent
      }(SortableEvent))

      /**
                 * Sortable sort event
                 * @class SortableSortEvent
                 * @module SortableSortEvent
                 * @extends SortableEvent
                 */

      SortableStartEvent.type = 'sortable:start'
      SortableStartEvent.cancelable = true

      const SortableSortEvent = exports.SortableSortEvent = (function (_SortableEvent2) {
        (0, _inherits3.default)(SortableSortEvent, _SortableEvent2)

        function SortableSortEvent () {
          (0, _classCallCheck3.default)(this, SortableSortEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SortableSortEvent.__proto__ || Object.getPrototypeOf(SortableSortEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(SortableSortEvent, [{
          key: 'currentIndex',

          /**
                         * Index of current draggable element
                         * @property currentIndex
                         * @type {Number}
                         * @readonly
                         */
          get: function get () {
            return this.data.currentIndex
          }

          /**
                         * Draggable element you are hovering over
                         * @property over
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'over',
          get: function get () {
            return this.data.oldIndex
          }

          /**
                         * Draggable container element you are hovering over
                         * @property overContainer
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'overContainer',
          get: function get () {
            return this.data.newIndex
          }
        }])
        return SortableSortEvent
      }(SortableEvent))

      /**
                 * Sortable sorted event
                 * @class SortableSortedEvent
                 * @module SortableSortedEvent
                 * @extends SortableEvent
                 */

      SortableSortEvent.type = 'sortable:sort'
      SortableSortEvent.cancelable = true

      const SortableSortedEvent = exports.SortableSortedEvent = (function (_SortableEvent3) {
        (0, _inherits3.default)(SortableSortedEvent, _SortableEvent3)

        function SortableSortedEvent () {
          (0, _classCallCheck3.default)(this, SortableSortedEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SortableSortedEvent.__proto__ || Object.getPrototypeOf(SortableSortedEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(SortableSortedEvent, [{
          key: 'oldIndex',

          /**
                         * Index of last sorted event
                         * @property oldIndex
                         * @type {Number}
                         * @readonly
                         */
          get: function get () {
            return this.data.oldIndex
          }

          /**
                         * New index of this sorted event
                         * @property newIndex
                         * @type {Number}
                         * @readonly
                         */

        }, {
          key: 'newIndex',
          get: function get () {
            return this.data.newIndex
          }

          /**
                         * Old container of draggable element
                         * @property oldContainer
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'oldContainer',
          get: function get () {
            return this.data.oldContainer
          }

          /**
                         * New container of draggable element
                         * @property newContainer
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'newContainer',
          get: function get () {
            return this.data.newContainer
          }
        }])
        return SortableSortedEvent
      }(SortableEvent))

      /**
                 * Sortable stop event
                 * @class SortableStopEvent
                 * @module SortableStopEvent
                 * @extends SortableEvent
                 */

      SortableSortedEvent.type = 'sortable:sorted'

      const SortableStopEvent = exports.SortableStopEvent = (function (_SortableEvent4) {
        (0, _inherits3.default)(SortableStopEvent, _SortableEvent4)

        function SortableStopEvent () {
          (0, _classCallCheck3.default)(this, SortableStopEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SortableStopEvent.__proto__ || Object.getPrototypeOf(SortableStopEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(SortableStopEvent, [{
          key: 'oldIndex',

          /**
                         * Original index on sortable start
                         * @property oldIndex
                         * @type {Number}
                         * @readonly
                         */
          get: function get () {
            return this.data.oldIndex
          }

          /**
                         * New index of draggable element
                         * @property newIndex
                         * @type {Number}
                         * @readonly
                         */

        }, {
          key: 'newIndex',
          get: function get () {
            return this.data.newIndex
          }

          /**
                         * Original container of draggable element
                         * @property oldContainer
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'oldContainer',
          get: function get () {
            return this.data.oldContainer
          }

          /**
                         * New container of draggable element
                         * @property newContainer
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'newContainer',
          get: function get () {
            return this.data.newContainer
          }
        }])
        return SortableStopEvent
      }(SortableEvent))

      SortableStopEvent.type = 'sortable:stop'
      /***/
    },
    /* 114 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _SortableEvent = __webpack_require__(77)

      Object.keys(_SortableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _SortableEvent[key]
          }
        })
      })

      const _Sortable = __webpack_require__(112)

      const _Sortable2 = _interopRequireDefault(_Sortable)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Sortable2.default
      /***/
    },
    /* 115 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _get2 = __webpack_require__(50)

      const _get3 = _interopRequireDefault(_get2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _Draggable2 = __webpack_require__(43)

      const _Draggable3 = _interopRequireDefault(_Draggable2)

      const _SwappableEvent = __webpack_require__(78)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onDragStart = Symbol('onDragStart')
      const onDragOver = Symbol('onDragOver')
      const onDragStop = Symbol('onDragStop')

      /**
                 * Returns an announcement message when the Draggable element is swapped with another draggable element
                 * @param {SwappableSwappedEvent} swappableEvent
                 * @return {String}
                 */
      function onSwappableSwappedDefaultAnnouncement (_ref) {
        const dragEvent = _ref.dragEvent
        const swappedElement = _ref.swappedElement

        const sourceText = dragEvent.source.textContent.trim() || dragEvent.source.id || 'swappable element'
        const overText = swappedElement.textContent.trim() || swappedElement.id || 'swappable element'

        return 'Swapped ' + sourceText + ' with ' + overText
      }

      /**
                 * @const {Object} defaultAnnouncements
                 * @const {Function} defaultAnnouncements['swappabled:swapped']
                 */
      const defaultAnnouncements = {
        'swappabled:swapped': onSwappableSwappedDefaultAnnouncement
      }

      /**
                 * Swappable is built on top of Draggable and allows swapping of draggable elements.
                 * Order is irrelevant to Swappable.
                 * @class Swappable
                 * @module Swappable
                 * @extends Draggable
                 */

      const Swappable = (function (_Draggable) {
        (0, _inherits3.default)(Swappable, _Draggable)

        /**
                     * Swappable constructor.
                     * @constructs Swappable
                     * @param {HTMLElement[]|NodeList|HTMLElement} containers - Swappable containers
                     * @param {Object} options - Options for Swappable
                     */
        function Swappable () {
          const containers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : []
          const options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          (0, _classCallCheck3.default)(this, Swappable)

          /**
                         * Last draggable element that was dragged over
                         * @property lastOver
                         * @type {HTMLElement}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (Swappable.__proto__ || Object.getPrototypeOf(Swappable)).call(this, containers, Object.assign({}, options, {
            announcements: Object.assign({}, defaultAnnouncements, options.announcements || {})
          })))

          _this.lastOver = null

          _this[onDragStart] = _this[onDragStart].bind(_this)
          _this[onDragOver] = _this[onDragOver].bind(_this)
          _this[onDragStop] = _this[onDragStop].bind(_this)

          _this.on('drag:start', _this[onDragStart]).on('drag:over', _this[onDragOver]).on('drag:stop', _this[onDragStop])
          return _this
        }

        /**
                     * Destroys Swappable instance.
                     */

        (0, _createClass3.default)(Swappable, [{
          key: 'destroy',
          value: function destroy () {
            (0, _get3.default)(Swappable.prototype.__proto__ || Object.getPrototypeOf(Swappable.prototype), 'destroy', this).call(this)

            this.off('drag:start', this._onDragStart).off('drag:over', this._onDragOver).off('drag:stop', this._onDragStop)
          }

          /**
                         * Drag start handler
                         * @private
                         * @param {DragStartEvent} event - Drag start event
                         */

        }, {
          key: onDragStart,
          value: function value (event) {
            const swappableStartEvent = new _SwappableEvent.SwappableStartEvent({
              dragEvent: event
            })

            this.trigger(swappableStartEvent)

            if (swappableStartEvent.canceled()) {
              event.cancel()
            }
          }

          /**
                         * Drag over handler
                         * @private
                         * @param {DragOverEvent} event - Drag over event
                         */

        }, {
          key: onDragOver,
          value: function value (event) {
            if (event.over === event.originalSource || event.over === event.source || event.canceled()) {
              return
            }

            const swappableSwapEvent = new _SwappableEvent.SwappableSwapEvent({
              dragEvent: event,
              over: event.over,
              overContainer: event.overContainer
            })

            this.trigger(swappableSwapEvent)

            if (swappableSwapEvent.canceled()) {
              return
            }

            // swap originally swapped element back
            if (this.lastOver && this.lastOver !== event.over) {
              swap(this.lastOver, event.source)
            }

            if (this.lastOver === event.over) {
              this.lastOver = null
            } else {
              this.lastOver = event.over
            }

            swap(event.source, event.over)

            const swappableSwappedEvent = new _SwappableEvent.SwappableSwappedEvent({
              dragEvent: event,
              swappedElement: event.over
            })

            this.trigger(swappableSwappedEvent)
          }

          /**
                         * Drag stop handler
                         * @private
                         * @param {DragStopEvent} event - Drag stop event
                         */

        }, {
          key: onDragStop,
          value: function value (event) {
            const swappableStopEvent = new _SwappableEvent.SwappableStopEvent({
              dragEvent: event
            })

            this.trigger(swappableStopEvent)
            this.lastOver = null
          }
        }])
        return Swappable
      }(_Draggable3.default))

      exports.default = Swappable

      function withTempElement (callback) {
        const tmpElement = document.createElement('div')
        callback(tmpElement)
        tmpElement.parentNode.removeChild(tmpElement)
      }

      function swap (source, over) {
        const overParent = over.parentNode
        const sourceParent = source.parentNode

        withTempElement(function (tmpElement) {
          sourceParent.insertBefore(tmpElement, source)
          overParent.insertBefore(source, over)
          sourceParent.insertBefore(over, tmpElement)
        })
      }
      /***/
    },
    /* 116 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.SwappableStopEvent = exports.SwappableSwappedEvent = exports.SwappableSwapEvent = exports.SwappableStartEvent = exports.SwappableEvent = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractEvent2 = __webpack_require__(9)

      const _AbstractEvent3 = _interopRequireDefault(_AbstractEvent2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * Base swappable event
                 * @class SwappableEvent
                 * @module SwappableEvent
                 * @extends AbstractEvent
                 */
      const SwappableEvent = exports.SwappableEvent = (function (_AbstractEvent) {
        (0, _inherits3.default)(SwappableEvent, _AbstractEvent)

        function SwappableEvent () {
          (0, _classCallCheck3.default)(this, SwappableEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SwappableEvent.__proto__ || Object.getPrototypeOf(SwappableEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(SwappableEvent, [{
          key: 'dragEvent',

          /**
                         * Original drag event that triggered this swappable event
                         * @property dragEvent
                         * @type {DragEvent}
                         * @readonly
                         */
          get: function get () {
            return this.data.dragEvent
          }
        }])
        return SwappableEvent
      }(_AbstractEvent3.default))

      /**
                 * Swappable start event
                 * @class SwappableStartEvent
                 * @module SwappableStartEvent
                 * @extends SwappableEvent
                 */

      SwappableEvent.type = 'swappable'

      const SwappableStartEvent = exports.SwappableStartEvent = (function (_SwappableEvent) {
        (0, _inherits3.default)(SwappableStartEvent, _SwappableEvent)

        function SwappableStartEvent () {
          (0, _classCallCheck3.default)(this, SwappableStartEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SwappableStartEvent.__proto__ || Object.getPrototypeOf(SwappableStartEvent)).apply(this, arguments))
        }

        return SwappableStartEvent
      }(SwappableEvent))

      /**
                 * Swappable swap event
                 * @class SwappableSwapEvent
                 * @module SwappableSwapEvent
                 * @extends SwappableEvent
                 */

      SwappableStartEvent.type = 'swappable:start'
      SwappableStartEvent.cancelable = true

      const SwappableSwapEvent = exports.SwappableSwapEvent = (function (_SwappableEvent2) {
        (0, _inherits3.default)(SwappableSwapEvent, _SwappableEvent2)

        function SwappableSwapEvent () {
          (0, _classCallCheck3.default)(this, SwappableSwapEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SwappableSwapEvent.__proto__ || Object.getPrototypeOf(SwappableSwapEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(SwappableSwapEvent, [{
          key: 'over',

          /**
                         * Draggable element you are over
                         * @property over
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.over
          }

          /**
                         * Draggable container you are over
                         * @property overContainer
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'overContainer',
          get: function get () {
            return this.data.overContainer
          }
        }])
        return SwappableSwapEvent
      }(SwappableEvent))

      /**
                 * Swappable swapped event
                 * @class SwappableSwappedEvent
                 * @module SwappableSwappedEvent
                 * @extends SwappableEvent
                 */

      SwappableSwapEvent.type = 'swappable:swap'
      SwappableSwapEvent.cancelable = true

      const SwappableSwappedEvent = exports.SwappableSwappedEvent = (function (_SwappableEvent3) {
        (0, _inherits3.default)(SwappableSwappedEvent, _SwappableEvent3)

        function SwappableSwappedEvent () {
          (0, _classCallCheck3.default)(this, SwappableSwappedEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SwappableSwappedEvent.__proto__ || Object.getPrototypeOf(SwappableSwappedEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(SwappableSwappedEvent, [{
          key: 'swappedElement',

          /**
                         * The draggable element that you swapped with
                         * @property swappedElement
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.swappedElement
          }
        }])
        return SwappableSwappedEvent
      }(SwappableEvent))

      /**
                 * Swappable stop event
                 * @class SwappableStopEvent
                 * @module SwappableStopEvent
                 * @extends SwappableEvent
                 */

      SwappableSwappedEvent.type = 'swappable:swapped'

      const SwappableStopEvent = exports.SwappableStopEvent = (function (_SwappableEvent4) {
        (0, _inherits3.default)(SwappableStopEvent, _SwappableEvent4)

        function SwappableStopEvent () {
          (0, _classCallCheck3.default)(this, SwappableStopEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SwappableStopEvent.__proto__ || Object.getPrototypeOf(SwappableStopEvent)).apply(this, arguments))
        }

        return SwappableStopEvent
      }(SwappableEvent))

      SwappableStopEvent.type = 'swappable:stop'
      /***/
    },
    /* 117 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _SwappableEvent = __webpack_require__(78)

      Object.keys(_SwappableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _SwappableEvent[key]
          }
        })
      })

      const _Swappable = __webpack_require__(115)

      const _Swappable2 = _interopRequireDefault(_Swappable)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Swappable2.default
      /***/
    },
    /* 118 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
      const toIObject = __webpack_require__(15)
      const $getOwnPropertyDescriptor = __webpack_require__(51).f

      __webpack_require__(79)('getOwnPropertyDescriptor', function () {
        return function getOwnPropertyDescriptor (it, key) {
          return $getOwnPropertyDescriptor(toIObject(it), key)
        }
      })
      /***/
    },
    /* 119 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(118)
      const $Object = __webpack_require__(6).Object
      module.exports = function getOwnPropertyDescriptor (it, key) {
        return $Object.getOwnPropertyDescriptor(it, key)
      }
      /***/
    },
    /* 120 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = { default: __webpack_require__(119), __esModule: true }
      /***/
    },
    /* 121 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.2.9 Object.getPrototypeOf(O)
      const toObject = __webpack_require__(55)
      const $getPrototypeOf = __webpack_require__(89)

      __webpack_require__(79)('getPrototypeOf', function () {
        return function getPrototypeOf (it) {
          return $getPrototypeOf(toObject(it))
        }
      })
      /***/
    },
    /* 122 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(121)
      module.exports = __webpack_require__(6).Object.getPrototypeOf
      /***/
    },
    /* 123 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = { default: __webpack_require__(122), __esModule: true }
      /***/
    },
    /* 124 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _toConsumableArray2 = __webpack_require__(25)

      const _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2)

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _get2 = __webpack_require__(50)

      const _get3 = _interopRequireDefault(_get2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _utils = __webpack_require__(11)

      const _Draggable2 = __webpack_require__(43)

      const _Draggable3 = _interopRequireDefault(_Draggable2)

      const _DroppableEvent = __webpack_require__(80)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onDragStart = Symbol('onDragStart')
      const onDragMove = Symbol('onDragMove')
      const onDragStop = Symbol('onDragStop')
      const dropInDropzone = Symbol('dropInDropZone')
      const returnToOriginalDropzone = Symbol('returnToOriginalDropzone')
      const closestDropzone = Symbol('closestDropzone')
      const getDropzones = Symbol('getDropzones')

      /**
                 * Returns an announcement message when the Draggable element is dropped into a dropzone element
                 * @param {DroppableDroppedEvent} droppableEvent
                 * @return {String}
                 */
      function onDroppableDroppedDefaultAnnouncement (_ref) {
        const dragEvent = _ref.dragEvent
        const dropzone = _ref.dropzone

        const sourceText = dragEvent.source.textContent.trim() || dragEvent.source.id || 'draggable element'
        const dropzoneText = dropzone.textContent.trim() || dropzone.id || 'droppable element'

        return 'Dropped ' + sourceText + ' into ' + dropzoneText
      }

      /**
                 * Returns an announcement message when the Draggable element has returned to its original dropzone element
                 * @param {DroppableReturnedEvent} droppableEvent
                 * @return {String}
                 */
      function onDroppableReturnedDefaultAnnouncement (_ref2) {
        const dragEvent = _ref2.dragEvent
        const dropzone = _ref2.dropzone

        const sourceText = dragEvent.source.textContent.trim() || dragEvent.source.id || 'draggable element'
        const dropzoneText = dropzone.textContent.trim() || dropzone.id || 'droppable element'

        return 'Returned ' + sourceText + ' from ' + dropzoneText
      }

      /**
                 * @const {Object} defaultAnnouncements
                 * @const {Function} defaultAnnouncements['droppable:dropped']
                 * @const {Function} defaultAnnouncements['droppable:returned']
                 */
      const defaultAnnouncements = {
        'droppable:dropped': onDroppableDroppedDefaultAnnouncement,
        'droppable:returned': onDroppableReturnedDefaultAnnouncement
      }

      const defaultClasses = {
        'droppable:active': 'draggable-dropzone--active',
        'droppable:occupied': 'draggable-dropzone--occupied'
      }

      const defaultOptions = {
        dropzone: '.draggable-droppable'
      }

      /**
                 * Droppable is built on top of Draggable and allows dropping draggable elements
                 * into dropzone element
                 * @class Droppable
                 * @module Droppable
                 * @extends Draggable
                 */

      const Droppable = (function (_Draggable) {
        (0, _inherits3.default)(Droppable, _Draggable)

        /**
                     * Droppable constructor.
                     * @constructs Droppable
                     * @param {HTMLElement[]|NodeList|HTMLElement} containers - Droppable containers
                     * @param {Object} options - Options for Droppable
                     */
        function Droppable () {
          const containers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : []
          const options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          (0, _classCallCheck3.default)(this, Droppable)

          /**
                         * All dropzone elements on drag start
                         * @property dropzones
                         * @type {HTMLElement[]}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (Droppable.__proto__ || Object.getPrototypeOf(Droppable)).call(this, containers, Object.assign({}, defaultOptions, options, {
            classes: Object.assign({}, defaultClasses, options.classes || {}),
            announcements: Object.assign({}, defaultAnnouncements, options.announcements || {})
          })))

          _this.dropzones = null

          /**
                         * Last dropzone element that the source was dropped into
                         * @property lastDropzone
                         * @type {HTMLElement}
                         */
          _this.lastDropzone = null

          /**
                         * Initial dropzone element that the source was drag from
                         * @property initialDropzone
                         * @type {HTMLElement}
                         */
          _this.initialDropzone = null

          _this[onDragStart] = _this[onDragStart].bind(_this)
          _this[onDragMove] = _this[onDragMove].bind(_this)
          _this[onDragStop] = _this[onDragStop].bind(_this)

          _this.on('drag:start', _this[onDragStart]).on('drag:move', _this[onDragMove]).on('drag:stop', _this[onDragStop])
          return _this
        }

        /**
                     * Destroys Droppable instance.
                     */

        (0, _createClass3.default)(Droppable, [{
          key: 'destroy',
          value: function destroy () {
            (0, _get3.default)(Droppable.prototype.__proto__ || Object.getPrototypeOf(Droppable.prototype), 'destroy', this).call(this)

            this.off('drag:start', this[onDragStart]).off('drag:move', this[onDragMove]).off('drag:stop', this[onDragStop])
          }

          /**
                         * Drag start handler
                         * @private
                         * @param {DragStartEvent} event - Drag start event
                         */

        }, {
          key: onDragStart,
          value: function value (event) {
            if (event.canceled()) {
              return
            }

            this.dropzones = [].concat((0, _toConsumableArray3.default)(this[getDropzones]()))
            const dropzone = (0, _utils.closest)(event.sensorEvent.target, this.options.dropzone)

            if (!dropzone) {
              event.cancel()
              return
            }

            const droppableStartEvent = new _DroppableEvent.DroppableStartEvent({
              dragEvent: event,
              dropzone: dropzone
            })

            this.trigger(droppableStartEvent)

            if (droppableStartEvent.canceled()) {
              event.cancel()
              return
            }

            this.initialDropzone = dropzone

            let _iteratorNormalCompletion = true
            let _didIteratorError = false
            let _iteratorError

            try {
              for (var _iterator = this.dropzones[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                const dropzoneElement = _step.value

                if (dropzoneElement.classList.contains(this.getClassNameFor('droppable:occupied'))) {
                  continue
                }

                dropzoneElement.classList.add(this.getClassNameFor('droppable:active'))
              }
            } catch (err) {
              _didIteratorError = true
              _iteratorError = err
            } finally {
              try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                  _iterator.return()
                }
              } finally {
                if (_didIteratorError) {
                  throw _iteratorError
                }
              }
            }
          }

          /**
                         * Drag move handler
                         * @private
                         * @param {DragMoveEvent} event - Drag move event
                         */

        }, {
          key: onDragMove,
          value: function value (event) {
            if (event.canceled()) {
              return
            }

            const dropzone = this[closestDropzone](event.sensorEvent.target)
            const overEmptyDropzone = dropzone && !dropzone.classList.contains(this.getClassNameFor('droppable:occupied'))

            if (overEmptyDropzone && this[dropInDropzone](event, dropzone)) {
              this.lastDropzone = dropzone
            } else if ((!dropzone || dropzone === this.initialDropzone) && this.lastDropzone) {
              this[returnToOriginalDropzone](event)
              this.lastDropzone = null
            }
          }

          /**
                         * Drag stop handler
                         * @private
                         * @param {DragStopEvent} event - Drag stop event
                         */

        }, {
          key: onDragStop,
          value: function value (event) {
            const droppableStopEvent = new _DroppableEvent.DroppableStopEvent({
              dragEvent: event,
              dropzone: this.lastDropzone || this.initialDropzone
            })

            this.trigger(droppableStopEvent)

            const occupiedClass = this.getClassNameFor('droppable:occupied')

            let _iteratorNormalCompletion2 = true
            let _didIteratorError2 = false
            let _iteratorError2

            try {
              for (var _iterator2 = this.dropzones[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                const dropzone = _step2.value

                dropzone.classList.remove(this.getClassNameFor('droppable:active'))
              }
            } catch (err) {
              _didIteratorError2 = true
              _iteratorError2 = err
            } finally {
              try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                  _iterator2.return()
                }
              } finally {
                if (_didIteratorError2) {
                  throw _iteratorError2
                }
              }
            }

            if (this.lastDropzone && this.lastDropzone !== this.initialDropzone) {
              this.initialDropzone.classList.remove(occupiedClass)
            }

            this.dropzones = null
            this.lastDropzone = null
            this.initialDropzone = null
          }

          /**
                         * Drops a draggable element into a dropzone element
                         * @private
                         * @param {DragMoveEvent} event - Drag move event
                         * @param {HTMLElement} dropzone - Dropzone element to drop draggable into
                         */

        }, {
          key: dropInDropzone,
          value: function value (event, dropzone) {
            const droppableDroppedEvent = new _DroppableEvent.DroppableDroppedEvent({
              dragEvent: event,
              dropzone: dropzone
            })

            this.trigger(droppableDroppedEvent)

            if (droppableDroppedEvent.canceled()) {
              return false
            }

            const occupiedClass = this.getClassNameFor('droppable:occupied')

            if (this.lastDropzone) {
              this.lastDropzone.classList.remove(occupiedClass)
            }

            dropzone.appendChild(event.source)
            dropzone.classList.add(occupiedClass)

            return true
          }

          /**
                         * Moves the previously dropped element back into its original dropzone
                         * @private
                         * @param {DragMoveEvent} event - Drag move event
                         */

        }, {
          key: returnToOriginalDropzone,
          value: function value (event) {
            const droppableReturnedEvent = new _DroppableEvent.DroppableReturnedEvent({
              dragEvent: event,
              dropzone: this.lastDropzone
            })

            this.trigger(droppableReturnedEvent)

            if (droppableReturnedEvent.canceled()) {
              return
            }

            this.initialDropzone.appendChild(event.source)
            this.lastDropzone.classList.remove(this.getClassNameFor('droppable:occupied'))
          }

          /**
                         * Returns closest dropzone element for even target
                         * @private
                         * @param {HTMLElement} target - Event target
                         * @return {HTMLElement|null}
                         */

        }, {
          key: closestDropzone,
          value: function value (target) {
            if (!this.dropzones) {
              return null
            }

            return (0, _utils.closest)(target, this.dropzones)
          }

          /**
                         * Returns all current dropzone elements for this draggable instance
                         * @private
                         * @return {NodeList|HTMLElement[]|Array}
                         */

        }, {
          key: getDropzones,
          value: function value () {
            const dropzone = this.options.dropzone

            if (typeof dropzone === 'string') {
              return document.querySelectorAll(dropzone)
            } else if (dropzone instanceof NodeList || dropzone instanceof Array) {
              return dropzone
            } else if (typeof dropzone === 'function') {
              return dropzone()
            } else {
              return []
            }
          }
        }])
        return Droppable
      }(_Draggable3.default))

      exports.default = Droppable
      /***/
    },
    /* 125 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.DroppableStopEvent = exports.DroppableReturnedEvent = exports.DroppableDroppedEvent = exports.DroppableStartEvent = exports.DroppableEvent = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractEvent2 = __webpack_require__(9)

      const _AbstractEvent3 = _interopRequireDefault(_AbstractEvent2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * Base droppable event
                 * @class DroppableEvent
                 * @module DroppableEvent
                 * @extends AbstractEvent
                 */
      const DroppableEvent = exports.DroppableEvent = (function (_AbstractEvent) {
        (0, _inherits3.default)(DroppableEvent, _AbstractEvent)

        function DroppableEvent () {
          (0, _classCallCheck3.default)(this, DroppableEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DroppableEvent.__proto__ || Object.getPrototypeOf(DroppableEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DroppableEvent, [{
          key: 'dragEvent',

          /**
                         * Original drag event that triggered this droppable event
                         * @property dragEvent
                         * @type {DragEvent}
                         * @readonly
                         */
          get: function get () {
            return this.data.dragEvent
          }
        }])
        return DroppableEvent
      }(_AbstractEvent3.default))

      /**
                 * Droppable start event
                 * @class DroppableStartEvent
                 * @module DroppableStartEvent
                 * @extends DroppableEvent
                 */

      DroppableEvent.type = 'droppable'

      const DroppableStartEvent = exports.DroppableStartEvent = (function (_DroppableEvent) {
        (0, _inherits3.default)(DroppableStartEvent, _DroppableEvent)

        function DroppableStartEvent () {
          (0, _classCallCheck3.default)(this, DroppableStartEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DroppableStartEvent.__proto__ || Object.getPrototypeOf(DroppableStartEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DroppableStartEvent, [{
          key: 'dropzone',

          /**
                         * The initial dropzone element of the currently dragging draggable element
                         * @property dropzone
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.dropzone
          }
        }])
        return DroppableStartEvent
      }(DroppableEvent))

      /**
                 * Droppable dropped event
                 * @class DroppableDroppedEvent
                 * @module DroppableDroppedEvent
                 * @extends DroppableEvent
                 */

      DroppableStartEvent.type = 'droppable:start'
      DroppableStartEvent.cancelable = true

      const DroppableDroppedEvent = exports.DroppableDroppedEvent = (function (_DroppableEvent2) {
        (0, _inherits3.default)(DroppableDroppedEvent, _DroppableEvent2)

        function DroppableDroppedEvent () {
          (0, _classCallCheck3.default)(this, DroppableDroppedEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DroppableDroppedEvent.__proto__ || Object.getPrototypeOf(DroppableDroppedEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DroppableDroppedEvent, [{
          key: 'dropzone',

          /**
                         * The dropzone element you dropped the draggable element into
                         * @property dropzone
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.dropzone
          }
        }])
        return DroppableDroppedEvent
      }(DroppableEvent))

      /**
                 * Droppable returned event
                 * @class DroppableReturnedEvent
                 * @module DroppableReturnedEvent
                 * @extends DroppableEvent
                 */

      DroppableDroppedEvent.type = 'droppable:dropped'
      DroppableDroppedEvent.cancelable = true

      const DroppableReturnedEvent = exports.DroppableReturnedEvent = (function (_DroppableEvent3) {
        (0, _inherits3.default)(DroppableReturnedEvent, _DroppableEvent3)

        function DroppableReturnedEvent () {
          (0, _classCallCheck3.default)(this, DroppableReturnedEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DroppableReturnedEvent.__proto__ || Object.getPrototypeOf(DroppableReturnedEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DroppableReturnedEvent, [{
          key: 'dropzone',

          /**
                         * The dropzone element you dragged away from
                         * @property dropzone
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.dropzone
          }
        }])
        return DroppableReturnedEvent
      }(DroppableEvent))

      /**
                 * Droppable stop event
                 * @class DroppableStopEvent
                 * @module DroppableStopEvent
                 * @extends DroppableEvent
                 */

      DroppableReturnedEvent.type = 'droppable:returned'
      DroppableReturnedEvent.cancelable = true

      const DroppableStopEvent = exports.DroppableStopEvent = (function (_DroppableEvent4) {
        (0, _inherits3.default)(DroppableStopEvent, _DroppableEvent4)

        function DroppableStopEvent () {
          (0, _classCallCheck3.default)(this, DroppableStopEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DroppableStopEvent.__proto__ || Object.getPrototypeOf(DroppableStopEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DroppableStopEvent, [{
          key: 'dropzone',

          /**
                         * The final dropzone element of the draggable element
                         * @property dropzone
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.dropzone
          }
        }])
        return DroppableStopEvent
      }(DroppableEvent))

      DroppableStopEvent.type = 'droppable:stop'
      DroppableStopEvent.cancelable = true
      /***/
    },
    /* 126 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _DroppableEvent = __webpack_require__(80)

      Object.keys(_DroppableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _DroppableEvent[key]
          }
        })
      })

      const _Droppable = __webpack_require__(124)

      const _Droppable2 = _interopRequireDefault(_Droppable)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Droppable2.default
      /***/
    },
    /* 127 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _toConsumableArray2 = __webpack_require__(25)

      const _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2)

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * The Emitter is a simple emitter class that provides you with `on()`, `off()` and `trigger()` methods
                 * @class Emitter
                 * @module Emitter
                 */
      const Emitter = (function () {
        function Emitter () {
          (0, _classCallCheck3.default)(this, Emitter)

          this.callbacks = {}
        }

        /**
                     * Registers callbacks by event name
                     * @param {String} type
                     * @param {...Function} callbacks
                     */

        (0, _createClass3.default)(Emitter, [{
          key: 'on',
          value: function on (type) {
            let _callbacks$type

            if (!this.callbacks[type]) {
              this.callbacks[type] = []
            }

            for (var _len = arguments.length, callbacks = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
              callbacks[_key - 1] = arguments[_key]
            }

            (_callbacks$type = this.callbacks[type]).push.apply(_callbacks$type, callbacks)

            return this
          }

          /**
                         * Unregisters callbacks by event name
                         * @param {String} type
                         * @param {Function} callback
                         */

        }, {
          key: 'off',
          value: function off (type, callback) {
            if (!this.callbacks[type]) {
              return null
            }

            const copy = this.callbacks[type].slice(0)

            for (let i = 0; i < copy.length; i++) {
              if (callback === copy[i]) {
                this.callbacks[type].splice(i, 1)
              }
            }

            return this
          }

          /**
                         * Triggers event callbacks by event object
                         * @param {AbstractEvent} event
                         */

        }, {
          key: 'trigger',
          value: function trigger (event) {
            if (!this.callbacks[event.type]) {
              return null
            }

            const callbacks = [].concat((0, _toConsumableArray3.default)(this.callbacks[event.type]))
            const caughtErrors = []

            for (let i = callbacks.length - 1; i >= 0; i--) {
              const callback = callbacks[i]

              try {
                callback(event)
              } catch (error) {
                caughtErrors.push(error)
              }
            }

            if (caughtErrors.length) {
              /* eslint-disable no-console */
              console.error("Draggable caught errors while triggering '" + event.type + "'", caughtErrors)
              /* eslint-disable no-console */
            }

            return this
          }
        }])
        return Emitter
      }())

      exports.default = Emitter
      /***/
    },
    /* 128 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _Emitter = __webpack_require__(127)

      const _Emitter2 = _interopRequireDefault(_Emitter)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Emitter2.default
      /***/
    },
    /* 129 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = undefined

      const _toConsumableArray2 = __webpack_require__(25)

      const _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2)

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _utils = __webpack_require__(11)

      const _Plugins = __webpack_require__(81)

      const _Emitter = __webpack_require__(128)

      const _Emitter2 = _interopRequireDefault(_Emitter)

      const _Sensors = __webpack_require__(65)

      const _DraggableEvent = __webpack_require__(82)

      const _DragEvent = __webpack_require__(83)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onDragStart = Symbol('onDragStart')
      const onDragMove = Symbol('onDragMove')
      const onDragStop = Symbol('onDragStop')
      const onDragPressure = Symbol('onDragPressure')

      /**
                 * @const {Object} defaultAnnouncements
                 * @const {Function} defaultAnnouncements['drag:start']
                 * @const {Function} defaultAnnouncements['drag:stop']
                 */
      const defaultAnnouncements = {
        'drag:start': function dragStart (event) {
          return 'Picked up ' + (event.source.textContent.trim() || event.source.id || 'draggable element')
        },
        'drag:stop': function dragStop (event) {
          return 'Released ' + (event.source.textContent.trim() || event.source.id || 'draggable element')
        }
      }

      const defaultClasses = {
        'container:dragging': 'draggable-container--is-dragging',
        'source:dragging': 'draggable-source--is-dragging',
        'source:placed': 'draggable-source--placed',
        'container:placed': 'draggable-container--placed',
        'body:dragging': 'draggable--is-dragging',
        'draggable:over': 'draggable--over',
        'container:over': 'draggable-container--over',
        'source:original': 'draggable--original',
        mirror: 'draggable-mirror'
      }

      const defaultOptions = exports.defaultOptions = {
        draggable: '.draggable-source',
        handle: null,
        delay: 100,
        placedTimeout: 800,
        plugins: [],
        sensors: []
      }

      /**
                 * This is the core draggable library that does the heavy lifting
                 * @class Draggable
                 * @module Draggable
                 */

      const Draggable = (function () {
        /**
                     * Draggable constructor.
                     * @constructs Draggable
                     * @param {HTMLElement[]|NodeList|HTMLElement} containers - Draggable containers
                     * @param {Object} options - Options for draggable
                     */
        function Draggable () {
          const _this = this

          const containers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [document.body]
          const options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          (0, _classCallCheck3.default)(this, Draggable)

          /**
                         * Draggable containers
                         * @property containers
                         * @type {HTMLElement[]}
                         */
          if (containers instanceof NodeList || containers instanceof Array) {
            this.containers = [].concat((0, _toConsumableArray3.default)(containers))
          } else if (containers instanceof HTMLElement) {
            this.containers = [containers]
          } else {
            throw new Error('Draggable containers are expected to be of type `NodeList`, `HTMLElement[]` or `HTMLElement`')
          }

          this.options = Object.assign({}, defaultOptions, options, {
            classes: Object.assign({}, defaultClasses, options.classes || {}),
            announcements: Object.assign({}, defaultAnnouncements, options.announcements || {})
          })

          /**
                         * Draggables event emitter
                         * @property emitter
                         * @type {Emitter}
                         */
          this.emitter = new _Emitter2.default()

          /**
                         * Current drag state
                         * @property dragging
                         * @type {Boolean}
                         */
          this.dragging = false

          /**
                         * Active plugins
                         * @property plugins
                         * @type {Plugin[]}
                         */
          this.plugins = []

          /**
                         * Active sensors
                         * @property sensors
                         * @type {Sensor[]}
                         */
          this.sensors = []

          this[onDragStart] = this[onDragStart].bind(this)
          this[onDragMove] = this[onDragMove].bind(this)
          this[onDragStop] = this[onDragStop].bind(this)
          this[onDragPressure] = this[onDragPressure].bind(this)

          document.addEventListener('drag:start', this[onDragStart], true)
          document.addEventListener('drag:move', this[onDragMove], true)
          document.addEventListener('drag:stop', this[onDragStop], true)
          document.addEventListener('drag:pressure', this[onDragPressure], true)

          const defaultPlugins = Object.values(Draggable.Plugins).map(function (Plugin) {
            return Plugin
          })
          const defaultSensors = [_Sensors.MouseSensor, _Sensors.TouchSensor]

          this.addPlugin.apply(this, [].concat((0, _toConsumableArray3.default)(defaultPlugins), (0, _toConsumableArray3.default)(this.options.plugins)))
          this.addSensor.apply(this, [].concat(defaultSensors, (0, _toConsumableArray3.default)(this.options.sensors)))

          const draggableInitializedEvent = new _DraggableEvent.DraggableInitializedEvent({
            draggable: this
          })

          this.on('mirror:created', function (_ref) {
            const mirror = _ref.mirror
            return _this.mirror = mirror
          })
          this.on('mirror:destroy', function () {
            return _this.mirror = null
          })

          this.trigger(draggableInitializedEvent)
        }

        /**
                     * Destroys Draggable instance. This removes all internal event listeners and
                     * deactivates sensors and plugins
                     */

        /**
                     * Default plugins draggable uses
                     * @static
                     * @property {Object} Plugins
                     * @property {Announcement} Plugins.Announcement
                     * @property {Focusable} Plugins.Focusable
                     * @property {Mirror} Plugins.Mirror
                     * @property {Scrollable} Plugins.Scrollable
                     * @type {Object}
                     */

        (0, _createClass3.default)(Draggable, [{
          key: 'destroy',
          value: function destroy () {
            document.removeEventListener('drag:start', this[onDragStart], true)
            document.removeEventListener('drag:move', this[onDragMove], true)
            document.removeEventListener('drag:stop', this[onDragStop], true)
            document.removeEventListener('drag:pressure', this[onDragPressure], true)

            const draggableDestroyEvent = new _DraggableEvent.DraggableDestroyEvent({
              draggable: this
            })

            this.trigger(draggableDestroyEvent)

            this.removePlugin.apply(this, (0, _toConsumableArray3.default)(this.plugins.map(function (plugin) {
              return plugin.constructor
            })))
            this.removeSensor.apply(this, (0, _toConsumableArray3.default)(this.sensors.map(function (sensor) {
              return sensor.constructor
            })))
          }

          /**
                         * Adds plugin to this draggable instance. This will end up calling the attach method of the plugin
                         * @param {...typeof Plugin} plugins - Plugins that you want attached to draggable
                         * @return {Draggable}
                         * @example draggable.addPlugin(CustomA11yPlugin, CustomMirrorPlugin)
                         */

        }, {
          key: 'addPlugin',
          value: function addPlugin () {
            const _this2 = this

            for (var _len = arguments.length, plugins = Array(_len), _key = 0; _key < _len; _key++) {
              plugins[_key] = arguments[_key]
            }

            const activePlugins = plugins.map(function (Plugin) {
              return new Plugin(_this2)
            })

            activePlugins.forEach(function (plugin) {
              return plugin.attach()
            })
            this.plugins = [].concat((0, _toConsumableArray3.default)(this.plugins), (0, _toConsumableArray3.default)(activePlugins))

            return this
          }

          /**
                         * Removes plugins that are already attached to this draggable instance. This will end up calling
                         * the detach method of the plugin
                         * @param {...typeof Plugin} plugins - Plugins that you want detached from draggable
                         * @return {Draggable}
                         * @example draggable.removePlugin(MirrorPlugin, CustomMirrorPlugin)
                         */

        }, {
          key: 'removePlugin',
          value: function removePlugin () {
            for (var _len2 = arguments.length, plugins = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
              plugins[_key2] = arguments[_key2]
            }

            const removedPlugins = this.plugins.filter(function (plugin) {
              return plugins.includes(plugin.constructor)
            })

            removedPlugins.forEach(function (plugin) {
              return plugin.detach()
            })
            this.plugins = this.plugins.filter(function (plugin) {
              return !plugins.includes(plugin.constructor)
            })

            return this
          }

          /**
                         * Adds sensors to this draggable instance. This will end up calling the attach method of the sensor
                         * @param {...typeof Sensor} sensors - Sensors that you want attached to draggable
                         * @return {Draggable}
                         * @example draggable.addSensor(ForceTouchSensor, CustomSensor)
                         */

        }, {
          key: 'addSensor',
          value: function addSensor () {
            const _this3 = this

            for (var _len3 = arguments.length, sensors = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
              sensors[_key3] = arguments[_key3]
            }

            const activeSensors = sensors.map(function (Sensor) {
              return new Sensor(_this3.containers, _this3.options)
            })

            activeSensors.forEach(function (sensor) {
              return sensor.attach()
            })
            this.sensors = [].concat((0, _toConsumableArray3.default)(this.sensors), (0, _toConsumableArray3.default)(activeSensors))

            return this
          }

          /**
                         * Removes sensors that are already attached to this draggable instance. This will end up calling
                         * the detach method of the sensor
                         * @param {...typeof Sensor} sensors - Sensors that you want attached to draggable
                         * @return {Draggable}
                         * @example draggable.removeSensor(TouchSensor, DragSensor)
                         */

        }, {
          key: 'removeSensor',
          value: function removeSensor () {
            for (var _len4 = arguments.length, sensors = Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
              sensors[_key4] = arguments[_key4]
            }

            const removedSensors = this.sensors.filter(function (sensor) {
              return sensors.includes(sensor.constructor)
            })

            removedSensors.forEach(function (sensor) {
              return sensor.detach()
            })
            this.sensors = this.sensors.filter(function (sensor) {
              return !sensors.includes(sensor.constructor)
            })

            return this
          }

          /**
                         * Adds container to this draggable instance
                         * @param {...HTMLElement} containers - Containers you want to add to draggable
                         * @return {Draggable}
                         * @example draggable.addContainer(document.body)
                         */

        }, {
          key: 'addContainer',
          value: function addContainer () {
            for (var _len5 = arguments.length, containers = Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
              containers[_key5] = arguments[_key5]
            }

            this.containers = [].concat((0, _toConsumableArray3.default)(this.containers), containers)
            this.sensors.forEach(function (sensor) {
              return sensor.addContainer.apply(sensor, containers)
            })
            return this
          }

          /**
                         * Removes container from this draggable instance
                         * @param {...HTMLElement} containers - Containers you want to remove from draggable
                         * @return {Draggable}
                         * @example draggable.removeContainer(document.body)
                         */

        }, {
          key: 'removeContainer',
          value: function removeContainer () {
            for (var _len6 = arguments.length, containers = Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
              containers[_key6] = arguments[_key6]
            }

            this.containers = this.containers.filter(function (container) {
              return !containers.includes(container)
            })
            this.sensors.forEach(function (sensor) {
              return sensor.removeContainer.apply(sensor, containers)
            })
            return this
          }

          /**
                         * Adds listener for draggable events
                         * @param {String} type - Event name
                         * @param {...Function} callbacks - Event callbacks
                         * @return {Draggable}
                         * @example draggable.on('drag:start', (dragEvent) => dragEvent.cancel());
                         */

        }, {
          key: 'on',
          value: function on (type) {
            let _emitter

            for (var _len7 = arguments.length, callbacks = Array(_len7 > 1 ? _len7 - 1 : 0), _key7 = 1; _key7 < _len7; _key7++) {
              callbacks[_key7 - 1] = arguments[_key7]
            }

            (_emitter = this.emitter).on.apply(_emitter, [type].concat(callbacks))
            return this
          }

          /**
                         * Removes listener from draggable
                         * @param {String} type - Event name
                         * @param {Function} callback - Event callback
                         * @return {Draggable}
                         * @example draggable.off('drag:start', handlerFunction);
                         */

        }, {
          key: 'off',
          value: function off (type, callback) {
            this.emitter.off(type, callback)
            return this
          }

          /**
                         * Triggers draggable event
                         * @param {AbstractEvent} event - Event instance
                         * @return {Draggable}
                         * @example draggable.trigger(event);
                         */

        }, {
          key: 'trigger',
          value: function trigger (event) {
            this.emitter.trigger(event)
            return this
          }

          /**
                         * Returns class name for class identifier
                         * @param {String} name - Name of class identifier
                         * @return {String|null}
                         */

        }, {
          key: 'getClassNameFor',
          value: function getClassNameFor (name) {
            return this.options.classes[name]
          }

          /**
                         * Returns true if this draggable instance is currently dragging
                         * @return {Boolean}
                         */

        }, {
          key: 'isDragging',
          value: function isDragging () {
            return Boolean(this.dragging)
          }

          /**
                         * Returns all draggable elements
                         * @return {HTMLElement[]}
                         */

        }, {
          key: 'getDraggableElements',
          value: function getDraggableElements () {
            const _this4 = this

            return this.containers.reduce(function (current, container) {
              return [].concat((0, _toConsumableArray3.default)(current), (0, _toConsumableArray3.default)(_this4.getDraggableElementsForContainer(container)))
            }, [])
          }

          /**
                         * Returns draggable elements for a given container, excluding the mirror and
                         * original source element if present
                         * @param {HTMLElement} container
                         * @return {HTMLElement[]}
                         */

        }, {
          key: 'getDraggableElementsForContainer',
          value: function getDraggableElementsForContainer (container) {
            const _this5 = this

            const allDraggableElements = container.querySelectorAll(this.options.draggable)

            return [].concat((0, _toConsumableArray3.default)(allDraggableElements)).filter(function (childElement) {
              return childElement !== _this5.originalSource && childElement !== _this5.mirror
            })
          }

          /**
                         * Drag start handler
                         * @private
                         * @param {Event} event - DOM Drag event
                         */

        }, {
          key: onDragStart,
          value: function value (event) {
            const _this6 = this

            const sensorEvent = getSensorEvent(event)
            const target = sensorEvent.target
            const container = sensorEvent.container

            if (!this.containers.includes(container)) {
              return
            }

            if (this.options.handle && target && !(0, _utils.closest)(target, this.options.handle)) {
              sensorEvent.cancel()
              return
            }

            // Find draggable source element
            this.originalSource = (0, _utils.closest)(target, this.options.draggable)
            this.sourceContainer = container

            if (!this.originalSource) {
              sensorEvent.cancel()
              return
            }

            if (this.lastPlacedSource && this.lastPlacedContainer) {
              clearTimeout(this.placedTimeoutID)
              this.lastPlacedSource.classList.remove(this.getClassNameFor('source:placed'))
              this.lastPlacedContainer.classList.remove(this.getClassNameFor('container:placed'))
            }

            this.source = this.originalSource.cloneNode(true)
            this.originalSource.parentNode.insertBefore(this.source, this.originalSource)
            this.originalSource.style.display = 'none'

            const dragEvent = new _DragEvent.DragStartEvent({
              source: this.source,
              originalSource: this.originalSource,
              sourceContainer: container,
              sensorEvent: sensorEvent
            })

            this.trigger(dragEvent)

            this.dragging = !dragEvent.canceled()

            if (dragEvent.canceled()) {
              this.source.parentNode.removeChild(this.source)
              this.originalSource.style.display = null
              return
            }

            this.originalSource.classList.add(this.getClassNameFor('source:original'))
            this.source.classList.add(this.getClassNameFor('source:dragging'))
            this.sourceContainer.classList.add(this.getClassNameFor('container:dragging'))
            document.body.classList.add(this.getClassNameFor('body:dragging'))
            applyUserSelect(document.body, 'none')

            requestAnimationFrame(function () {
              const oldSensorEvent = getSensorEvent(event)
              const newSensorEvent = oldSensorEvent.clone({ target: _this6.source })

              _this6[onDragMove](Object.assign({}, event, {
                detail: newSensorEvent
              }))
            })
          }

          /**
                         * Drag move handler
                         * @private
                         * @param {Event} event - DOM Drag event
                         */

        }, {
          key: onDragMove,
          value: function value (event) {
            if (!this.dragging) {
              return
            }

            const sensorEvent = getSensorEvent(event)
            const container = sensorEvent.container

            let target = sensorEvent.target

            const dragMoveEvent = new _DragEvent.DragMoveEvent({
              source: this.source,
              originalSource: this.originalSource,
              sourceContainer: container,
              sensorEvent: sensorEvent
            })

            this.trigger(dragMoveEvent)

            if (dragMoveEvent.canceled()) {
              sensorEvent.cancel()
            }

            target = (0, _utils.closest)(target, this.options.draggable)
            const withinCorrectContainer = (0, _utils.closest)(sensorEvent.target, this.containers)
            const overContainer = sensorEvent.overContainer || withinCorrectContainer
            const isLeavingContainer = this.currentOverContainer && overContainer !== this.currentOverContainer
            const isLeavingDraggable = this.currentOver && target !== this.currentOver
            const isOverContainer = overContainer && this.currentOverContainer !== overContainer
            const isOverDraggable = withinCorrectContainer && target && this.currentOver !== target

            if (isLeavingDraggable) {
              const dragOutEvent = new _DragEvent.DragOutEvent({
                source: this.source,
                originalSource: this.originalSource,
                sourceContainer: container,
                sensorEvent: sensorEvent,
                over: this.currentOver
              })

              this.currentOver.classList.remove(this.getClassNameFor('draggable:over'))
              this.currentOver = null

              this.trigger(dragOutEvent)
            }

            if (isLeavingContainer) {
              const dragOutContainerEvent = new _DragEvent.DragOutContainerEvent({
                source: this.source,
                originalSource: this.originalSource,
                sourceContainer: container,
                sensorEvent: sensorEvent,
                overContainer: this.currentOverContainer
              })

              this.currentOverContainer.classList.remove(this.getClassNameFor('container:over'))
              this.currentOverContainer = null

              this.trigger(dragOutContainerEvent)
            }

            if (isOverContainer) {
              overContainer.classList.add(this.getClassNameFor('container:over'))

              const dragOverContainerEvent = new _DragEvent.DragOverContainerEvent({
                source: this.source,
                originalSource: this.originalSource,
                sourceContainer: container,
                sensorEvent: sensorEvent,
                overContainer: overContainer
              })

              this.currentOverContainer = overContainer

              this.trigger(dragOverContainerEvent)
            }

            if (isOverDraggable) {
              target.classList.add(this.getClassNameFor('draggable:over'))

              const dragOverEvent = new _DragEvent.DragOverEvent({
                source: this.source,
                originalSource: this.originalSource,
                sourceContainer: container,
                sensorEvent: sensorEvent,
                overContainer: overContainer,
                over: target
              })

              this.currentOver = target

              this.trigger(dragOverEvent)
            }
          }

          /**
                         * Drag stop handler
                         * @private
                         * @param {Event} event - DOM Drag event
                         */

        }, {
          key: onDragStop,
          value: function value (event) {
            const _this7 = this

            if (!this.dragging) {
              return
            }

            this.dragging = false

            const dragStopEvent = new _DragEvent.DragStopEvent({
              source: this.source,
              originalSource: this.originalSource,
              sensorEvent: event.sensorEvent,
              sourceContainer: this.sourceContainer
            })

            this.trigger(dragStopEvent)

            this.source.parentNode.insertBefore(this.originalSource, this.source)
            this.source.parentNode.removeChild(this.source)
            this.originalSource.style.display = ''

            this.source.classList.remove(this.getClassNameFor('source:dragging'))
            this.originalSource.classList.remove(this.getClassNameFor('source:original'))
            this.originalSource.classList.add(this.getClassNameFor('source:placed'))
            this.sourceContainer.classList.add(this.getClassNameFor('container:placed'))
            this.sourceContainer.classList.remove(this.getClassNameFor('container:dragging'))
            document.body.classList.remove(this.getClassNameFor('body:dragging'))
            applyUserSelect(document.body, '')

            if (this.currentOver) {
              this.currentOver.classList.remove(this.getClassNameFor('draggable:over'))
            }

            if (this.currentOverContainer) {
              this.currentOverContainer.classList.remove(this.getClassNameFor('container:over'))
            }

            this.lastPlacedSource = this.originalSource
            this.lastPlacedContainer = this.sourceContainer

            this.placedTimeoutID = setTimeout(function () {
              if (_this7.lastPlacedSource) {
                _this7.lastPlacedSource.classList.remove(_this7.getClassNameFor('source:placed'))
              }

              if (_this7.lastPlacedContainer) {
                _this7.lastPlacedContainer.classList.remove(_this7.getClassNameFor('container:placed'))
              }

              _this7.lastPlacedSource = null
              _this7.lastPlacedContainer = null
            }, this.options.placedTimeout)

            this.source = null
            this.originalSource = null
            this.currentOverContainer = null
            this.currentOver = null
            this.sourceContainer = null
          }

          /**
                         * Drag pressure handler
                         * @private
                         * @param {Event} event - DOM Drag event
                         */

        }, {
          key: onDragPressure,
          value: function value (event) {
            if (!this.dragging) {
              return
            }

            const sensorEvent = getSensorEvent(event)
            const source = this.source || (0, _utils.closest)(sensorEvent.originalEvent.target, this.options.draggable)

            const dragPressureEvent = new _DragEvent.DragPressureEvent({
              sensorEvent: sensorEvent,
              source: source,
              pressure: sensorEvent.pressure
            })

            this.trigger(dragPressureEvent)
          }
        }])
        return Draggable
      }())

      Draggable.Plugins = { Announcement: _Plugins.Announcement, Focusable: _Plugins.Focusable, Mirror: _Plugins.Mirror, Scrollable: _Plugins.Scrollable }
      exports.default = Draggable

      function getSensorEvent (event) {
        return event.detail
      }

      function applyUserSelect (element, value) {
        element.style.webkitUserSelect = value
        element.style.mozUserSelect = value
        element.style.msUserSelect = value
        element.style.oUserSelect = value
        element.style.userSelect = value
      }
      /***/
    },
    /* 130 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = exports.scroll = exports.onDragStop = exports.onDragMove = exports.onDragStart = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractPlugin2 = __webpack_require__(10)

      const _AbstractPlugin3 = _interopRequireDefault(_AbstractPlugin2)

      const _utils = __webpack_require__(11)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onDragStart = exports.onDragStart = Symbol('onDragStart')
      const onDragMove = exports.onDragMove = Symbol('onDragMove')
      const onDragStop = exports.onDragStop = Symbol('onDragStop')
      const scroll = exports.scroll = Symbol('scroll')

      /**
                 * Scrollable default options
                 * @property {Object} defaultOptions
                 * @property {Number} defaultOptions.speed
                 * @property {Number} defaultOptions.sensitivity
                 * @property {HTMLElement[]} defaultOptions.scrollableElements
                 * @type {Object}
                 */
      const defaultOptions = exports.defaultOptions = {
        speed: 6,
        sensitivity: 50,
        scrollableElements: []
      }

      /**
                 * Scrollable plugin which scrolls the closest scrollable parent
                 * @class Scrollable
                 * @module Scrollable
                 * @extends AbstractPlugin
                 */

      const Scrollable = (function (_AbstractPlugin) {
        (0, _inherits3.default)(Scrollable, _AbstractPlugin)

        /**
                     * Scrollable constructor.
                     * @constructs Scrollable
                     * @param {Draggable} draggable - Draggable instance
                     */
        function Scrollable (draggable) {
          (0, _classCallCheck3.default)(this, Scrollable)

          /**
                         * Scrollable options
                         * @property {Object} options
                         * @property {Number} options.speed
                         * @property {Number} options.sensitivity
                         * @property {HTMLElement[]} options.scrollableElements
                         * @type {Object}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (Scrollable.__proto__ || Object.getPrototypeOf(Scrollable)).call(this, draggable))

          _this.options = Object.assign({}, defaultOptions, _this.getOptions())

          /**
                         * Keeps current mouse position
                         * @property {Object} currentMousePosition
                         * @property {Number} currentMousePosition.clientX
                         * @property {Number} currentMousePosition.clientY
                         * @type {Object|null}
                         */
          _this.currentMousePosition = null

          /**
                         * Scroll animation frame
                         * @property scrollAnimationFrame
                         * @type {Number|null}
                         */
          _this.scrollAnimationFrame = null

          /**
                         * Closest scrollable element
                         * @property scrollableElement
                         * @type {HTMLElement|null}
                         */
          _this.scrollableElement = null

          /**
                         * Animation frame looking for the closest scrollable element
                         * @property findScrollableElementFrame
                         * @type {Number|null}
                         */
          _this.findScrollableElementFrame = null

          _this[onDragStart] = _this[onDragStart].bind(_this)
          _this[onDragMove] = _this[onDragMove].bind(_this)
          _this[onDragStop] = _this[onDragStop].bind(_this)
          _this[scroll] = _this[scroll].bind(_this)
          return _this
        }

        /**
                     * Attaches plugins event listeners
                     */

        (0, _createClass3.default)(Scrollable, [{
          key: 'attach',
          value: function attach () {
            this.draggable.on('drag:start', this[onDragStart]).on('drag:move', this[onDragMove]).on('drag:stop', this[onDragStop])
          }

          /**
                         * Detaches plugins event listeners
                         */

        }, {
          key: 'detach',
          value: function detach () {
            this.draggable.off('drag:start', this[onDragStart]).off('drag:move', this[onDragMove]).off('drag:stop', this[onDragStop])
          }

          /**
                         * Returns options passed through draggable
                         * @return {Object}
                         */

        }, {
          key: 'getOptions',
          value: function getOptions () {
            return this.draggable.options.scrollable || {}
          }

          /**
                         * Returns closest scrollable elements by element
                         * @param {HTMLElement} target
                         * @return {HTMLElement}
                         */

        }, {
          key: 'getScrollableElement',
          value: function getScrollableElement (target) {
            if (this.hasDefinedScrollableElements()) {
              return (0, _utils.closest)(target, this.options.scrollableElements) || document.documentElement
            } else {
              return closestScrollableElement(target)
            }
          }

          /**
                         * Returns true if at least one scrollable element have been defined via options
                         * @param {HTMLElement} target
                         * @return {Boolean}
                         */

        }, {
          key: 'hasDefinedScrollableElements',
          value: function hasDefinedScrollableElements () {
            return Boolean(this.options.scrollableElements.length !== 0)
          }

          /**
                         * Drag start handler. Finds closest scrollable parent in separate frame
                         * @param {DragStartEvent} dragEvent
                         * @private
                         */

        }, {
          key: onDragStart,
          value: function value (dragEvent) {
            const _this2 = this

            this.findScrollableElementFrame = requestAnimationFrame(function () {
              _this2.scrollableElement = _this2.getScrollableElement(dragEvent.source)
            })
          }

          /**
                         * Drag move handler. Remembers mouse position and initiates scrolling
                         * @param {DragMoveEvent} dragEvent
                         * @private
                         */

        }, {
          key: onDragMove,
          value: function value (dragEvent) {
            const _this3 = this

            this.findScrollableElementFrame = requestAnimationFrame(function () {
              _this3.scrollableElement = _this3.getScrollableElement(dragEvent.sensorEvent.target)
            })

            if (!this.scrollableElement) {
              return
            }

            const sensorEvent = dragEvent.sensorEvent
            const scrollOffset = { x: 0, y: 0 }

            if ('ontouchstart' in window) {
              scrollOffset.y = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0
              scrollOffset.x = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0
            }

            this.currentMousePosition = {
              clientX: sensorEvent.clientX - scrollOffset.x,
              clientY: sensorEvent.clientY - scrollOffset.y
            }

            this.scrollAnimationFrame = requestAnimationFrame(this[scroll])
          }

          /**
                         * Drag stop handler. Cancels scroll animations and resets state
                         * @private
                         */

        }, {
          key: onDragStop,
          value: function value () {
            cancelAnimationFrame(this.scrollAnimationFrame)
            cancelAnimationFrame(this.findScrollableElementFrame)

            this.scrollableElement = null
            this.scrollAnimationFrame = null
            this.findScrollableElementFrame = null
            this.currentMousePosition = null
          }

          /**
                         * Scroll function that does the heavylifting
                         * @private
                         */

        }, {
          key: scroll,
          value: function value () {
            if (!this.scrollableElement || !this.currentMousePosition) {
              return
            }

            cancelAnimationFrame(this.scrollAnimationFrame)

            const _options = this.options
            const speed = _options.speed
            const sensitivity = _options.sensitivity

            const rect = this.scrollableElement.getBoundingClientRect()
            const bottomCutOff = rect.bottom > window.innerHeight
            const topCutOff = rect.top < 0
            const cutOff = topCutOff || bottomCutOff

            const documentScrollingElement = getDocumentScrollingElement()
            const scrollableElement = this.scrollableElement
            const clientX = this.currentMousePosition.clientX
            const clientY = this.currentMousePosition.clientY

            if (scrollableElement !== document.body && scrollableElement !== document.documentElement && !cutOff) {
              const offsetHeight = scrollableElement.offsetHeight
              const offsetWidth = scrollableElement.offsetWidth

              if (rect.top + offsetHeight - clientY < sensitivity) {
                scrollableElement.scrollTop += speed
              } else if (clientY - rect.top < sensitivity) {
                scrollableElement.scrollTop -= speed
              }

              if (rect.left + offsetWidth - clientX < sensitivity) {
                scrollableElement.scrollLeft += speed
              } else if (clientX - rect.left < sensitivity) {
                scrollableElement.scrollLeft -= speed
              }
            } else {
              const _window = window
              const innerHeight = _window.innerHeight
              const innerWidth = _window.innerWidth

              if (clientY < sensitivity) {
                documentScrollingElement.scrollTop -= speed
              } else if (innerHeight - clientY < sensitivity) {
                documentScrollingElement.scrollTop += speed
              }

              if (clientX < sensitivity) {
                documentScrollingElement.scrollLeft -= speed
              } else if (innerWidth - clientX < sensitivity) {
                documentScrollingElement.scrollLeft += speed
              }
            }

            this.scrollAnimationFrame = requestAnimationFrame(this[scroll])
          }
        }])
        return Scrollable
      }(_AbstractPlugin3.default))

      /**
                 * Returns true if the passed element has overflow
                 * @param {HTMLElement} element
                 * @return {Boolean}
                 * @private
                 */

      exports.default = Scrollable

      function hasOverflow (element) {
        const overflowRegex = /(auto|scroll)/
        const computedStyles = getComputedStyle(element, null)

        const overflow = computedStyles.getPropertyValue('overflow') + computedStyles.getPropertyValue('overflow-y') + computedStyles.getPropertyValue('overflow-x')

        return overflowRegex.test(overflow)
      }

      /**
                 * Returns true if the passed element is statically positioned
                 * @param {HTMLElement} element
                 * @return {Boolean}
                 * @private
                 */
      function isStaticallyPositioned (element) {
        const position = getComputedStyle(element).getPropertyValue('position')
        return position === 'static'
      }

      /**
                 * Finds closest scrollable element
                 * @param {HTMLElement} element
                 * @return {HTMLElement}
                 * @private
                 */
      function closestScrollableElement (element) {
        if (!element) {
          return getDocumentScrollingElement()
        }

        const position = getComputedStyle(element).getPropertyValue('position')
        const excludeStaticParents = position === 'absolute'

        const scrollableElement = (0, _utils.closest)(element, function (parent) {
          if (excludeStaticParents && isStaticallyPositioned(parent)) {
            return false
          }
          return hasOverflow(parent)
        })

        if (position === 'fixed' || !scrollableElement) {
          return getDocumentScrollingElement()
        } else {
          return scrollableElement
        }
      }

      /**
                 * Returns element that scrolls document
                 * @return {HTMLElement}
                 * @private
                 */
      function getDocumentScrollingElement () {
        return document.scrollingElement || document.documentElement
      }
      /***/
    },
    /* 131 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = undefined

      const _Scrollable = __webpack_require__(130)

      const _Scrollable2 = _interopRequireDefault(_Scrollable)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Scrollable2.default
      exports.defaultOptions = _Scrollable.defaultOptions
      /***/
    },
    /* 132 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.MirrorDestroyEvent = exports.MirrorMoveEvent = exports.MirrorAttachedEvent = exports.MirrorCreatedEvent = exports.MirrorCreateEvent = exports.MirrorEvent = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractEvent2 = __webpack_require__(9)

      const _AbstractEvent3 = _interopRequireDefault(_AbstractEvent2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * Base mirror event
                 * @class MirrorEvent
                 * @module MirrorEvent
                 * @extends AbstractEvent
                 */
      const MirrorEvent = exports.MirrorEvent = (function (_AbstractEvent) {
        (0, _inherits3.default)(MirrorEvent, _AbstractEvent)

        function MirrorEvent () {
          (0, _classCallCheck3.default)(this, MirrorEvent)
          return (0, _possibleConstructorReturn3.default)(this, (MirrorEvent.__proto__ || Object.getPrototypeOf(MirrorEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(MirrorEvent, [{
          key: 'source',

          /**
                         * Draggables source element
                         * @property source
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.source
          }

          /**
                         * Draggables original source element
                         * @property originalSource
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'originalSource',
          get: function get () {
            return this.data.originalSource
          }

          /**
                         * Draggables source container element
                         * @property sourceContainer
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'sourceContainer',
          get: function get () {
            return this.data.sourceContainer
          }

          /**
                         * Sensor event
                         * @property sensorEvent
                         * @type {SensorEvent}
                         * @readonly
                         */

        }, {
          key: 'sensorEvent',
          get: function get () {
            return this.data.sensorEvent
          }

          /**
                         * Drag event
                         * @property dragEvent
                         * @type {DragEvent}
                         * @readonly
                         */

        }, {
          key: 'dragEvent',
          get: function get () {
            return this.data.dragEvent
          }

          /**
                         * Original event that triggered sensor event
                         * @property originalEvent
                         * @type {Event}
                         * @readonly
                         */

        }, {
          key: 'originalEvent',
          get: function get () {
            if (this.sensorEvent) {
              return this.sensorEvent.originalEvent
            }

            return null
          }
        }])
        return MirrorEvent
      }(_AbstractEvent3.default))

      /**
                 * Mirror create event
                 * @class MirrorCreateEvent
                 * @module MirrorCreateEvent
                 * @extends MirrorEvent
                 */

      const MirrorCreateEvent = exports.MirrorCreateEvent = (function (_MirrorEvent) {
        (0, _inherits3.default)(MirrorCreateEvent, _MirrorEvent)

        function MirrorCreateEvent () {
          (0, _classCallCheck3.default)(this, MirrorCreateEvent)
          return (0, _possibleConstructorReturn3.default)(this, (MirrorCreateEvent.__proto__ || Object.getPrototypeOf(MirrorCreateEvent)).apply(this, arguments))
        }

        return MirrorCreateEvent
      }(MirrorEvent))

      /**
                 * Mirror created event
                 * @class MirrorCreatedEvent
                 * @module MirrorCreatedEvent
                 * @extends MirrorEvent
                 */

      MirrorCreateEvent.type = 'mirror:create'

      const MirrorCreatedEvent = exports.MirrorCreatedEvent = (function (_MirrorEvent2) {
        (0, _inherits3.default)(MirrorCreatedEvent, _MirrorEvent2)

        function MirrorCreatedEvent () {
          (0, _classCallCheck3.default)(this, MirrorCreatedEvent)
          return (0, _possibleConstructorReturn3.default)(this, (MirrorCreatedEvent.__proto__ || Object.getPrototypeOf(MirrorCreatedEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(MirrorCreatedEvent, [{
          key: 'mirror',

          /**
                         * Draggables mirror element
                         * @property mirror
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.mirror
          }
        }])
        return MirrorCreatedEvent
      }(MirrorEvent))

      /**
                 * Mirror attached event
                 * @class MirrorAttachedEvent
                 * @module MirrorAttachedEvent
                 * @extends MirrorEvent
                 */

      MirrorCreatedEvent.type = 'mirror:created'

      const MirrorAttachedEvent = exports.MirrorAttachedEvent = (function (_MirrorEvent3) {
        (0, _inherits3.default)(MirrorAttachedEvent, _MirrorEvent3)

        function MirrorAttachedEvent () {
          (0, _classCallCheck3.default)(this, MirrorAttachedEvent)
          return (0, _possibleConstructorReturn3.default)(this, (MirrorAttachedEvent.__proto__ || Object.getPrototypeOf(MirrorAttachedEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(MirrorAttachedEvent, [{
          key: 'mirror',

          /**
                         * Draggables mirror element
                         * @property mirror
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.mirror
          }
        }])
        return MirrorAttachedEvent
      }(MirrorEvent))

      /**
                 * Mirror move event
                 * @class MirrorMoveEvent
                 * @module MirrorMoveEvent
                 * @extends MirrorEvent
                 */

      MirrorAttachedEvent.type = 'mirror:attached'

      const MirrorMoveEvent = exports.MirrorMoveEvent = (function (_MirrorEvent4) {
        (0, _inherits3.default)(MirrorMoveEvent, _MirrorEvent4)

        function MirrorMoveEvent () {
          (0, _classCallCheck3.default)(this, MirrorMoveEvent)
          return (0, _possibleConstructorReturn3.default)(this, (MirrorMoveEvent.__proto__ || Object.getPrototypeOf(MirrorMoveEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(MirrorMoveEvent, [{
          key: 'mirror',

          /**
                         * Draggables mirror element
                         * @property mirror
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.mirror
          }
        }])
        return MirrorMoveEvent
      }(MirrorEvent))

      /**
                 * Mirror destroy event
                 * @class MirrorDestroyEvent
                 * @module MirrorDestroyEvent
                 * @extends MirrorEvent
                 */

      MirrorMoveEvent.type = 'mirror:move'
      MirrorMoveEvent.cancelable = true

      const MirrorDestroyEvent = exports.MirrorDestroyEvent = (function (_MirrorEvent5) {
        (0, _inherits3.default)(MirrorDestroyEvent, _MirrorEvent5)

        function MirrorDestroyEvent () {
          (0, _classCallCheck3.default)(this, MirrorDestroyEvent)
          return (0, _possibleConstructorReturn3.default)(this, (MirrorDestroyEvent.__proto__ || Object.getPrototypeOf(MirrorDestroyEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(MirrorDestroyEvent, [{
          key: 'mirror',

          /**
                         * Draggables mirror element
                         * @property mirror
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.mirror
          }
        }])
        return MirrorDestroyEvent
      }(MirrorEvent))

      MirrorDestroyEvent.type = 'mirror:destroy'
      MirrorDestroyEvent.cancelable = true
      /***/
    },
    /* 133 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _MirrorEvent = __webpack_require__(132)

      Object.keys(_MirrorEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _MirrorEvent[key]
          }
        })
      })
      /***/
    },
    /* 134 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      exports.__esModule = true

      exports.default = function (obj, keys) {
        const target = {}

        for (const i in obj) {
          if (keys.indexOf(i) >= 0) continue
          if (!Object.prototype.hasOwnProperty.call(obj, i)) continue
          target[i] = obj[i]
        }

        return target
      }
      /***/
    },
    /* 135 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = exports.getAppendableContainer = exports.onScroll = exports.onMirrorMove = exports.onMirrorCreated = exports.onDragStop = exports.onDragMove = exports.onDragStart = undefined

      const _objectWithoutProperties2 = __webpack_require__(134)

      const _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2)

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractPlugin2 = __webpack_require__(10)

      const _AbstractPlugin3 = _interopRequireDefault(_AbstractPlugin2)

      const _MirrorEvent = __webpack_require__(133)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onDragStart = exports.onDragStart = Symbol('onDragStart')
      const onDragMove = exports.onDragMove = Symbol('onDragMove')
      const onDragStop = exports.onDragStop = Symbol('onDragStop')
      const onMirrorCreated = exports.onMirrorCreated = Symbol('onMirrorCreated')
      const onMirrorMove = exports.onMirrorMove = Symbol('onMirrorMove')
      const onScroll = exports.onScroll = Symbol('onScroll')
      const getAppendableContainer = exports.getAppendableContainer = Symbol('getAppendableContainer')

      /**
                 * Mirror default options
                 * @property {Object} defaultOptions
                 * @property {Boolean} defaultOptions.constrainDimensions
                 * @property {Boolean} defaultOptions.xAxis
                 * @property {Boolean} defaultOptions.yAxis
                 * @property {null} defaultOptions.cursorOffsetX
                 * @property {null} defaultOptions.cursorOffsetY
                 * @type {Object}
                 */
      const defaultOptions = exports.defaultOptions = {
        constrainDimensions: false,
        xAxis: true,
        yAxis: true,
        cursorOffsetX: null,
        cursorOffsetY: null
      }

      /**
                 * Mirror plugin which controls the mirror positioning while dragging
                 * @class Mirror
                 * @module Mirror
                 * @extends AbstractPlugin
                 */

      const Mirror = (function (_AbstractPlugin) {
        (0, _inherits3.default)(Mirror, _AbstractPlugin)

        /**
                     * Mirror constructor.
                     * @constructs Mirror
                     * @param {Draggable} draggable - Draggable instance
                     */
        function Mirror (draggable) {
          (0, _classCallCheck3.default)(this, Mirror)

          /**
                         * Mirror options
                         * @property {Object} options
                         * @property {Boolean} options.constrainDimensions
                         * @property {Boolean} options.xAxis
                         * @property {Boolean} options.yAxis
                         * @property {Number|null} options.cursorOffsetX
                         * @property {Number|null} options.cursorOffsetY
                         * @property {String|HTMLElement|Function} options.appendTo
                         * @type {Object}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (Mirror.__proto__ || Object.getPrototypeOf(Mirror)).call(this, draggable))

          _this.options = Object.assign({}, defaultOptions, _this.getOptions())

          /**
                         * Scroll offset for touch devices because the mirror is positioned fixed
                         * @property {Object} scrollOffset
                         * @property {Number} scrollOffset.x
                         * @property {Number} scrollOffset.y
                         */
          _this.scrollOffset = { x: 0, y: 0 }

          /**
                         * Initial scroll offset for touch devices because the mirror is positioned fixed
                         * @property {Object} scrollOffset
                         * @property {Number} scrollOffset.x
                         * @property {Number} scrollOffset.y
                         */
          _this.initialScrollOffset = {
            x: window.scrollX,
            y: window.scrollY
          }

          _this[onDragStart] = _this[onDragStart].bind(_this)
          _this[onDragMove] = _this[onDragMove].bind(_this)
          _this[onDragStop] = _this[onDragStop].bind(_this)
          _this[onMirrorCreated] = _this[onMirrorCreated].bind(_this)
          _this[onMirrorMove] = _this[onMirrorMove].bind(_this)
          _this[onScroll] = _this[onScroll].bind(_this)
          return _this
        }

        /**
                     * Attaches plugins event listeners
                     */

        (0, _createClass3.default)(Mirror, [{
          key: 'attach',
          value: function attach () {
            this.draggable.on('drag:start', this[onDragStart]).on('drag:move', this[onDragMove]).on('drag:stop', this[onDragStop]).on('mirror:created', this[onMirrorCreated]).on('mirror:move', this[onMirrorMove])
          }

          /**
                         * Detaches plugins event listeners
                         */

        }, {
          key: 'detach',
          value: function detach () {
            this.draggable.off('drag:start', this[onDragStart]).off('drag:move', this[onDragMove]).off('drag:stop', this[onDragStop]).off('mirror:created', this[onMirrorCreated]).off('mirror:move', this[onMirrorMove])
          }

          /**
                         * Returns options passed through draggable
                         * @return {Object}
                         */

        }, {
          key: 'getOptions',
          value: function getOptions () {
            return this.draggable.options.mirror || {}
          }
        }, {
          key: onDragStart,
          value: function value (dragEvent) {
            if (dragEvent.canceled()) {
              return
            }

            if ('ontouchstart' in window) {
              document.addEventListener('scroll', this[onScroll], true)
            }

            this.initialScrollOffset = {
              x: window.scrollX,
              y: window.scrollY
            }

            const source = dragEvent.source
            const originalSource = dragEvent.originalSource
            const sourceContainer = dragEvent.sourceContainer
            const sensorEvent = dragEvent.sensorEvent

            const mirrorCreateEvent = new _MirrorEvent.MirrorCreateEvent({
              source: source,
              originalSource: originalSource,
              sourceContainer: sourceContainer,
              sensorEvent: sensorEvent,
              dragEvent: dragEvent
            })

            this.draggable.trigger(mirrorCreateEvent)

            if (isNativeDragEvent(sensorEvent) || mirrorCreateEvent.canceled()) {
              return
            }

            const appendableContainer = this[getAppendableContainer](source) || sourceContainer
            this.mirror = source.cloneNode(true)

            const mirrorCreatedEvent = new _MirrorEvent.MirrorCreatedEvent({
              source: source,
              originalSource: originalSource,
              sourceContainer: sourceContainer,
              sensorEvent: sensorEvent,
              dragEvent: dragEvent,
              mirror: this.mirror
            })

            const mirrorAttachedEvent = new _MirrorEvent.MirrorAttachedEvent({
              source: source,
              originalSource: originalSource,
              sourceContainer: sourceContainer,
              sensorEvent: sensorEvent,
              dragEvent: dragEvent,
              mirror: this.mirror
            })

            this.draggable.trigger(mirrorCreatedEvent)
            appendableContainer.appendChild(this.mirror)
            this.draggable.trigger(mirrorAttachedEvent)
          }
        }, {
          key: onDragMove,
          value: function value (dragEvent) {
            if (!this.mirror || dragEvent.canceled()) {
              return
            }

            const source = dragEvent.source
            const originalSource = dragEvent.originalSource
            const sourceContainer = dragEvent.sourceContainer
            const sensorEvent = dragEvent.sensorEvent

            const mirrorMoveEvent = new _MirrorEvent.MirrorMoveEvent({
              source: source,
              originalSource: originalSource,
              sourceContainer: sourceContainer,
              sensorEvent: sensorEvent,
              dragEvent: dragEvent,
              mirror: this.mirror
            })

            this.draggable.trigger(mirrorMoveEvent)
          }
        }, {
          key: onDragStop,
          value: function value (dragEvent) {
            if ('ontouchstart' in window) {
              document.removeEventListener('scroll', this[onScroll], true)
            }

            this.initialScrollOffset = { x: 0, y: 0 }
            this.scrollOffset = { x: 0, y: 0 }

            if (!this.mirror) {
              return
            }

            const source = dragEvent.source
            const sourceContainer = dragEvent.sourceContainer
            const sensorEvent = dragEvent.sensorEvent

            const mirrorDestroyEvent = new _MirrorEvent.MirrorDestroyEvent({
              source: source,
              mirror: this.mirror,
              sourceContainer: sourceContainer,
              sensorEvent: sensorEvent,
              dragEvent: dragEvent
            })

            this.draggable.trigger(mirrorDestroyEvent)

            if (!mirrorDestroyEvent.canceled()) {
              this.mirror.parentNode.removeChild(this.mirror)
            }
          }
        }, {
          key: onScroll,
          value: function value () {
            this.scrollOffset = {
              x: window.scrollX - this.initialScrollOffset.x,
              y: window.scrollY - this.initialScrollOffset.y
            }
          }

          /**
                         * Mirror created handler
                         * @param {MirrorCreatedEvent} mirrorEvent
                         * @return {Promise}
                         * @private
                         */

        }, {
          key: onMirrorCreated,
          value: function value (_ref) {
            const _this2 = this

            const mirror = _ref.mirror
            const source = _ref.source
            const sensorEvent = _ref.sensorEvent

            const mirrorClass = this.draggable.getClassNameFor('mirror')

            const setState = function setState (_ref2) {
              const mirrorOffset = _ref2.mirrorOffset
              const initialX = _ref2.initialX
              const initialY = _ref2.initialY
              const args = (0, _objectWithoutProperties3.default)(_ref2, ['mirrorOffset', 'initialX', 'initialY'])

              _this2.mirrorOffset = mirrorOffset
              _this2.initialX = initialX
              _this2.initialY = initialY
              return Object.assign({ mirrorOffset: mirrorOffset, initialX: initialX, initialY: initialY }, args)
            }

            const initialState = {
              mirror: mirror,
              source: source,
              sensorEvent: sensorEvent,
              mirrorClass: mirrorClass,
              scrollOffset: this.scrollOffset,
              options: this.options
            }

            return Promise.resolve(initialState)
            // Fix reflow here
              .then(computeMirrorDimensions).then(calculateMirrorOffset).then(resetMirror).then(addMirrorClasses).then(positionMirror({ initial: true })).then(removeMirrorID).then(setState)
          }

          /**
                         * Mirror move handler
                         * @param {MirrorMoveEvent} mirrorEvent
                         * @return {Promise|null}
                         * @private
                         */

        }, {
          key: onMirrorMove,
          value: function value (mirrorEvent) {
            if (mirrorEvent.canceled()) {
              return null
            }

            const initialState = {
              mirror: mirrorEvent.mirror,
              sensorEvent: mirrorEvent.sensorEvent,
              mirrorOffset: this.mirrorOffset,
              options: this.options,
              initialX: this.initialX,
              initialY: this.initialY,
              scrollOffset: this.scrollOffset
            }

            return Promise.resolve(initialState).then(positionMirror({ raf: true }))
          }

          /**
                         * Returns appendable container for mirror based on the appendTo option
                         * @private
                         * @param {Object} options
                         * @param {HTMLElement} options.source - Current source
                         * @return {HTMLElement}
                         */

        }, {
          key: getAppendableContainer,
          value: function value (source) {
            const appendTo = this.options.appendTo

            if (typeof appendTo === 'string') {
              return document.querySelector(appendTo)
            } else if (appendTo instanceof HTMLElement) {
              return appendTo
            } else if (typeof appendTo === 'function') {
              return appendTo(source)
            } else {
              return source.parentNode
            }
          }
        }])
        return Mirror
      }(_AbstractPlugin3.default))

      /**
                 * Computes mirror dimensions based on the source element
                 * Adds sourceRect to state
                 * @param {Object} state
                 * @param {HTMLElement} state.source
                 * @return {Promise}
                 * @private
                 */

      exports.default = Mirror

      function computeMirrorDimensions (_ref3) {
        const source = _ref3.source
        const args = (0, _objectWithoutProperties3.default)(_ref3, ['source'])

        return withPromise(function (resolve) {
          const sourceRect = source.getBoundingClientRect()
          resolve(Object.assign({ source: source, sourceRect: sourceRect }, args))
        })
      }

      /**
                 * Calculates mirror offset
                 * Adds mirrorOffset to state
                 * @param {Object} state
                 * @param {SensorEvent} state.sensorEvent
                 * @param {DOMRect} state.sourceRect
                 * @return {Promise}
                 * @private
                 */
      function calculateMirrorOffset (_ref4) {
        const sensorEvent = _ref4.sensorEvent
        const sourceRect = _ref4.sourceRect
        const options = _ref4.options
        const args = (0, _objectWithoutProperties3.default)(_ref4, ['sensorEvent', 'sourceRect', 'options'])

        return withPromise(function (resolve) {
          const top = options.cursorOffsetY === null ? sensorEvent.clientY - sourceRect.top : options.cursorOffsetY
          const left = options.cursorOffsetX === null ? sensorEvent.clientX - sourceRect.left : options.cursorOffsetX

          const mirrorOffset = { top: top, left: left }

          resolve(Object.assign({ sensorEvent: sensorEvent, sourceRect: sourceRect, mirrorOffset: mirrorOffset, options: options }, args))
        })
      }

      /**
                 * Applys mirror styles
                 * @param {Object} state
                 * @param {HTMLElement} state.mirror
                 * @param {HTMLElement} state.source
                 * @param {Object} state.options
                 * @return {Promise}
                 * @private
                 */
      function resetMirror (_ref5) {
        const mirror = _ref5.mirror
        const source = _ref5.source
        const options = _ref5.options
        const args = (0, _objectWithoutProperties3.default)(_ref5, ['mirror', 'source', 'options'])

        return withPromise(function (resolve) {
          let offsetHeight = void 0
          let offsetWidth = void 0

          if (options.constrainDimensions) {
            const computedSourceStyles = getComputedStyle(source)
            offsetHeight = computedSourceStyles.getPropertyValue('height')
            offsetWidth = computedSourceStyles.getPropertyValue('width')
          }

          mirror.style.position = 'fixed'
          mirror.style.pointerEvents = 'none'
          mirror.style.top = 0
          mirror.style.left = 0
          mirror.style.margin = 0

          if (options.constrainDimensions) {
            mirror.style.height = offsetHeight
            mirror.style.width = offsetWidth
          }

          resolve(Object.assign({ mirror: mirror, source: source, options: options }, args))
        })
      }

      /**
                 * Applys mirror class on mirror element
                 * @param {Object} state
                 * @param {HTMLElement} state.mirror
                 * @param {String} state.mirrorClass
                 * @return {Promise}
                 * @private
                 */
      function addMirrorClasses (_ref6) {
        const mirror = _ref6.mirror
        const mirrorClass = _ref6.mirrorClass
        const args = (0, _objectWithoutProperties3.default)(_ref6, ['mirror', 'mirrorClass'])

        return withPromise(function (resolve) {
          mirror.classList.add(mirrorClass)
          resolve(Object.assign({ mirror: mirror, mirrorClass: mirrorClass }, args))
        })
      }

      /**
                 * Removes source ID from cloned mirror element
                 * @param {Object} state
                 * @param {HTMLElement} state.mirror
                 * @return {Promise}
                 * @private
                 */
      function removeMirrorID (_ref7) {
        const mirror = _ref7.mirror
        const args = (0, _objectWithoutProperties3.default)(_ref7, ['mirror'])

        return withPromise(function (resolve) {
          mirror.removeAttribute('id')
          delete mirror.id
          resolve(Object.assign({ mirror: mirror }, args))
        })
      }

      /**
                 * Positions mirror with translate3d
                 * @param {Object} state
                 * @param {HTMLElement} state.mirror
                 * @param {SensorEvent} state.sensorEvent
                 * @param {Object} state.mirrorOffset
                 * @param {Number} state.initialY
                 * @param {Number} state.initialX
                 * @param {Object} state.options
                 * @return {Promise}
                 * @private
                 */
      function positionMirror () {
        const _ref8 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {}
        const _ref8$withFrame = _ref8.withFrame
        const withFrame = _ref8$withFrame === undefined ? false : _ref8$withFrame
        const _ref8$initial = _ref8.initial
        const initial = _ref8$initial === undefined ? false : _ref8$initial

        return function (_ref9) {
          const mirror = _ref9.mirror
          const sensorEvent = _ref9.sensorEvent
          const mirrorOffset = _ref9.mirrorOffset
          const initialY = _ref9.initialY
          const initialX = _ref9.initialX
          const scrollOffset = _ref9.scrollOffset
          const options = _ref9.options
          const args = (0, _objectWithoutProperties3.default)(_ref9, ['mirror', 'sensorEvent', 'mirrorOffset', 'initialY', 'initialX', 'scrollOffset', 'options'])

          return withPromise(function (resolve) {
            const result = Object.assign({
              mirror: mirror,
              sensorEvent: sensorEvent,
              mirrorOffset: mirrorOffset,
              options: options
            }, args)

            if (mirrorOffset) {
              const x = sensorEvent.clientX - mirrorOffset.left - scrollOffset.x
              const y = sensorEvent.clientY - mirrorOffset.top - scrollOffset.y

              if (options.xAxis && options.yAxis || initial) {
                mirror.style.transform = 'translate3d(' + x + 'px, ' + y + 'px, 0)'
              } else if (options.xAxis && !options.yAxis) {
                mirror.style.transform = 'translate3d(' + x + 'px, ' + initialY + 'px, 0)'
              } else if (options.yAxis && !options.xAxis) {
                mirror.style.transform = 'translate3d(' + initialX + 'px, ' + y + 'px, 0)'
              }

              if (initial) {
                result.initialX = x
                result.initialY = y
              }
            }

            resolve(result)
          }, { frame: withFrame })
        }
      }

      /**
                 * Wraps functions in promise with potential animation frame option
                 * @param {Function} callback
                 * @param {Object} options
                 * @param {Boolean} options.raf
                 * @return {Promise}
                 * @private
                 */
      function withPromise (callback) {
        const _ref10 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {}
        const _ref10$raf = _ref10.raf
        const raf = _ref10$raf === undefined ? false : _ref10$raf

        return new Promise(function (resolve, reject) {
          if (raf) {
            requestAnimationFrame(function () {
              callback(resolve, reject)
            })
          } else {
            callback(resolve, reject)
          }
        })
      }

      /**
                 * Returns true if the sensor event was triggered by a native browser drag event
                 * @param {SensorEvent} sensorEvent
                 */
      function isNativeDragEvent (sensorEvent) {
        return (/^drag/.test(sensorEvent.originalEvent.type))
      }
      /***/
    },
    /* 136 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = undefined

      const _Mirror = __webpack_require__(135)

      const _Mirror2 = _interopRequireDefault(_Mirror)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Mirror2.default
      exports.defaultOptions = _Mirror.defaultOptions
      /***/
    },
    /* 137 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _toConsumableArray2 = __webpack_require__(25)

      const _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2)

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractPlugin2 = __webpack_require__(10)

      const _AbstractPlugin3 = _interopRequireDefault(_AbstractPlugin2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onInitialize = Symbol('onInitialize')
      const onDestroy = Symbol('onDestroy')

      /**
                 * Focusable default options
                 * @property {Object} defaultOptions
                 * @type {Object}
                 */
      const defaultOptions = {}

      /**
                 * Focusable plugin
                 * @class Focusable
                 * @module Focusable
                 * @extends AbstractPlugin
                 */

      const Focusable = (function (_AbstractPlugin) {
        (0, _inherits3.default)(Focusable, _AbstractPlugin)

        /**
                     * Focusable constructor.
                     * @constructs Focusable
                     * @param {Draggable} draggable - Draggable instance
                     */
        function Focusable (draggable) {
          (0, _classCallCheck3.default)(this, Focusable)

          /**
                         * Focusable options
                         * @property {Object} options
                         * @type {Object}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (Focusable.__proto__ || Object.getPrototypeOf(Focusable)).call(this, draggable))

          _this.options = Object.assign({}, defaultOptions, _this.getOptions())

          _this[onInitialize] = _this[onInitialize].bind(_this)
          _this[onDestroy] = _this[onDestroy].bind(_this)
          return _this
        }

        /**
                     * Attaches listeners to draggable
                     */

        (0, _createClass3.default)(Focusable, [{
          key: 'attach',
          value: function attach () {
            this.draggable.on('draggable:initialize', this[onInitialize]).on('draggable:destroy', this[onDestroy])
          }

          /**
                         * Detaches listeners from draggable
                         */

        }, {
          key: 'detach',
          value: function detach () {
            this.draggable.off('draggable:initialize', this[onInitialize]).off('draggable:destroy', this[onDestroy])
          }

          /**
                         * Returns options passed through draggable
                         * @return {Object}
                         */

        }, {
          key: 'getOptions',
          value: function getOptions () {
            return this.draggable.options.focusable || {}
          }

          /**
                         * Returns draggable containers and elements
                         * @return {HTMLElement[]}
                         */

        }, {
          key: 'getElements',
          value: function getElements () {
            return [].concat((0, _toConsumableArray3.default)(this.draggable.containers), (0, _toConsumableArray3.default)(this.draggable.getDraggableElements()))
          }

          /**
                         * Intialize handler
                         * @private
                         */

        }, {
          key: onInitialize,
          value: function value () {
            const _this2 = this

            // Can wait until the next best frame is available
            requestAnimationFrame(function () {
              _this2.getElements().forEach(function (element) {
                return decorateElement(element)
              })
            })
          }

          /**
                         * Destroy handler
                         * @private
                         */

        }, {
          key: onDestroy,
          value: function value () {
            const _this3 = this

            // Can wait until the next best frame is available
            requestAnimationFrame(function () {
              _this3.getElements().forEach(function (element) {
                return stripElement(element)
              })
            })
          }
        }])
        return Focusable
      }(_AbstractPlugin3.default))

      /**
                 * Keeps track of all the elements that are missing tabindex attributes
                 * so they can be reset when draggable gets destroyed
                 * @const {HTMLElement[]} elementsWithMissingTabIndex
                 */

      exports.default = Focusable
      const elementsWithMissingTabIndex = []

      /**
                 * Decorates element with tabindex attributes
                 * @param {HTMLElement} element
                 * @return {Object}
                 * @private
                 */
      function decorateElement (element) {
        const hasMissingTabIndex = Boolean(!element.getAttribute('tabindex') && element.tabIndex === -1)

        if (hasMissingTabIndex) {
          elementsWithMissingTabIndex.push(element)
          element.tabIndex = 0
        }
      }

      /**
                 * Removes elements tabindex attributes
                 * @param {HTMLElement} element
                 * @private
                 */
      function stripElement (element) {
        const tabIndexElementPosition = elementsWithMissingTabIndex.indexOf(element)

        if (tabIndexElementPosition !== -1) {
          element.tabIndex = -1
          elementsWithMissingTabIndex.splice(tabIndexElementPosition, 1)
        }
      }
      /***/
    },
    /* 138 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _Focusable = __webpack_require__(137)

      const _Focusable2 = _interopRequireDefault(_Focusable)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Focusable2.default
      /***/
    },
    /* 139 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractPlugin2 = __webpack_require__(10)

      const _AbstractPlugin3 = _interopRequireDefault(_AbstractPlugin2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onInitialize = Symbol('onInitialize')
      const onDestroy = Symbol('onDestroy')
      const announceEvent = Symbol('announceEvent')
      const announceMessage = Symbol('announceMessage')

      const ARIA_RELEVANT = 'aria-relevant'
      const ARIA_ATOMIC = 'aria-atomic'
      const ARIA_LIVE = 'aria-live'
      const ROLE = 'role'

      /**
                 * Announcement default options
                 * @property {Object} defaultOptions
                 * @property {Number} defaultOptions.expire
                 * @type {Object}
                 */
      const defaultOptions = exports.defaultOptions = {
        expire: 7000
      }

      /**
                 * Announcement plugin
                 * @class Announcement
                 * @module Announcement
                 * @extends AbstractPlugin
                 */

      const Announcement = (function (_AbstractPlugin) {
        (0, _inherits3.default)(Announcement, _AbstractPlugin)

        /**
                     * Announcement constructor.
                     * @constructs Announcement
                     * @param {Draggable} draggable - Draggable instance
                     */
        function Announcement (draggable) {
          (0, _classCallCheck3.default)(this, Announcement)

          /**
                         * Plugin options
                         * @property options
                         * @type {Object}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (Announcement.__proto__ || Object.getPrototypeOf(Announcement)).call(this, draggable))

          _this.options = Object.assign({}, defaultOptions, _this.getOptions())

          /**
                         * Original draggable trigger method. Hack until we have onAll or on('all')
                         * @property originalTriggerMethod
                         * @type {Function}
                         */
          _this.originalTriggerMethod = _this.draggable.trigger

          _this[onInitialize] = _this[onInitialize].bind(_this)
          _this[onDestroy] = _this[onDestroy].bind(_this)
          return _this
        }

        /**
                     * Attaches listeners to draggable
                     */

        (0, _createClass3.default)(Announcement, [{
          key: 'attach',
          value: function attach () {
            this.draggable.on('draggable:initialize', this[onInitialize])
          }

          /**
                         * Detaches listeners from draggable
                         */

        }, {
          key: 'detach',
          value: function detach () {
            this.draggable.off('draggable:destroy', this[onDestroy])
          }

          /**
                         * Returns passed in options
                         */

        }, {
          key: 'getOptions',
          value: function getOptions () {
            return this.draggable.options.announcements || {}
          }

          /**
                         * Announces event
                         * @private
                         * @param {AbstractEvent} event
                         */

        }, {
          key: announceEvent,
          value: function value (event) {
            const message = this.options[event.type]

            if (message && typeof message === 'string') {
              this[announceMessage](message)
            }

            if (message && typeof message === 'function') {
              this[announceMessage](message(event))
            }
          }

          /**
                         * Announces message to screen reader
                         * @private
                         * @param {String} message
                         */

        }, {
          key: announceMessage,
          value: function value (message) {
            announce(message, { expire: this.options.expire })
          }

          /**
                         * Initialize hander
                         * @private
                         */

        }, {
          key: onInitialize,
          value: function value () {
            const _this2 = this

            // Hack until there is an api for listening for all events
            this.draggable.trigger = function (event) {
              try {
                _this2[announceEvent](event)
              } finally {
                // Ensure that original trigger is called
                _this2.originalTriggerMethod.call(_this2.draggable, event)
              }
            }
          }

          /**
                         * Destroy hander
                         * @private
                         */

        }, {
          key: onDestroy,
          value: function value () {
            this.draggable.trigger = this.originalTriggerMethod
          }
        }])
        return Announcement
      }(_AbstractPlugin3.default))

      /**
                 * @const {HTMLElement} liveRegion
                 */

      exports.default = Announcement
      const liveRegion = createRegion()

      /**
                 * Announces message via live region
                 * @param {String} message
                 * @param {Object} options
                 * @param {Number} options.expire
                 */
      function announce (message, _ref) {
        const expire = _ref.expire

        const element = document.createElement('div')

        element.textContent = message
        liveRegion.appendChild(element)

        return setTimeout(function () {
          liveRegion.removeChild(element)
        }, expire)
      }

      /**
                 * Creates region element
                 * @return {HTMLElement}
                 */
      function createRegion () {
        const element = document.createElement('div')

        element.setAttribute('id', 'draggable-live-region')
        element.setAttribute(ARIA_RELEVANT, 'additions')
        element.setAttribute(ARIA_ATOMIC, 'true')
        element.setAttribute(ARIA_LIVE, 'assertive')
        element.setAttribute(ROLE, 'log')

        element.style.position = 'fixed'
        element.style.width = '1px'
        element.style.height = '1px'
        element.style.top = '-1px'
        element.style.overflow = 'hidden'

        return element
      }

      // Append live region element as early as possible
      document.addEventListener('DOMContentLoaded', function () {
        document.body.appendChild(liveRegion)
      })
      /***/
    },
    /* 140 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = undefined

      const _Announcement = __webpack_require__(139)

      const _Announcement2 = _interopRequireDefault(_Announcement)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Announcement2.default
      exports.defaultOptions = _Announcement.defaultOptions
      /***/
    },
    /* 141 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.DraggableDestroyEvent = exports.DraggableInitializedEvent = exports.DraggableEvent = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractEvent2 = __webpack_require__(9)

      const _AbstractEvent3 = _interopRequireDefault(_AbstractEvent2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * Base draggable event
                 * @class DraggableEvent
                 * @module DraggableEvent
                 * @extends AbstractEvent
                 */
      const DraggableEvent = exports.DraggableEvent = (function (_AbstractEvent) {
        (0, _inherits3.default)(DraggableEvent, _AbstractEvent)

        function DraggableEvent () {
          (0, _classCallCheck3.default)(this, DraggableEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DraggableEvent.__proto__ || Object.getPrototypeOf(DraggableEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DraggableEvent, [{
          key: 'draggable',

          /**
                         * Draggable instance
                         * @property draggable
                         * @type {Draggable}
                         * @readonly
                         */
          get: function get () {
            return this.data.draggable
          }
        }])
        return DraggableEvent
      }(_AbstractEvent3.default))

      /**
                 * Draggable initialized event
                 * @class DraggableInitializedEvent
                 * @module DraggableInitializedEvent
                 * @extends DraggableEvent
                 */

      DraggableEvent.type = 'draggable'

      const DraggableInitializedEvent = exports.DraggableInitializedEvent = (function (_DraggableEvent) {
        (0, _inherits3.default)(DraggableInitializedEvent, _DraggableEvent)

        function DraggableInitializedEvent () {
          (0, _classCallCheck3.default)(this, DraggableInitializedEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DraggableInitializedEvent.__proto__ || Object.getPrototypeOf(DraggableInitializedEvent)).apply(this, arguments))
        }

        return DraggableInitializedEvent
      }(DraggableEvent))

      /**
                 * Draggable destory event
                 * @class DraggableInitializedEvent
                 * @module DraggableDestroyEvent
                 * @extends DraggableDestroyEvent
                 */

      DraggableInitializedEvent.type = 'draggable:initialize'

      const DraggableDestroyEvent = exports.DraggableDestroyEvent = (function (_DraggableEvent2) {
        (0, _inherits3.default)(DraggableDestroyEvent, _DraggableEvent2)

        function DraggableDestroyEvent () {
          (0, _classCallCheck3.default)(this, DraggableDestroyEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DraggableDestroyEvent.__proto__ || Object.getPrototypeOf(DraggableDestroyEvent)).apply(this, arguments))
        }

        return DraggableDestroyEvent
      }(DraggableEvent))

      DraggableDestroyEvent.type = 'draggable:destroy'
      /***/
    },
    /* 142 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.DragStopEvent = exports.DragPressureEvent = exports.DragOutContainerEvent = exports.DragOverContainerEvent = exports.DragOutEvent = exports.DragOverEvent = exports.DragMoveEvent = exports.DragStartEvent = exports.DragEvent = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractEvent2 = __webpack_require__(9)

      const _AbstractEvent3 = _interopRequireDefault(_AbstractEvent2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * Base drag event
                 * @class DragEvent
                 * @module DragEvent
                 * @extends AbstractEvent
                 */
      const DragEvent = exports.DragEvent = (function (_AbstractEvent) {
        (0, _inherits3.default)(DragEvent, _AbstractEvent)

        function DragEvent () {
          (0, _classCallCheck3.default)(this, DragEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragEvent.__proto__ || Object.getPrototypeOf(DragEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DragEvent, [{
          key: 'source',

          /**
                         * Draggables source element
                         * @property source
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.source
          }

          /**
                         * Draggables original source element
                         * @property originalSource
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'originalSource',
          get: function get () {
            return this.data.originalSource
          }

          /**
                         * Draggables mirror element
                         * @property mirror
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'mirror',
          get: function get () {
            return this.data.mirror
          }

          /**
                         * Draggables source container element
                         * @property sourceContainer
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'sourceContainer',
          get: function get () {
            return this.data.sourceContainer
          }

          /**
                         * Sensor event
                         * @property sensorEvent
                         * @type {SensorEvent}
                         * @readonly
                         */

        }, {
          key: 'sensorEvent',
          get: function get () {
            return this.data.sensorEvent
          }

          /**
                         * Original event that triggered sensor event
                         * @property originalEvent
                         * @type {Event}
                         * @readonly
                         */

        }, {
          key: 'originalEvent',
          get: function get () {
            if (this.sensorEvent) {
              return this.sensorEvent.originalEvent
            }

            return null
          }
        }])
        return DragEvent
      }(_AbstractEvent3.default))

      /**
                 * Drag start event
                 * @class DragStartEvent
                 * @module DragStartEvent
                 * @extends DragEvent
                 */

      DragEvent.type = 'drag'

      const DragStartEvent = exports.DragStartEvent = (function (_DragEvent) {
        (0, _inherits3.default)(DragStartEvent, _DragEvent)

        function DragStartEvent () {
          (0, _classCallCheck3.default)(this, DragStartEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragStartEvent.__proto__ || Object.getPrototypeOf(DragStartEvent)).apply(this, arguments))
        }

        return DragStartEvent
      }(DragEvent))

      /**
                 * Drag move event
                 * @class DragMoveEvent
                 * @module DragMoveEvent
                 * @extends DragEvent
                 */

      DragStartEvent.type = 'drag:start'
      DragStartEvent.cancelable = true

      const DragMoveEvent = exports.DragMoveEvent = (function (_DragEvent2) {
        (0, _inherits3.default)(DragMoveEvent, _DragEvent2)

        function DragMoveEvent () {
          (0, _classCallCheck3.default)(this, DragMoveEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragMoveEvent.__proto__ || Object.getPrototypeOf(DragMoveEvent)).apply(this, arguments))
        }

        return DragMoveEvent
      }(DragEvent))

      /**
                 * Drag over event
                 * @class DragOverEvent
                 * @module DragOverEvent
                 * @extends DragEvent
                 */

      DragMoveEvent.type = 'drag:move'

      const DragOverEvent = exports.DragOverEvent = (function (_DragEvent3) {
        (0, _inherits3.default)(DragOverEvent, _DragEvent3)

        function DragOverEvent () {
          (0, _classCallCheck3.default)(this, DragOverEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragOverEvent.__proto__ || Object.getPrototypeOf(DragOverEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DragOverEvent, [{
          key: 'overContainer',

          /**
                         * Draggable container you are over
                         * @property overContainer
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.overContainer
          }

          /**
                         * Draggable element you are over
                         * @property over
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'over',
          get: function get () {
            return this.data.over
          }
        }])
        return DragOverEvent
      }(DragEvent))

      /**
                 * Drag out event
                 * @class DragOutEvent
                 * @module DragOutEvent
                 * @extends DragEvent
                 */

      DragOverEvent.type = 'drag:over'
      DragOverEvent.cancelable = true

      const DragOutEvent = exports.DragOutEvent = (function (_DragEvent4) {
        (0, _inherits3.default)(DragOutEvent, _DragEvent4)

        function DragOutEvent () {
          (0, _classCallCheck3.default)(this, DragOutEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragOutEvent.__proto__ || Object.getPrototypeOf(DragOutEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DragOutEvent, [{
          key: 'overContainer',

          /**
                         * Draggable container you are over
                         * @property overContainer
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.overContainer
          }

          /**
                         * Draggable element you left
                         * @property over
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'over',
          get: function get () {
            return this.data.over
          }
        }])
        return DragOutEvent
      }(DragEvent))

      /**
                 * Drag over container event
                 * @class DragOverContainerEvent
                 * @module DragOverContainerEvent
                 * @extends DragEvent
                 */

      DragOutEvent.type = 'drag:out'

      const DragOverContainerEvent = exports.DragOverContainerEvent = (function (_DragEvent5) {
        (0, _inherits3.default)(DragOverContainerEvent, _DragEvent5)

        function DragOverContainerEvent () {
          (0, _classCallCheck3.default)(this, DragOverContainerEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragOverContainerEvent.__proto__ || Object.getPrototypeOf(DragOverContainerEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DragOverContainerEvent, [{
          key: 'overContainer',

          /**
                         * Draggable container you are over
                         * @property overContainer
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.overContainer
          }
        }])
        return DragOverContainerEvent
      }(DragEvent))

      /**
                 * Drag out container event
                 * @class DragOutContainerEvent
                 * @module DragOutContainerEvent
                 * @extends DragEvent
                 */

      DragOverContainerEvent.type = 'drag:over:container'

      const DragOutContainerEvent = exports.DragOutContainerEvent = (function (_DragEvent6) {
        (0, _inherits3.default)(DragOutContainerEvent, _DragEvent6)

        function DragOutContainerEvent () {
          (0, _classCallCheck3.default)(this, DragOutContainerEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragOutContainerEvent.__proto__ || Object.getPrototypeOf(DragOutContainerEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DragOutContainerEvent, [{
          key: 'overContainer',

          /**
                         * Draggable container you left
                         * @property overContainer
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.overContainer
          }
        }])
        return DragOutContainerEvent
      }(DragEvent))

      /**
                 * Drag pressure event
                 * @class DragPressureEvent
                 * @module DragPressureEvent
                 * @extends DragEvent
                 */

      DragOutContainerEvent.type = 'drag:out:container'

      const DragPressureEvent = exports.DragPressureEvent = (function (_DragEvent7) {
        (0, _inherits3.default)(DragPressureEvent, _DragEvent7)

        function DragPressureEvent () {
          (0, _classCallCheck3.default)(this, DragPressureEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragPressureEvent.__proto__ || Object.getPrototypeOf(DragPressureEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(DragPressureEvent, [{
          key: 'pressure',

          /**
                         * Pressure applied on draggable element
                         * @property pressure
                         * @type {Number}
                         * @readonly
                         */
          get: function get () {
            return this.data.pressure
          }
        }])
        return DragPressureEvent
      }(DragEvent))

      /**
                 * Drag stop event
                 * @class DragStopEvent
                 * @module DragStopEvent
                 * @extends DragEvent
                 */

      DragPressureEvent.type = 'drag:pressure'

      const DragStopEvent = exports.DragStopEvent = (function (_DragEvent8) {
        (0, _inherits3.default)(DragStopEvent, _DragEvent8)

        function DragStopEvent () {
          (0, _classCallCheck3.default)(this, DragStopEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragStopEvent.__proto__ || Object.getPrototypeOf(DragStopEvent)).apply(this, arguments))
        }

        return DragStopEvent
      }(DragEvent))

      DragStopEvent.type = 'drag:stop'
      /***/
    },
    /* 143 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractPlugin2 = __webpack_require__(10)

      const _AbstractPlugin3 = _interopRequireDefault(_AbstractPlugin2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onSortableSorted = Symbol('onSortableSorted')

      /**
                 * SwapAnimation default options
                 * @property {Object} defaultOptions
                 * @property {Number} defaultOptions.duration
                 * @property {String} defaultOptions.easingFunction
                 * @property {Boolean} defaultOptions.horizontal
                 * @type {Object}
                 */
      const defaultOptions = exports.defaultOptions = {
        duration: 150,
        easingFunction: 'ease-in-out',
        horizontal: false
      }

      /**
                 * SwapAnimation plugin adds swap animations for sortable
                 * @class SwapAnimation
                 * @module SwapAnimation
                 * @extends AbstractPlugin
                 */

      const SwapAnimation = (function (_AbstractPlugin) {
        (0, _inherits3.default)(SwapAnimation, _AbstractPlugin)

        /**
                     * SwapAnimation constructor.
                     * @constructs SwapAnimation
                     * @param {Draggable} draggable - Draggable instance
                     */
        function SwapAnimation (draggable) {
          (0, _classCallCheck3.default)(this, SwapAnimation)

          /**
                         * SwapAnimation options
                         * @property {Object} options
                         * @property {Number} defaultOptions.duration
                         * @property {String} defaultOptions.easingFunction
                         * @type {Object}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (SwapAnimation.__proto__ || Object.getPrototypeOf(SwapAnimation)).call(this, draggable))

          _this.options = Object.assign({}, defaultOptions, _this.getOptions())

          /**
                         * Last animation frame
                         * @property {Number} lastAnimationFrame
                         * @type {Number}
                         */
          _this.lastAnimationFrame = null

          _this[onSortableSorted] = _this[onSortableSorted].bind(_this)
          return _this
        }

        /**
                     * Attaches plugins event listeners
                     */

        (0, _createClass3.default)(SwapAnimation, [{
          key: 'attach',
          value: function attach () {
            this.draggable.on('sortable:sorted', this[onSortableSorted])
          }

          /**
                         * Detaches plugins event listeners
                         */

        }, {
          key: 'detach',
          value: function detach () {
            this.draggable.off('sortable:sorted', this[onSortableSorted])
          }

          /**
                         * Returns options passed through draggable
                         * @return {Object}
                         */

        }, {
          key: 'getOptions',
          value: function getOptions () {
            return this.draggable.options.swapAnimation || {}
          }

          /**
                         * Sortable sorted handler
                         * @param {SortableSortedEvent} sortableEvent
                         * @private
                         */

        }, {
          key: onSortableSorted,
          value: function value (_ref) {
            const _this2 = this

            const oldIndex = _ref.oldIndex
            const newIndex = _ref.newIndex
            const dragEvent = _ref.dragEvent
            const source = dragEvent.source
            const over = dragEvent.over

            cancelAnimationFrame(this.lastAnimationFrame)

            // Can be done in a separate frame
            this.lastAnimationFrame = requestAnimationFrame(function () {
              if (oldIndex >= newIndex) {
                animate(source, over, _this2.options)
              } else {
                animate(over, source, _this2.options)
              }
            })
          }
        }])
        return SwapAnimation
      }(_AbstractPlugin3.default))

      /**
                 * Animates two elements
                 * @param {HTMLElement} from
                 * @param {HTMLElement} to
                 * @param {Object} options
                 * @param {Number} options.duration
                 * @param {String} options.easingFunction
                 * @param {String} options.horizontal
                 * @private
                 */

      exports.default = SwapAnimation

      function animate (from, to, _ref2) {
        const duration = _ref2.duration
        const easingFunction = _ref2.easingFunction
        const horizontal = _ref2.horizontal
        const _arr = [from, to]

        for (let _i = 0; _i < _arr.length; _i++) {
          const element = _arr[_i]
          element.style.pointerEvents = 'none'
        }

        if (horizontal) {
          const width = from.offsetWidth
          from.style.transform = 'translate3d(' + width + 'px, 0, 0)'
          to.style.transform = 'translate3d(-' + width + 'px, 0, 0)'
        } else {
          const height = from.offsetHeight
          from.style.transform = 'translate3d(0, ' + height + 'px, 0)'
          to.style.transform = 'translate3d(0, -' + height + 'px, 0)'
        }

        requestAnimationFrame(function () {
          const _arr2 = [from, to]

          for (let _i2 = 0; _i2 < _arr2.length; _i2++) {
            const _element = _arr2[_i2]
            _element.addEventListener('transitionend', resetElementOnTransitionEnd)
            _element.style.transition = 'transform ' + duration + 'ms ' + easingFunction
            _element.style.transform = ''
          }
        })
      }

      /**
                 * Resets animation style properties after animation has completed
                 * @param {Event} event
                 * @private
                 */
      function resetElementOnTransitionEnd (event) {
        event.target.style.transition = ''
        event.target.style.pointerEvents = ''
        event.target.removeEventListener('transitionend', resetElementOnTransitionEnd)
      }
      /***/
    },
    /* 144 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = undefined

      const _SwapAnimation = __webpack_require__(143)

      const _SwapAnimation2 = _interopRequireDefault(_SwapAnimation)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _SwapAnimation2.default
      exports.defaultOptions = _SwapAnimation.defaultOptions
      /***/
    },
    /* 145 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractPlugin2 = __webpack_require__(10)

      const _AbstractPlugin3 = _interopRequireDefault(_AbstractPlugin2)

      const _SnappableEvent = __webpack_require__(84)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onDragStart = Symbol('onDragStart')
      const onDragStop = Symbol('onDragStop')
      const onDragOver = Symbol('onDragOver')
      const onDragOut = Symbol('onDragOut')
      const onMirrorCreated = Symbol('onMirrorCreated')
      const onMirrorDestroy = Symbol('onMirrorDestroy')

      /**
                 * Snappable plugin which snaps draggable elements into place
                 * @class Snappable
                 * @module Snappable
                 * @extends AbstractPlugin
                 */

      const Snappable = (function (_AbstractPlugin) {
        (0, _inherits3.default)(Snappable, _AbstractPlugin)

        /**
                     * Snappable constructor.
                     * @constructs Snappable
                     * @param {Draggable} draggable - Draggable instance
                     */
        function Snappable (draggable) {
          (0, _classCallCheck3.default)(this, Snappable)

          /**
                         * Keeps track of the first source element
                         * @property {HTMLElement|null} firstSource
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (Snappable.__proto__ || Object.getPrototypeOf(Snappable)).call(this, draggable))

          _this.firstSource = null

          /**
                         * Keeps track of the mirror element
                         * @property {HTMLElement} mirror
                         */
          _this.mirror = null

          _this[onDragStart] = _this[onDragStart].bind(_this)
          _this[onDragStop] = _this[onDragStop].bind(_this)
          _this[onDragOver] = _this[onDragOver].bind(_this)
          _this[onDragOut] = _this[onDragOut].bind(_this)
          _this[onMirrorCreated] = _this[onMirrorCreated].bind(_this)
          _this[onMirrorDestroy] = _this[onMirrorDestroy].bind(_this)
          return _this
        }

        /**
                     * Attaches plugins event listeners
                     */

        (0, _createClass3.default)(Snappable, [{
          key: 'attach',
          value: function attach () {
            this.draggable.on('drag:start', this[onDragStart]).on('drag:stop', this[onDragStop]).on('drag:over', this[onDragOver]).on('drag:out', this[onDragOut]).on('droppable:over', this[onDragOver]).on('droppable:out', this[onDragOut]).on('mirror:created', this[onMirrorCreated]).on('mirror:destroy', this[onMirrorDestroy])
          }

          /**
                         * Detaches plugins event listeners
                         */

        }, {
          key: 'detach',
          value: function detach () {
            this.draggable.off('drag:start', this[onDragStart]).off('drag:stop', this[onDragStop]).off('drag:over', this[onDragOver]).off('drag:out', this[onDragOut]).off('droppable:over', this[onDragOver]).off('droppable:out', this[onDragOut]).off('mirror:created', this[onMirrorCreated]).off('mirror:destroy', this[onMirrorDestroy])
          }

          /**
                         * Drag start handler
                         * @private
                         * @param {DragStartEvent} event - Drag start event
                         */

        }, {
          key: onDragStart,
          value: function value (event) {
            if (event.canceled()) {
              return
            }

            this.firstSource = event.source
          }

          /**
                         * Drag stop handler
                         * @private
                         * @param {DragStopEvent} event - Drag stop event
                         */

        }, {
          key: onDragStop,
          value: function value () {
            this.firstSource = null
          }

          /**
                         * Drag over handler
                         * @private
                         * @param {DragOverEvent|DroppableOverEvent} event - Drag over event
                         */

        }, {
          key: onDragOver,
          value: function value (event) {
            const _this2 = this

            if (event.canceled()) {
              return
            }

            const source = event.source || event.dragEvent.source

            if (source === this.firstSource) {
              this.firstSource = null
              return
            }

            const snapInEvent = new _SnappableEvent.SnapInEvent({
              dragEvent: event,
              snappable: event.over || event.droppable
            })

            this.draggable.trigger(snapInEvent)

            if (snapInEvent.canceled()) {
              return
            }

            if (this.mirror) {
              this.mirror.style.display = 'none'
            }

            source.classList.remove(this.draggable.getClassNameFor('source:dragging'))
            source.classList.add(this.draggable.getClassNameFor('source:placed'))

            // Need to cancel this in drag out
            setTimeout(function () {
              source.classList.remove(_this2.draggable.getClassNameFor('source:placed'))
            }, this.draggable.options.placedTimeout)
          }

          /**
                         * Drag out handler
                         * @private
                         * @param {DragOutEvent|DroppableOutEvent} event - Drag out event
                         */

        }, {
          key: onDragOut,
          value: function value (event) {
            if (event.canceled()) {
              return
            }

            const source = event.source || event.dragEvent.source

            const snapOutEvent = new _SnappableEvent.SnapOutEvent({
              dragEvent: event,
              snappable: event.over || event.droppable
            })

            this.draggable.trigger(snapOutEvent)

            if (snapOutEvent.canceled()) {
              return
            }

            if (this.mirror) {
              this.mirror.style.display = ''
            }

            source.classList.add(this.draggable.getClassNameFor('source:dragging'))
          }

          /**
                         * Mirror created handler
                         * @param {MirrorCreatedEvent} mirrorEvent
                         * @private
                         */

        }, {
          key: onMirrorCreated,
          value: function value (_ref) {
            const mirror = _ref.mirror

            this.mirror = mirror
          }

          /**
                         * Mirror destroy handler
                         * @param {MirrorDestroyEvent} mirrorEvent
                         * @private
                         */

        }, {
          key: onMirrorDestroy,
          value: function value () {
            this.mirror = null
          }
        }])
        return Snappable
      }(_AbstractPlugin3.default))

      exports.default = Snappable
      /***/
    },
    /* 146 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.SnapOutEvent = exports.SnapInEvent = exports.SnapEvent = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractEvent2 = __webpack_require__(9)

      const _AbstractEvent3 = _interopRequireDefault(_AbstractEvent2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * Base snap event
                 * @class SnapEvent
                 * @module SnapEvent
                 * @extends AbstractEvent
                 */
      const SnapEvent = exports.SnapEvent = (function (_AbstractEvent) {
        (0, _inherits3.default)(SnapEvent, _AbstractEvent)

        function SnapEvent () {
          (0, _classCallCheck3.default)(this, SnapEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SnapEvent.__proto__ || Object.getPrototypeOf(SnapEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(SnapEvent, [{
          key: 'dragEvent',

          /**
                         * Drag event that triggered this snap event
                         * @property dragEvent
                         * @type {DragEvent}
                         * @readonly
                         */
          get: function get () {
            return this.data.dragEvent
          }

          /**
                         * Snappable element
                         * @property snappable
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'snappable',
          get: function get () {
            return this.data.snappable
          }
        }])
        return SnapEvent
      }(_AbstractEvent3.default))

      /**
                 * Snap in event
                 * @class SnapInEvent
                 * @module SnapInEvent
                 * @extends SnapEvent
                 */

      SnapEvent.type = 'snap'

      const SnapInEvent = exports.SnapInEvent = (function (_SnapEvent) {
        (0, _inherits3.default)(SnapInEvent, _SnapEvent)

        function SnapInEvent () {
          (0, _classCallCheck3.default)(this, SnapInEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SnapInEvent.__proto__ || Object.getPrototypeOf(SnapInEvent)).apply(this, arguments))
        }

        return SnapInEvent
      }(SnapEvent))

      /**
                 * Snap out event
                 * @class SnapOutEvent
                 * @module SnapOutEvent
                 * @extends SnapEvent
                 */

      SnapInEvent.type = 'snap:in'
      SnapInEvent.cancelable = true

      const SnapOutEvent = exports.SnapOutEvent = (function (_SnapEvent2) {
        (0, _inherits3.default)(SnapOutEvent, _SnapEvent2)

        function SnapOutEvent () {
          (0, _classCallCheck3.default)(this, SnapOutEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SnapOutEvent.__proto__ || Object.getPrototypeOf(SnapOutEvent)).apply(this, arguments))
        }

        return SnapOutEvent
      }(SnapEvent))

      SnapOutEvent.type = 'snap:out'
      SnapOutEvent.cancelable = true
      /***/
    },
    /* 147 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _SnappableEvent = __webpack_require__(84)

      Object.keys(_SnappableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _SnappableEvent[key]
          }
        })
      })

      const _Snappable = __webpack_require__(145)

      const _Snappable2 = _interopRequireDefault(_Snappable)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Snappable2.default
      /***/
    },
    /* 148 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractPlugin2 = __webpack_require__(10)

      const _AbstractPlugin3 = _interopRequireDefault(_AbstractPlugin2)

      const _utils = __webpack_require__(11)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onMirrorCreated = Symbol('onMirrorCreated')
      const onMirrorDestroy = Symbol('onMirrorDestroy')
      const onDragOver = Symbol('onDragOver')
      const resize = Symbol('resize')

      /**
                 * ResizeMirror default options
                 * @property {Object} defaultOptions
                 * @type {Object}
                 */
      const defaultOptions = exports.defaultOptions = {}

      /**
                 * The ResizeMirror plugin resizes the mirror element to the dimensions of the draggable element that the mirror is hovering over
                 * @class ResizeMirror
                 * @module ResizeMirror
                 * @extends AbstractPlugin
                 */

      const ResizeMirror = (function (_AbstractPlugin) {
        (0, _inherits3.default)(ResizeMirror, _AbstractPlugin)

        /**
                     * ResizeMirror constructor.
                     * @constructs ResizeMirror
                     * @param {Draggable} draggable - Draggable instance
                     */
        function ResizeMirror (draggable) {
          (0, _classCallCheck3.default)(this, ResizeMirror)

          /**
                         * ResizeMirror options
                         * @property {Object} options
                         * @type {Object}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (ResizeMirror.__proto__ || Object.getPrototypeOf(ResizeMirror)).call(this, draggable))

          _this.options = Object.assign({}, defaultOptions, _this.getOptions())

          /**
                         * ResizeMirror remembers the last width when resizing the mirror
                         * to avoid additional writes to the DOM
                         * @property {number} lastWidth
                         */
          _this.lastWidth = 0

          /**
                         * ResizeMirror remembers the last height when resizing the mirror
                         * to avoid additional writes to the DOM
                         * @property {number} lastHeight
                         */
          _this.lastHeight = 0

          /**
                         * Keeps track of the mirror element
                         * @property {HTMLElement} mirror
                         */
          _this.mirror = null

          _this[onMirrorCreated] = _this[onMirrorCreated].bind(_this)
          _this[onMirrorDestroy] = _this[onMirrorDestroy].bind(_this)
          _this[onDragOver] = _this[onDragOver].bind(_this)
          return _this
        }

        /**
                     * Attaches plugins event listeners
                     */

        (0, _createClass3.default)(ResizeMirror, [{
          key: 'attach',
          value: function attach () {
            this.draggable.on('mirror:created', this[onMirrorCreated]).on('drag:over', this[onDragOver]).on('drag:over:container', this[onDragOver])
          }

          /**
                         * Detaches plugins event listeners
                         */

        }, {
          key: 'detach',
          value: function detach () {
            this.draggable.off('mirror:created', this[onMirrorCreated]).off('mirror:destroy', this[onMirrorDestroy]).off('drag:over', this[onDragOver]).off('drag:over:container', this[onDragOver])
          }

          /**
                         * Returns options passed through draggable
                         * @return {Object}
                         */

        }, {
          key: 'getOptions',
          value: function getOptions () {
            return this.draggable.options.resizeMirror || {}
          }

          /**
                         * Mirror created handler
                         * @param {MirrorCreatedEvent} mirrorEvent
                         * @private
                         */

        }, {
          key: onMirrorCreated,
          value: function value (_ref) {
            const mirror = _ref.mirror

            this.mirror = mirror
          }

          /**
                         * Mirror destroy handler
                         * @param {MirrorDestroyEvent} mirrorEvent
                         * @private
                         */

        }, {
          key: onMirrorDestroy,
          value: function value () {
            this.mirror = null
          }

          /**
                         * Drag over handler
                         * @param {DragOverEvent | DragOverContainer} dragEvent
                         * @private
                         */

        }, {
          key: onDragOver,
          value: function value (dragEvent) {
            this[resize](dragEvent)
          }

          /**
                         * Resize function for
                         * @param {DragOverEvent | DragOverContainer} dragEvent
                         * @private
                         */

        }, {
          key: resize,
          value: function value (_ref2) {
            const _this2 = this

            const overContainer = _ref2.overContainer
            const over = _ref2.over

            requestAnimationFrame(function () {
              if (_this2.mirror.parentNode !== overContainer) {
                overContainer.appendChild(_this2.mirror)
              }

              const overElement = over || _this2.draggable.getDraggableElementsForContainer(overContainer)[0]

              if (!overElement) {
                return
              }

              (0, _utils.requestNextAnimationFrame)(function () {
                const overRect = overElement.getBoundingClientRect()

                if (_this2.lastHeight === overRect.height && _this2.lastWidth === overRect.width) {
                  return
                }

                _this2.mirror.style.width = overRect.width + 'px'
                _this2.mirror.style.height = overRect.height + 'px'

                _this2.lastWidth = overRect.width
                _this2.lastHeight = overRect.height
              })
            })
          }
        }])
        return ResizeMirror
      }(_AbstractPlugin3.default))

      exports.default = ResizeMirror
      /***/
    },
    /* 149 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.defaultOptions = undefined

      const _ResizeMirror = __webpack_require__(148)

      const _ResizeMirror2 = _interopRequireDefault(_ResizeMirror)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _ResizeMirror2.default
      exports.defaultOptions = _ResizeMirror.defaultOptions
      /***/
    },
    /* 150 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractPlugin2 = __webpack_require__(10)

      const _AbstractPlugin3 = _interopRequireDefault(_AbstractPlugin2)

      const _utils = __webpack_require__(11)

      const _CollidableEvent = __webpack_require__(85)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onDragMove = Symbol('onDragMove')
      const onDragStop = Symbol('onDragStop')
      const onRequestAnimationFrame = Symbol('onRequestAnimationFrame')

      /**
                 * Collidable plugin which detects colliding elements while dragging
                 * @class Collidable
                 * @module Collidable
                 * @extends AbstractPlugin
                 */

      const Collidable = (function (_AbstractPlugin) {
        (0, _inherits3.default)(Collidable, _AbstractPlugin)

        /**
                     * Collidable constructor.
                     * @constructs Collidable
                     * @param {Draggable} draggable - Draggable instance
                     */
        function Collidable (draggable) {
          (0, _classCallCheck3.default)(this, Collidable)

          /**
                         * Keeps track of currently colliding elements
                         * @property {HTMLElement|null} currentlyCollidingElement
                         * @type {HTMLElement|null}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (Collidable.__proto__ || Object.getPrototypeOf(Collidable)).call(this, draggable))

          _this.currentlyCollidingElement = null

          /**
                         * Keeps track of currently colliding elements
                         * @property {HTMLElement|null} lastCollidingElement
                         * @type {HTMLElement|null}
                         */
          _this.lastCollidingElement = null

          /**
                         * Animation frame for finding colliding elements
                         * @property {Number|null} currentAnimationFrame
                         * @type {Number|null}
                         */
          _this.currentAnimationFrame = null

          _this[onDragMove] = _this[onDragMove].bind(_this)
          _this[onDragStop] = _this[onDragStop].bind(_this)
          _this[onRequestAnimationFrame] = _this[onRequestAnimationFrame].bind(_this)
          return _this
        }

        /**
                     * Attaches plugins event listeners
                     */

        (0, _createClass3.default)(Collidable, [{
          key: 'attach',
          value: function attach () {
            this.draggable.on('drag:move', this[onDragMove]).on('drag:stop', this[onDragStop])
          }

          /**
                         * Detaches plugins event listeners
                         */

        }, {
          key: 'detach',
          value: function detach () {
            this.draggable.off('drag:move', this[onDragMove]).off('drag:stop', this[onDragStop])
          }

          /**
                         * Returns current collidables based on `collidables` option
                         * @return {HTMLElement[]}
                         */

        }, {
          key: 'getCollidables',
          value: function getCollidables () {
            const collidables = this.draggable.options.collidables

            if (typeof collidables === 'string') {
              return Array.prototype.slice.call(document.querySelectorAll(collidables))
            } else if (collidables instanceof NodeList || collidables instanceof Array) {
              return Array.prototype.slice.call(collidables)
            } else if (collidables instanceof HTMLElement) {
              return [collidables]
            } else if (typeof collidables === 'function') {
              return collidables()
            } else {
              return []
            }
          }

          /**
                         * Drag move handler
                         * @private
                         * @param {DragMoveEvent} event - Drag move event
                         */

        }, {
          key: onDragMove,
          value: function value (event) {
            const target = event.sensorEvent.target

            this.currentAnimationFrame = requestAnimationFrame(this[onRequestAnimationFrame](target))

            if (this.currentlyCollidingElement) {
              event.cancel()
            }

            const collidableInEvent = new _CollidableEvent.CollidableInEvent({
              dragEvent: event,
              collidingElement: this.currentlyCollidingElement
            })

            const collidableOutEvent = new _CollidableEvent.CollidableOutEvent({
              dragEvent: event,
              collidingElement: this.lastCollidingElement
            })

            const enteringCollidable = Boolean(this.currentlyCollidingElement && this.lastCollidingElement !== this.currentlyCollidingElement)
            const leavingCollidable = Boolean(!this.currentlyCollidingElement && this.lastCollidingElement)

            if (enteringCollidable) {
              if (this.lastCollidingElement) {
                this.draggable.trigger(collidableOutEvent)
              }

              this.draggable.trigger(collidableInEvent)
            } else if (leavingCollidable) {
              this.draggable.trigger(collidableOutEvent)
            }

            this.lastCollidingElement = this.currentlyCollidingElement
          }

          /**
                         * Drag stop handler
                         * @private
                         * @param {DragStopEvent} event - Drag stop event
                         */

        }, {
          key: onDragStop,
          value: function value (event) {
            const lastCollidingElement = this.currentlyCollidingElement || this.lastCollidingElement
            const collidableOutEvent = new _CollidableEvent.CollidableOutEvent({
              dragEvent: event,
              collidingElement: lastCollidingElement
            })

            if (lastCollidingElement) {
              this.draggable.trigger(collidableOutEvent)
            }

            this.lastCollidingElement = null
            this.currentlyCollidingElement = null
          }

          /**
                         * Animation frame function
                         * @private
                         * @param {HTMLElement} target - Current move target
                         * @return {Function}
                         */

        }, {
          key: onRequestAnimationFrame,
          value: function value (target) {
            const _this2 = this

            return function () {
              const collidables = _this2.getCollidables()
              _this2.currentlyCollidingElement = (0, _utils.closest)(target, function (element) {
                return collidables.includes(element)
              })
            }
          }
        }])
        return Collidable
      }(_AbstractPlugin3.default))

      exports.default = Collidable
      /***/
    },
    /* 151 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.CollidableOutEvent = exports.CollidableInEvent = exports.CollidableEvent = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractEvent2 = __webpack_require__(9)

      const _AbstractEvent3 = _interopRequireDefault(_AbstractEvent2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * Base collidable event
                 * @class CollidableEvent
                 * @module CollidableEvent
                 * @extends AbstractEvent
                 */
      const CollidableEvent = exports.CollidableEvent = (function (_AbstractEvent) {
        (0, _inherits3.default)(CollidableEvent, _AbstractEvent)

        function CollidableEvent () {
          (0, _classCallCheck3.default)(this, CollidableEvent)
          return (0, _possibleConstructorReturn3.default)(this, (CollidableEvent.__proto__ || Object.getPrototypeOf(CollidableEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(CollidableEvent, [{
          key: 'dragEvent',

          /**
                         * Drag event that triggered this colliable event
                         * @property dragEvent
                         * @type {DragEvent}
                         * @readonly
                         */
          get: function get () {
            return this.data.dragEvent
          }
        }])
        return CollidableEvent
      }(_AbstractEvent3.default))

      /**
                 * Collidable in event
                 * @class CollidableInEvent
                 * @module CollidableInEvent
                 * @extends CollidableEvent
                 */

      CollidableEvent.type = 'collidable'

      const CollidableInEvent = exports.CollidableInEvent = (function (_CollidableEvent) {
        (0, _inherits3.default)(CollidableInEvent, _CollidableEvent)

        function CollidableInEvent () {
          (0, _classCallCheck3.default)(this, CollidableInEvent)
          return (0, _possibleConstructorReturn3.default)(this, (CollidableInEvent.__proto__ || Object.getPrototypeOf(CollidableInEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(CollidableInEvent, [{
          key: 'collidingElement',

          /**
                         * Element you are currently colliding with
                         * @property collidingElement
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.collidingElement
          }
        }])
        return CollidableInEvent
      }(CollidableEvent))

      /**
                 * Collidable out event
                 * @class CollidableOutEvent
                 * @module CollidableOutEvent
                 * @extends CollidableEvent
                 */

      CollidableInEvent.type = 'collidable:in'

      const CollidableOutEvent = exports.CollidableOutEvent = (function (_CollidableEvent2) {
        (0, _inherits3.default)(CollidableOutEvent, _CollidableEvent2)

        function CollidableOutEvent () {
          (0, _classCallCheck3.default)(this, CollidableOutEvent)
          return (0, _possibleConstructorReturn3.default)(this, (CollidableOutEvent.__proto__ || Object.getPrototypeOf(CollidableOutEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(CollidableOutEvent, [{
          key: 'collidingElement',

          /**
                         * Element you were previously colliding with
                         * @property collidingElement
                         * @type {HTMLElement}
                         * @readonly
                         */
          get: function get () {
            return this.data.collidingElement
          }
        }])
        return CollidableOutEvent
      }(CollidableEvent))

      CollidableOutEvent.type = 'collidable:out'
      /***/
    },
    /* 152 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _CollidableEvent = __webpack_require__(85)

      Object.keys(_CollidableEvent).forEach(function (key) {
        if (key === 'default' || key === '__esModule') return
        Object.defineProperty(exports, key, {
          enumerable: true,
          get: function get () {
            return _CollidableEvent[key]
          }
        })
      })

      const _Collidable = __webpack_require__(150)

      const _Collidable2 = _interopRequireDefault(_Collidable)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _Collidable2.default
      /***/
    },
    /* 153 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _Collidable = __webpack_require__(152)

      Object.defineProperty(exports, 'Collidable', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_Collidable).default
        }
      })

      const _ResizeMirror = __webpack_require__(149)

      Object.defineProperty(exports, 'ResizeMirror', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_ResizeMirror).default
        }
      })
      Object.defineProperty(exports, 'defaultResizeMirrorOptions', {
        enumerable: true,
        get: function get () {
          return _ResizeMirror.defaultOptions
        }
      })

      const _Snappable = __webpack_require__(147)

      Object.defineProperty(exports, 'Snappable', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_Snappable).default
        }
      })

      const _SwapAnimation = __webpack_require__(144)

      Object.defineProperty(exports, 'SwapAnimation', {
        enumerable: true,
        get: function get () {
          return _interopRequireDefault(_SwapAnimation).default
        }
      })
      Object.defineProperty(exports, 'defaultSwapAnimationOptions', {
        enumerable: true,
        get: function get () {
          return _SwapAnimation.defaultOptions
        }
      })

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }
      /***/
    },
    /* 154 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _Sensor2 = __webpack_require__(34)

      const _Sensor3 = _interopRequireDefault(_Sensor2)

      const _SensorEvent = __webpack_require__(32)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onMouseForceWillBegin = Symbol('onMouseForceWillBegin')
      const onMouseForceDown = Symbol('onMouseForceDown')
      const onMouseDown = Symbol('onMouseDown')
      const onMouseForceChange = Symbol('onMouseForceChange')
      const onMouseMove = Symbol('onMouseMove')
      const onMouseUp = Symbol('onMouseUp')
      const onMouseForceGlobalChange = Symbol('onMouseForceGlobalChange')

      /**
                 * This sensor picks up native force touch events and dictates drag operations
                 * @class ForceTouchSensor
                 * @module ForceTouchSensor
                 * @extends Sensor
                 */

      const ForceTouchSensor = (function (_Sensor) {
        (0, _inherits3.default)(ForceTouchSensor, _Sensor)

        /**
                     * ForceTouchSensor constructor.
                     * @constructs ForceTouchSensor
                     * @param {HTMLElement[]|NodeList|HTMLElement} containers - Containers
                     * @param {Object} options - Options
                     */
        function ForceTouchSensor () {
          const containers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : []
          const options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          (0, _classCallCheck3.default)(this, ForceTouchSensor)

          /**
                         * Draggable element needs to be remembered to unset the draggable attribute after drag operation has completed
                         * @property mightDrag
                         * @type {Boolean}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (ForceTouchSensor.__proto__ || Object.getPrototypeOf(ForceTouchSensor)).call(this, containers, options))

          _this.mightDrag = false

          _this[onMouseForceWillBegin] = _this[onMouseForceWillBegin].bind(_this)
          _this[onMouseForceDown] = _this[onMouseForceDown].bind(_this)
          _this[onMouseDown] = _this[onMouseDown].bind(_this)
          _this[onMouseForceChange] = _this[onMouseForceChange].bind(_this)
          _this[onMouseMove] = _this[onMouseMove].bind(_this)
          _this[onMouseUp] = _this[onMouseUp].bind(_this)
          return _this
        }

        /**
                     * Attaches sensors event listeners to the DOM
                     */

        (0, _createClass3.default)(ForceTouchSensor, [{
          key: 'attach',
          value: function attach () {
            let _iteratorNormalCompletion = true
            let _didIteratorError = false
            let _iteratorError

            try {
              for (var _iterator = this.containers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                const container = _step.value

                container.addEventListener('webkitmouseforcewillbegin', this[onMouseForceWillBegin], false)
                container.addEventListener('webkitmouseforcedown', this[onMouseForceDown], false)
                container.addEventListener('mousedown', this[onMouseDown], true)
                container.addEventListener('webkitmouseforcechanged', this[onMouseForceChange], false)
              }
            } catch (err) {
              _didIteratorError = true
              _iteratorError = err
            } finally {
              try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                  _iterator.return()
                }
              } finally {
                if (_didIteratorError) {
                  throw _iteratorError
                }
              }
            }

            document.addEventListener('mousemove', this[onMouseMove])
            document.addEventListener('mouseup', this[onMouseUp])
          }

          /**
                         * Detaches sensors event listeners to the DOM
                         */

        }, {
          key: 'detach',
          value: function detach () {
            let _iteratorNormalCompletion2 = true
            let _didIteratorError2 = false
            let _iteratorError2

            try {
              for (var _iterator2 = this.containers[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                const container = _step2.value

                container.removeEventListener('webkitmouseforcewillbegin', this[onMouseForceWillBegin], false)
                container.removeEventListener('webkitmouseforcedown', this[onMouseForceDown], false)
                container.removeEventListener('mousedown', this[onMouseDown], true)
                container.removeEventListener('webkitmouseforcechanged', this[onMouseForceChange], false)
              }
            } catch (err) {
              _didIteratorError2 = true
              _iteratorError2 = err
            } finally {
              try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                  _iterator2.return()
                }
              } finally {
                if (_didIteratorError2) {
                  throw _iteratorError2
                }
              }
            }

            document.removeEventListener('mousemove', this[onMouseMove])
            document.removeEventListener('mouseup', this[onMouseUp])
          }

          /**
                         * Mouse force will begin handler
                         * @private
                         * @param {Event} event - Mouse force will begin event
                         */

        }, {
          key: onMouseForceWillBegin,
          value: function value (event) {
            event.preventDefault()
            this.mightDrag = true
          }

          /**
                         * Mouse force down handler
                         * @private
                         * @param {Event} event - Mouse force down event
                         */

        }, {
          key: onMouseForceDown,
          value: function value (event) {
            if (this.dragging) {
              return
            }

            const target = document.elementFromPoint(event.clientX, event.clientY)
            const container = event.currentTarget

            const dragStartEvent = new _SensorEvent.DragStartSensorEvent({
              clientX: event.clientX,
              clientY: event.clientY,
              target: target,
              container: container,
              originalEvent: event
            })

            this.trigger(container, dragStartEvent)

            this.currentContainer = container
            this.dragging = !dragStartEvent.canceled()
            this.mightDrag = false
          }

          /**
                         * Mouse up handler
                         * @private
                         * @param {Event} event - Mouse up event
                         */

        }, {
          key: onMouseUp,
          value: function value (event) {
            if (!this.dragging) {
              return
            }

            const dragStopEvent = new _SensorEvent.DragStopSensorEvent({
              clientX: event.clientX,
              clientY: event.clientY,
              target: null,
              container: this.currentContainer,
              originalEvent: event
            })

            this.trigger(this.currentContainer, dragStopEvent)

            this.currentContainer = null
            this.dragging = false
            this.mightDrag = false
          }

          /**
                         * Mouse down handler
                         * @private
                         * @param {Event} event - Mouse down event
                         */

        }, {
          key: onMouseDown,
          value: function value (event) {
            if (!this.mightDrag) {
              return
            }

            // Need workaround for real click
            // Cancel potential drag events
            event.stopPropagation()
            event.stopImmediatePropagation()
            event.preventDefault()
          }

          /**
                         * Mouse move handler
                         * @private
                         * @param {Event} event - Mouse force will begin event
                         */

        }, {
          key: onMouseMove,
          value: function value (event) {
            if (!this.dragging) {
              return
            }

            const target = document.elementFromPoint(event.clientX, event.clientY)

            const dragMoveEvent = new _SensorEvent.DragMoveSensorEvent({
              clientX: event.clientX,
              clientY: event.clientY,
              target: target,
              container: this.currentContainer,
              originalEvent: event
            })

            this.trigger(this.currentContainer, dragMoveEvent)
          }

          /**
                         * Mouse force change handler
                         * @private
                         * @param {Event} event - Mouse force change event
                         */

        }, {
          key: onMouseForceChange,
          value: function value (event) {
            if (this.dragging) {
              return
            }

            const target = event.target
            const container = event.currentTarget

            const dragPressureEvent = new _SensorEvent.DragPressureSensorEvent({
              pressure: event.webkitForce,
              clientX: event.clientX,
              clientY: event.clientY,
              target: target,
              container: container,
              originalEvent: event
            })

            this.trigger(container, dragPressureEvent)
          }

          /**
                         * Mouse force global change handler
                         * @private
                         * @param {Event} event - Mouse force global change event
                         */

        }, {
          key: onMouseForceGlobalChange,
          value: function value (event) {
            if (!this.dragging) {
              return
            }

            const target = event.target

            const dragPressureEvent = new _SensorEvent.DragPressureSensorEvent({
              pressure: event.webkitForce,
              clientX: event.clientX,
              clientY: event.clientY,
              target: target,
              container: this.currentContainer,
              originalEvent: event
            })

            this.trigger(this.currentContainer, dragPressureEvent)
          }
        }])
        return ForceTouchSensor
      }(_Sensor3.default))

      exports.default = ForceTouchSensor
      /***/
    },
    /* 155 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _ForceTouchSensor = __webpack_require__(154)

      const _ForceTouchSensor2 = _interopRequireDefault(_ForceTouchSensor)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _ForceTouchSensor2.default
      /***/
    },
    /* 156 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _utils = __webpack_require__(11)

      const _Sensor2 = __webpack_require__(34)

      const _Sensor3 = _interopRequireDefault(_Sensor2)

      const _SensorEvent = __webpack_require__(32)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onMouseDown = Symbol('onMouseDown')
      const onMouseUp = Symbol('onMouseUp')
      const onDragStart = Symbol('onDragStart')
      const onDragOver = Symbol('onDragOver')
      const onDragEnd = Symbol('onDragEnd')
      const onDrop = Symbol('onDrop')
      const reset = Symbol('reset')

      /**
                 * This sensor picks up native browser drag events and dictates drag operations
                 * @class DragSensor
                 * @module DragSensor
                 * @extends Sensor
                 */

      const DragSensor = (function (_Sensor) {
        (0, _inherits3.default)(DragSensor, _Sensor)

        /**
                     * DragSensor constructor.
                     * @constructs DragSensor
                     * @param {HTMLElement[]|NodeList|HTMLElement} containers - Containers
                     * @param {Object} options - Options
                     */
        function DragSensor () {
          const containers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : []
          const options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          (0, _classCallCheck3.default)(this, DragSensor)

          /**
                         * Mouse down timer which will end up setting the draggable attribute, unless canceled
                         * @property mouseDownTimeout
                         * @type {Number}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (DragSensor.__proto__ || Object.getPrototypeOf(DragSensor)).call(this, containers, options))

          _this.mouseDownTimeout = null

          /**
                         * Draggable element needs to be remembered to unset the draggable attribute after drag operation has completed
                         * @property draggableElement
                         * @type {HTMLElement}
                         */
          _this.draggableElement = null

          /**
                         * Native draggable element could be links or images, their draggable state will be disabled during drag operation
                         * @property nativeDraggableElement
                         * @type {HTMLElement}
                         */
          _this.nativeDraggableElement = null

          _this[onMouseDown] = _this[onMouseDown].bind(_this)
          _this[onMouseUp] = _this[onMouseUp].bind(_this)
          _this[onDragStart] = _this[onDragStart].bind(_this)
          _this[onDragOver] = _this[onDragOver].bind(_this)
          _this[onDragEnd] = _this[onDragEnd].bind(_this)
          _this[onDrop] = _this[onDrop].bind(_this)
          return _this
        }

        /**
                     * Attaches sensors event listeners to the DOM
                     */

        (0, _createClass3.default)(DragSensor, [{
          key: 'attach',
          value: function attach () {
            document.addEventListener('mousedown', this[onMouseDown], true)
          }

          /**
                         * Detaches sensors event listeners to the DOM
                         */

        }, {
          key: 'detach',
          value: function detach () {
            document.removeEventListener('mousedown', this[onMouseDown], true)
          }

          /**
                         * Drag start handler
                         * @private
                         * @param {Event} event - Drag start event
                         */

        }, {
          key: onDragStart,
          value: function value (event) {
            const _this2 = this

            // Need for firefox. "text" key is needed for IE
            event.dataTransfer.setData('text', '')
            event.dataTransfer.effectAllowed = this.options.type

            const target = document.elementFromPoint(event.clientX, event.clientY)
            this.currentContainer = (0, _utils.closest)(event.target, this.containers)

            if (!this.currentContainer) {
              return
            }

            const dragStartEvent = new _SensorEvent.DragStartSensorEvent({
              clientX: event.clientX,
              clientY: event.clientY,
              target: target,
              container: this.currentContainer,
              originalEvent: event
            })

            // Workaround
            setTimeout(function () {
              _this2.trigger(_this2.currentContainer, dragStartEvent)

              if (dragStartEvent.canceled()) {
                _this2.dragging = false
              } else {
                _this2.dragging = true
              }
            }, 0)
          }

          /**
                         * Drag over handler
                         * @private
                         * @param {Event} event - Drag over event
                         */

        }, {
          key: onDragOver,
          value: function value (event) {
            if (!this.dragging) {
              return
            }

            const target = document.elementFromPoint(event.clientX, event.clientY)
            const container = this.currentContainer

            const dragMoveEvent = new _SensorEvent.DragMoveSensorEvent({
              clientX: event.clientX,
              clientY: event.clientY,
              target: target,
              container: container,
              originalEvent: event
            })

            this.trigger(container, dragMoveEvent)

            if (!dragMoveEvent.canceled()) {
              event.preventDefault()
              event.dataTransfer.dropEffect = this.options.type
            }
          }

          /**
                         * Drag end handler
                         * @private
                         * @param {Event} event - Drag end event
                         */

        }, {
          key: onDragEnd,
          value: function value (event) {
            if (!this.dragging) {
              return
            }

            document.removeEventListener('mouseup', this[onMouseUp], true)

            const target = document.elementFromPoint(event.clientX, event.clientY)
            const container = this.currentContainer

            const dragStopEvent = new _SensorEvent.DragStopSensorEvent({
              clientX: event.clientX,
              clientY: event.clientY,
              target: target,
              container: container,
              originalEvent: event
            })

            this.trigger(container, dragStopEvent)

            this.dragging = false

            this[reset]()
          }

          /**
                         * Drop handler
                         * @private
                         * @param {Event} event - Drop event
                         */

        }, {
          key: onDrop,
          value: function value (event) {
            // eslint-disable-line class-methods-use-this
            event.preventDefault()
          }

          /**
                         * Mouse down handler
                         * @private
                         * @param {Event} event - Mouse down event
                         */

        }, {
          key: onMouseDown,
          value: function value (event) {
            const _this3 = this

            // Firefox bug for inputs within draggables https://bugzilla.mozilla.org/show_bug.cgi?id=739071
            if (event.target && (event.target.form || event.target.contenteditable)) {
              return
            }

            const nativeDraggableElement = (0, _utils.closest)(event.target, function (element) {
              return element.draggable
            })

            if (nativeDraggableElement) {
              nativeDraggableElement.draggable = false
              this.nativeDraggableElement = nativeDraggableElement
            }

            document.addEventListener('mouseup', this[onMouseUp], true)
            document.addEventListener('dragstart', this[onDragStart], false)
            document.addEventListener('dragover', this[onDragOver], false)
            document.addEventListener('dragend', this[onDragEnd], false)
            document.addEventListener('drop', this[onDrop], false)

            const target = (0, _utils.closest)(event.target, this.options.draggable)

            if (!target) {
              return
            }

            this.mouseDownTimeout = setTimeout(function () {
              target.draggable = true
              _this3.draggableElement = target
            }, this.options.delay)
          }

          /**
                         * Mouse up handler
                         * @private
                         * @param {Event} event - Mouse up event
                         */

        }, {
          key: onMouseUp,
          value: function value () {
            this[reset]()
          }

          /**
                         * Mouse up handler
                         * @private
                         * @param {Event} event - Mouse up event
                         */

        }, {
          key: reset,
          value: function value () {
            clearTimeout(this.mouseDownTimeout)

            document.removeEventListener('mouseup', this[onMouseUp], true)
            document.removeEventListener('dragstart', this[onDragStart], false)
            document.removeEventListener('dragover', this[onDragOver], false)
            document.removeEventListener('dragend', this[onDragEnd], false)
            document.removeEventListener('drop', this[onDrop], false)

            if (this.nativeDraggableElement) {
              this.nativeDraggableElement.draggable = true
              this.nativeDraggableElement = null
            }

            if (this.draggableElement) {
              this.draggableElement.draggable = false
              this.draggableElement = null
            }
          }
        }])
        return DragSensor
      }(_Sensor3.default))

      exports.default = DragSensor
      /***/
    },
    /* 157 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _DragSensor = __webpack_require__(156)

      const _DragSensor2 = _interopRequireDefault(_DragSensor)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _DragSensor2.default
      /***/
    },
    /* 158 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _utils = __webpack_require__(11)

      const _Sensor2 = __webpack_require__(34)

      const _Sensor3 = _interopRequireDefault(_Sensor2)

      const _SensorEvent = __webpack_require__(32)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onTouchStart = Symbol('onTouchStart')
      const onTouchHold = Symbol('onTouchHold')
      const onTouchEnd = Symbol('onTouchEnd')
      const onTouchMove = Symbol('onTouchMove')

      /**
                 * Prevents scrolling when set to true
                 * @var {Boolean} preventScrolling
                 */
      let preventScrolling = false

      // WebKit requires cancelable `touchmove` events to be added as early as possible
      window.addEventListener('touchmove', function (event) {
        if (!preventScrolling) {
          return
        }

        // Prevent scrolling
        event.preventDefault()
      }, { passive: false })

      /**
                 * This sensor picks up native browser touch events and dictates drag operations
                 * @class TouchSensor
                 * @module TouchSensor
                 * @extends Sensor
                 */

      const TouchSensor = (function (_Sensor) {
        (0, _inherits3.default)(TouchSensor, _Sensor)

        /**
                     * TouchSensor constructor.
                     * @constructs TouchSensor
                     * @param {HTMLElement[]|NodeList|HTMLElement} containers - Containers
                     * @param {Object} options - Options
                     */
        function TouchSensor () {
          const containers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : []
          const options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          (0, _classCallCheck3.default)(this, TouchSensor)

          /**
                         * Closest scrollable container so accidental scroll can cancel long touch
                         * @property currentScrollableParent
                         * @type {HTMLElement}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (TouchSensor.__proto__ || Object.getPrototypeOf(TouchSensor)).call(this, containers, options))

          _this.currentScrollableParent = null

          /**
                         * TimeoutID for long touch
                         * @property tapTimeout
                         * @type {Number}
                         */
          _this.tapTimeout = null

          /**
                         * touchMoved indicates if touch has moved during tapTimeout
                         * @property touchMoved
                         * @type {Boolean}
                         */
          _this.touchMoved = false

          _this[onTouchStart] = _this[onTouchStart].bind(_this)
          _this[onTouchHold] = _this[onTouchHold].bind(_this)
          _this[onTouchEnd] = _this[onTouchEnd].bind(_this)
          _this[onTouchMove] = _this[onTouchMove].bind(_this)
          return _this
        }

        /**
                     * Attaches sensors event listeners to the DOM
                     */

        (0, _createClass3.default)(TouchSensor, [{
          key: 'attach',
          value: function attach () {
            document.addEventListener('touchstart', this[onTouchStart])
          }

          /**
                         * Detaches sensors event listeners to the DOM
                         */

        }, {
          key: 'detach',
          value: function detach () {
            document.removeEventListener('touchstart', this[onTouchStart])
          }

          /**
                         * Touch start handler
                         * @private
                         * @param {Event} event - Touch start event
                         */

        }, {
          key: onTouchStart,
          value: function value (event) {
            const container = (0, _utils.closest)(event.target, this.containers)

            if (!container) {
              return
            }

            document.addEventListener('touchmove', this[onTouchMove])
            document.addEventListener('touchend', this[onTouchEnd])
            document.addEventListener('touchcancel', this[onTouchEnd])
            container.addEventListener('contextmenu', onContextMenu)

            this.currentContainer = container
            this.tapTimeout = setTimeout(this[onTouchHold](event, container), this.options.delay)
          }

          /**
                         * Touch hold handler
                         * @private
                         * @param {Event} event - Touch start event
                         * @param {HTMLElement} container - Container element
                         */

        }, {
          key: onTouchHold,
          value: function value (event, container) {
            const _this2 = this

            return function () {
              if (_this2.touchMoved) {
                return
              }

              const touch = event.touches[0] || event.changedTouches[0]
              const target = event.target

              const dragStartEvent = new _SensorEvent.DragStartSensorEvent({
                clientX: touch.pageX,
                clientY: touch.pageY,
                target: target,
                container: container,
                originalEvent: event
              })

              _this2.trigger(container, dragStartEvent)

              _this2.dragging = !dragStartEvent.canceled()
              preventScrolling = _this2.dragging
            }
          }

          /**
                         * Touch move handler
                         * @private
                         * @param {Event} event - Touch move event
                         */

        }, {
          key: onTouchMove,
          value: function value (event) {
            this.touchMoved = true

            if (!this.dragging) {
              return
            }

            const touch = event.touches[0] || event.changedTouches[0]
            const target = document.elementFromPoint(touch.pageX - window.scrollX, touch.pageY - window.scrollY)

            const dragMoveEvent = new _SensorEvent.DragMoveSensorEvent({
              clientX: touch.pageX,
              clientY: touch.pageY,
              target: target,
              container: this.currentContainer,
              originalEvent: event
            })

            this.trigger(this.currentContainer, dragMoveEvent)
          }

          /**
                         * Touch end handler
                         * @private
                         * @param {Event} event - Touch end event
                         */

        }, {
          key: onTouchEnd,
          value: function value (event) {
            this.touchMoved = false
            preventScrolling = false

            document.removeEventListener('touchend', this[onTouchEnd])
            document.removeEventListener('touchcancel', this[onTouchEnd])
            document.removeEventListener('touchmove', this[onTouchMove])

            if (this.currentContainer) {
              this.currentContainer.removeEventListener('contextmenu', onContextMenu)
            }

            clearTimeout(this.tapTimeout)

            if (!this.dragging) {
              return
            }

            const touch = event.touches[0] || event.changedTouches[0]
            const target = document.elementFromPoint(touch.pageX - window.scrollX, touch.pageY - window.scrollY)

            event.preventDefault()

            const dragStopEvent = new _SensorEvent.DragStopSensorEvent({
              clientX: touch.pageX,
              clientY: touch.pageY,
              target: target,
              container: this.currentContainer,
              originalEvent: event
            })

            this.trigger(this.currentContainer, dragStopEvent)

            this.currentContainer = null
            this.dragging = false
          }
        }])
        return TouchSensor
      }(_Sensor3.default))

      exports.default = TouchSensor

      function onContextMenu (event) {
        event.preventDefault()
        event.stopPropagation()
      }
      /***/
    },
    /* 159 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _TouchSensor = __webpack_require__(158)

      const _TouchSensor2 = _interopRequireDefault(_TouchSensor)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _TouchSensor2.default
      /***/
    },
    /* 160 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.DragPressureSensorEvent = exports.DragStopSensorEvent = exports.DragMoveSensorEvent = exports.DragStartSensorEvent = exports.SensorEvent = undefined

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _AbstractEvent2 = __webpack_require__(9)

      const _AbstractEvent3 = _interopRequireDefault(_AbstractEvent2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * Base sensor event
                 * @class SensorEvent
                 * @module SensorEvent
                 * @extends AbstractEvent
                 */
      const SensorEvent = exports.SensorEvent = (function (_AbstractEvent) {
        (0, _inherits3.default)(SensorEvent, _AbstractEvent)

        function SensorEvent () {
          (0, _classCallCheck3.default)(this, SensorEvent)
          return (0, _possibleConstructorReturn3.default)(this, (SensorEvent.__proto__ || Object.getPrototypeOf(SensorEvent)).apply(this, arguments))
        }

        (0, _createClass3.default)(SensorEvent, [{
          key: 'originalEvent',

          /**
                         * Original browser event that triggered a sensor
                         * @property originalEvent
                         * @type {Event}
                         * @readonly
                         */
          get: function get () {
            return this.data.originalEvent
          }

          /**
                         * Normalized clientX for both touch and mouse events
                         * @property clientX
                         * @type {Number}
                         * @readonly
                         */

        }, {
          key: 'clientX',
          get: function get () {
            return this.data.clientX
          }

          /**
                         * Normalized clientY for both touch and mouse events
                         * @property clientY
                         * @type {Number}
                         * @readonly
                         */

        }, {
          key: 'clientY',
          get: function get () {
            return this.data.clientY
          }

          /**
                         * Normalized target for both touch and mouse events
                         * Returns the element that is behind cursor or touch pointer
                         * @property target
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'target',
          get: function get () {
            return this.data.target
          }

          /**
                         * Container that initiated the sensor
                         * @property container
                         * @type {HTMLElement}
                         * @readonly
                         */

        }, {
          key: 'container',
          get: function get () {
            return this.data.container
          }

          /**
                         * Trackpad pressure
                         * @property pressure
                         * @type {Number}
                         * @readonly
                         */

        }, {
          key: 'pressure',
          get: function get () {
            return this.data.pressure
          }
        }])
        return SensorEvent
      }(_AbstractEvent3.default))

      /**
                 * Drag start sensor event
                 * @class DragStartSensorEvent
                 * @module DragStartSensorEvent
                 * @extends SensorEvent
                 */

      const DragStartSensorEvent = exports.DragStartSensorEvent = (function (_SensorEvent) {
        (0, _inherits3.default)(DragStartSensorEvent, _SensorEvent)

        function DragStartSensorEvent () {
          (0, _classCallCheck3.default)(this, DragStartSensorEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragStartSensorEvent.__proto__ || Object.getPrototypeOf(DragStartSensorEvent)).apply(this, arguments))
        }

        return DragStartSensorEvent
      }(SensorEvent))

      /**
                 * Drag move sensor event
                 * @class DragMoveSensorEvent
                 * @module DragMoveSensorEvent
                 * @extends SensorEvent
                 */

      DragStartSensorEvent.type = 'drag:start'

      const DragMoveSensorEvent = exports.DragMoveSensorEvent = (function (_SensorEvent2) {
        (0, _inherits3.default)(DragMoveSensorEvent, _SensorEvent2)

        function DragMoveSensorEvent () {
          (0, _classCallCheck3.default)(this, DragMoveSensorEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragMoveSensorEvent.__proto__ || Object.getPrototypeOf(DragMoveSensorEvent)).apply(this, arguments))
        }

        return DragMoveSensorEvent
      }(SensorEvent))

      /**
                 * Drag stop sensor event
                 * @class DragStopSensorEvent
                 * @module DragStopSensorEvent
                 * @extends SensorEvent
                 */

      DragMoveSensorEvent.type = 'drag:move'

      const DragStopSensorEvent = exports.DragStopSensorEvent = (function (_SensorEvent3) {
        (0, _inherits3.default)(DragStopSensorEvent, _SensorEvent3)

        function DragStopSensorEvent () {
          (0, _classCallCheck3.default)(this, DragStopSensorEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragStopSensorEvent.__proto__ || Object.getPrototypeOf(DragStopSensorEvent)).apply(this, arguments))
        }

        return DragStopSensorEvent
      }(SensorEvent))

      /**
                 * Drag pressure sensor event
                 * @class DragPressureSensorEvent
                 * @module DragPressureSensorEvent
                 * @extends SensorEvent
                 */

      DragStopSensorEvent.type = 'drag:stop'

      const DragPressureSensorEvent = exports.DragPressureSensorEvent = (function (_SensorEvent4) {
        (0, _inherits3.default)(DragPressureSensorEvent, _SensorEvent4)

        function DragPressureSensorEvent () {
          (0, _classCallCheck3.default)(this, DragPressureSensorEvent)
          return (0, _possibleConstructorReturn3.default)(this, (DragPressureSensorEvent.__proto__ || Object.getPrototypeOf(DragPressureSensorEvent)).apply(this, arguments))
        }

        return DragPressureSensorEvent
      }(SensorEvent))

      DragPressureSensorEvent.type = 'drag:pressure'
      /***/
    },
    /* 161 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.default = requestNextAnimationFrame

      function requestNextAnimationFrame (callback) {
        return requestAnimationFrame(function () {
          requestAnimationFrame(callback)
        })
      }
      /***/
    },
    /* 162 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _requestNextAnimationFrame = __webpack_require__(161)

      const _requestNextAnimationFrame2 = _interopRequireDefault(_requestNextAnimationFrame)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _requestNextAnimationFrame2.default
      /***/
    },
    /* 163 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _toConsumableArray2 = __webpack_require__(25)

      const _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2)

      exports.default = closest

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const matchFunction = Element.prototype.matches || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector

      /**
                 * Get the closest parent element of a given element that matches the given
                 * selector string or matching function
                 *
                 * @param {Element} element The child element to find a parent of
                 * @param {String|Function} selector The string or function to use to match
                 *     the parent element
                 * @return {Element|null}
                 */
      function closest (element, value) {
        if (!element) {
          return null
        }

        const selector = value
        const callback = value
        const nodeList = value
        const singleElement = value

        const isSelector = Boolean(typeof value === 'string')
        const isFunction = Boolean(typeof value === 'function')
        const isNodeList = Boolean(value instanceof NodeList || value instanceof Array)
        const isElement = Boolean(value instanceof HTMLElement)

        function conditionFn (currentElement) {
          if (!currentElement) {
            return currentElement
          } else if (isSelector) {
            return matchFunction.call(currentElement, selector)
          } else if (isNodeList) {
            return [].concat((0, _toConsumableArray3.default)(nodeList)).includes(currentElement)
          } else if (isElement) {
            return singleElement === currentElement
          } else if (isFunction) {
            return callback(currentElement)
          } else {
            return null
          }
        }

        let current = element

        do {
          current = current.correspondingUseElement || current.correspondingElement || current

          if (conditionFn(current)) {
            return current
          }

          current = current.parentNode
        } while (current && current !== document.body && current !== document)

        return null
      }
      /***/
    },
    /* 164 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _closest = __webpack_require__(163)

      const _closest2 = _interopRequireDefault(_closest)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _closest2.default
      /***/
    },
    /* 165 */
    /***/
    function (module, exports, __webpack_require__) {
      const $export = __webpack_require__(18)
      // 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
      $export($export.S, 'Object', { create: __webpack_require__(62) })
      /***/
    },
    /* 166 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(165)
      const $Object = __webpack_require__(6).Object
      module.exports = function create (P, D) {
        return $Object.create(P, D)
      }
      /***/
    },
    /* 167 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = { default: __webpack_require__(166), __esModule: true }
      /***/
    },
    /* 168 */
    /***/
    function (module, exports, __webpack_require__) {
      // Works with __proto__ only. Old v8 can't work with null proto objects.
      /* eslint-disable no-proto */
      const isObject = __webpack_require__(26)
      const anObject = __webpack_require__(27)
      const check = function (O, proto) {
        anObject(O)
        if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!")
      }
      module.exports = {
                        set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
            (function (test, buggy, set) {
              try {
                set = __webpack_require__(67)(Function.call, __webpack_require__(51).f(Object.prototype, '__proto__').set, 2)
                set(test, [])
                buggy = !(test instanceof Array)
              } catch (e) { buggy = true }
              return function setPrototypeOf (O, proto) {
                check(O, proto)
                if (buggy) O.__proto__ = proto
                else set(O, proto)
                return O
              }
            }({}, false)) : undefined),
        check: check
      }
      /***/
    },
    /* 169 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.3.19 Object.setPrototypeOf(O, proto)
      const $export = __webpack_require__(18)
      $export($export.S, 'Object', { setPrototypeOf: __webpack_require__(168).set })
      /***/
    },
    /* 170 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(169)
      module.exports = __webpack_require__(6).Object.setPrototypeOf
      /***/
    },
    /* 171 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = { default: __webpack_require__(170), __esModule: true }
      /***/
    },
    /* 172 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(53)('observable')
      /***/
    },
    /* 173 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(53)('asyncIterator')
      /***/
    },
    /* 174 */
    /***/
    function (module, exports) {

      /***/
    },
    /* 175 */
    /***/
    function (module, exports, __webpack_require__) {
      // fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
      const toIObject = __webpack_require__(15)
      const gOPN = __webpack_require__(86).f
      const toString = {}.toString

      const windowNames = typeof window === 'object' && window && Object.getOwnPropertyNames
        ? Object.getOwnPropertyNames(window) : []

      const getWindowNames = function (it) {
        try {
          return gOPN(it)
        } catch (e) {
          return windowNames.slice()
        }
      }

      module.exports.f = function getOwnPropertyNames (it) {
        return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it))
      }
      /***/
    },
    /* 176 */
    /***/
    function (module, exports, __webpack_require__) {
      // 7.2.2 IsArray(argument)
      const cof = __webpack_require__(60)
      module.exports = Array.isArray || function isArray (arg) {
        return cof(arg) == 'Array'
      }
      /***/
    },
    /* 177 */
    /***/
    function (module, exports, __webpack_require__) {
      // all enumerable object keys, includes symbols
      const getKeys = __webpack_require__(61)
      const gOPS = __webpack_require__(87)
      const pIE = __webpack_require__(52)
      module.exports = function (it) {
        const result = getKeys(it)
        const getSymbols = gOPS.f
        if (getSymbols) {
          const symbols = getSymbols(it)
          const isEnum = pIE.f
          let i = 0
          let key
          while (symbols.length > i) { if (isEnum.call(it, key = symbols[i++])) result.push(key) }
        }
        return result
      }
      /***/
    },
    /* 178 */
    /***/
    function (module, exports, __webpack_require__) {
      const META = __webpack_require__(44)('meta')
      const isObject = __webpack_require__(26)
      const has = __webpack_require__(16)
      const setDesc = __webpack_require__(12).f
      let id = 0
      const isExtensible = Object.isExtensible || function () {
        return true
      }
      const FREEZE = !__webpack_require__(36)(function () {
        return isExtensible(Object.preventExtensions({}))
      })
      const setMeta = function (it) {
        setDesc(it, META, {
          value: {
            i: 'O' + ++id, // object ID
            w: {} // weak collections IDs
          }
        })
      }
      const fastKey = function (it, create) {
        // return primitive with prefix
        if (!isObject(it)) return typeof it === 'symbol' ? it : (typeof it === 'string' ? 'S' : 'P') + it
        if (!has(it, META)) {
          // can't set metadata to uncaught frozen object
          if (!isExtensible(it)) return 'F'
          // not necessary to add metadata
          if (!create) return 'E'
          // add missing metadata
          setMeta(it)
          // return object ID
        }
        return it[META].i
      }
      const getWeak = function (it, create) {
        if (!has(it, META)) {
          // can't set metadata to uncaught frozen object
          if (!isExtensible(it)) return true
          // not necessary to add metadata
          if (!create) return false
          // add missing metadata
          setMeta(it)
          // return hash weak collections IDs
        }
        return it[META].w
      }
      // add metadata on freeze-family methods calling
      const onFreeze = function (it) {
        if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it)
        return it
      }
      var meta = module.exports = {
        KEY: META,
        NEED: false,
        fastKey: fastKey,
        getWeak: getWeak,
        onFreeze: onFreeze
      }
      /***/
    },
    /* 179 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      // ECMAScript 6 symbols shim
      const global = __webpack_require__(13)
      const has = __webpack_require__(16)
      const DESCRIPTORS = __webpack_require__(17)
      const $export = __webpack_require__(18)
      const redefine = __webpack_require__(92)
      const META = __webpack_require__(178).KEY
      const $fails = __webpack_require__(36)
      const shared = __webpack_require__(58)
      const setToStringTag = __webpack_require__(56)
      const uid = __webpack_require__(44)
      const wks = __webpack_require__(8)
      const wksExt = __webpack_require__(54)
      const wksDefine = __webpack_require__(53)
      const enumKeys = __webpack_require__(177)
      const isArray = __webpack_require__(176)
      const anObject = __webpack_require__(27)
      const isObject = __webpack_require__(26)
      const toIObject = __webpack_require__(15)
      const toPrimitive = __webpack_require__(66)
      const createDesc = __webpack_require__(35)
      const _create = __webpack_require__(62)
      const gOPNExt = __webpack_require__(175)
      const $GOPD = __webpack_require__(51)
      const $DP = __webpack_require__(12)
      const $keys = __webpack_require__(61)
      const gOPD = $GOPD.f
      const dP = $DP.f
      const gOPN = gOPNExt.f
      let $Symbol = global.Symbol
      const $JSON = global.JSON
      const _stringify = $JSON && $JSON.stringify
      const PROTOTYPE = 'prototype'
      const HIDDEN = wks('_hidden')
      const TO_PRIMITIVE = wks('toPrimitive')
      const isEnum = {}.propertyIsEnumerable
      const SymbolRegistry = shared('symbol-registry')
      const AllSymbols = shared('symbols')
      const OPSymbols = shared('op-symbols')
      const ObjectProto = Object[PROTOTYPE]
      const USE_NATIVE = typeof $Symbol === 'function'
      const QObject = global.QObject
      // Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
      let setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild

      // fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
      const setSymbolDesc = DESCRIPTORS && $fails(function () {
        return _create(dP({}, 'a', {
          get: function () { return dP(this, 'a', { value: 7 }).a }
        })).a != 7
      }) ? function (it, key, D) {
            const protoDesc = gOPD(ObjectProto, key)
            if (protoDesc) delete ObjectProto[key]
            dP(it, key, D)
            if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc)
          } : dP

      const wrap = function (tag) {
        const sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE])
        sym._k = tag
        return sym
      }

      const isSymbol = USE_NATIVE && typeof $Symbol.iterator === 'symbol' ? function (it) {
        return typeof it === 'symbol'
      } : function (it) {
        return it instanceof $Symbol
      }

      var $defineProperty = function defineProperty (it, key, D) {
        if (it === ObjectProto) $defineProperty(OPSymbols, key, D)
        anObject(it)
        key = toPrimitive(key, true)
        anObject(D)
        if (has(AllSymbols, key)) {
          if (!D.enumerable) {
            if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}))
            it[HIDDEN][key] = true
          } else {
            if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false
            D = _create(D, { enumerable: createDesc(0, false) })
          }
          return setSymbolDesc(it, key, D)
        }
        return dP(it, key, D)
      }
      const $defineProperties = function defineProperties (it, P) {
        anObject(it)
        const keys = enumKeys(P = toIObject(P))
        let i = 0
        const l = keys.length
        let key
        while (l > i) $defineProperty(it, key = keys[i++], P[key])
        return it
      }
      const $create = function create (it, P) {
        return P === undefined ? _create(it) : $defineProperties(_create(it), P)
      }
      const $propertyIsEnumerable = function propertyIsEnumerable (key) {
        const E = isEnum.call(this, key = toPrimitive(key, true))
        if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false
        return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true
      }
      const $getOwnPropertyDescriptor = function getOwnPropertyDescriptor (it, key) {
        it = toIObject(it)
        key = toPrimitive(key, true)
        if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return
        const D = gOPD(it, key)
        if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true
        return D
      }
      const $getOwnPropertyNames = function getOwnPropertyNames (it) {
        const names = gOPN(toIObject(it))
        const result = []
        let i = 0
        let key
        while (names.length > i) {
          if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key)
        }
        return result
      }
      const $getOwnPropertySymbols = function getOwnPropertySymbols (it) {
        const IS_OP = it === ObjectProto
        const names = gOPN(IS_OP ? OPSymbols : toIObject(it))
        const result = []
        let i = 0
        let key
        while (names.length > i) {
          if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key])
        }
        return result
      }

      // 19.4.1.1 Symbol([description])
      if (!USE_NATIVE) {
        $Symbol = function Symbol () {
          if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!')
          const tag = uid(arguments.length > 0 ? arguments[0] : undefined)
          var $set = function (value) {
            if (this === ObjectProto) $set.call(OPSymbols, value)
            if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false
            setSymbolDesc(this, tag, createDesc(1, value))
          }
          if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set })
          return wrap(tag)
        }
        redefine($Symbol[PROTOTYPE], 'toString', function toString () {
          return this._k
        })

        $GOPD.f = $getOwnPropertyDescriptor
        $DP.f = $defineProperty
        __webpack_require__(86).f = gOPNExt.f = $getOwnPropertyNames
        __webpack_require__(52).f = $propertyIsEnumerable
        __webpack_require__(87).f = $getOwnPropertySymbols

        if (DESCRIPTORS && !__webpack_require__(45)) {
          redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true)
        }

        wksExt.f = function (name) {
          return wrap(wks(name))
        }
      }

      $export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol })

      for (let es6Symbols = (
        // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
          'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
        ).split(','), j = 0; es6Symbols.length > j;) wks(es6Symbols[j++])

      for (let wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++])

      $export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
        // 19.4.2.1 Symbol.for(key)
        for: function (key) {
          return has(SymbolRegistry, key += '')
            ? SymbolRegistry[key]
            : SymbolRegistry[key] = $Symbol(key)
        },
        // 19.4.2.5 Symbol.keyFor(sym)
        keyFor: function keyFor (sym) {
          if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!')
          for (const key in SymbolRegistry) { if (SymbolRegistry[key] === sym) return key }
        },
        useSetter: function () { setter = true },
        useSimple: function () { setter = false }
      })

      $export($export.S + $export.F * !USE_NATIVE, 'Object', {
        // 19.1.2.2 Object.create(O [, Properties])
        create: $create,
        // 19.1.2.4 Object.defineProperty(O, P, Attributes)
        defineProperty: $defineProperty,
        // 19.1.2.3 Object.defineProperties(O, Properties)
        defineProperties: $defineProperties,
        // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
        getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
        // 19.1.2.7 Object.getOwnPropertyNames(O)
        getOwnPropertyNames: $getOwnPropertyNames,
        // 19.1.2.8 Object.getOwnPropertySymbols(O)
        getOwnPropertySymbols: $getOwnPropertySymbols
      })

      // 24.3.2 JSON.stringify(value [, replacer [, space]])
      $JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
        const S = $Symbol()
        // MS Edge converts symbol values to JSON as {}
        // WebKit converts symbol values to JSON as null
        // V8 throws on boxed symbols
        return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}'
      })), 'JSON', {
        stringify: function stringify (it) {
          const args = [it]
          let i = 1
          let replacer, $replacer
          while (arguments.length > i) args.push(arguments[i++])
          $replacer = replacer = args[1]
          if (!isObject(replacer) && it === undefined || isSymbol(it)) return // IE8 returns string on undefined
          if (!isArray(replacer)) {
            replacer = function (key, value) {
              if (typeof $replacer === 'function') value = $replacer.call(this, key, value)
              if (!isSymbol(value)) return value
            }
          }
          args[1] = replacer
          return _stringify.apply($JSON, args)
        }
      })

      // 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
      $Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(28)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf)
      // 19.4.3.5 Symbol.prototype[@@toStringTag]
      setToStringTag($Symbol, 'Symbol')
      // 20.2.1.9 Math[@@toStringTag]
      setToStringTag(Math, 'Math', true)
      // 24.3.3 JSON[@@toStringTag]
      setToStringTag(global.JSON, 'JSON', true)
      /***/
    },
    /* 180 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(179)
      __webpack_require__(174)
      __webpack_require__(173)
      __webpack_require__(172)
      module.exports = __webpack_require__(6).Symbol
      /***/
    },
    /* 181 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = { default: __webpack_require__(180), __esModule: true }
      /***/
    },
    /* 182 */
    /***/
    function (module, exports) {
      module.exports = function (done, value) {
        return { value: value, done: !!done }
      }
      /***/
    },
    /* 183 */
    /***/
    function (module, exports) {
      module.exports = function () { /* empty */ }
      /***/
    },
    /* 184 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const addToUnscopables = __webpack_require__(183)
      const step = __webpack_require__(182)
      const Iterators = __webpack_require__(33)
      const toIObject = __webpack_require__(15)

      // 22.1.3.4 Array.prototype.entries()
      // 22.1.3.13 Array.prototype.keys()
      // 22.1.3.29 Array.prototype.values()
      // 22.1.3.30 Array.prototype[@@iterator]()
      module.exports = __webpack_require__(93)(Array, 'Array', function (iterated, kind) {
        this._t = toIObject(iterated) // target
        this._i = 0 // next index
        this._k = kind // kind
        // 22.1.5.2.1 %ArrayIteratorPrototype%.next()
      }, function () {
        const O = this._t
        const kind = this._k
        const index = this._i++
        if (!O || index >= O.length) {
          this._t = undefined
          return step(1)
        }
        if (kind == 'keys') return step(0, index)
        if (kind == 'values') return step(0, O[index])
        return step(0, [index, O[index]])
      }, 'values')

      // argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
      Iterators.Arguments = Iterators.Array

      addToUnscopables('keys')
      addToUnscopables('values')
      addToUnscopables('entries')
      /***/
    },
    /* 185 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(184)
      const global = __webpack_require__(13)
      const hide = __webpack_require__(28)
      const Iterators = __webpack_require__(33)
      const TO_STRING_TAG = __webpack_require__(8)('toStringTag')

      const DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
                    'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
                    'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
                    'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
                    'TextTrackList,TouchList').split(',')

      for (let i = 0; i < DOMIterables.length; i++) {
        const NAME = DOMIterables[i]
        const Collection = global[NAME]
        const proto = Collection && Collection.prototype
        if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME)
        Iterators[NAME] = Iterators.Array
      }
      /***/
    },
    /* 186 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(94)
      __webpack_require__(185)
      module.exports = __webpack_require__(54).f('iterator')
      /***/
    },
    /* 187 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = { default: __webpack_require__(186), __esModule: true }
      /***/
    },
    /* 188 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      const _possibleConstructorReturn2 = __webpack_require__(3)

      const _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2)

      const _inherits2 = __webpack_require__(2)

      const _inherits3 = _interopRequireDefault(_inherits2)

      const _utils = __webpack_require__(11)

      const _Sensor2 = __webpack_require__(34)

      const _Sensor3 = _interopRequireDefault(_Sensor2)

      const _SensorEvent = __webpack_require__(32)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const onContextMenuWhileDragging = Symbol('onContextMenuWhileDragging')
      const onMouseDown = Symbol('onMouseDown')
      const onMouseMove = Symbol('onMouseMove')
      const onMouseUp = Symbol('onMouseUp')

      /**
                 * This sensor picks up native browser mouse events and dictates drag operations
                 * @class MouseSensor
                 * @module MouseSensor
                 * @extends Sensor
                 */

      const MouseSensor = (function (_Sensor) {
        (0, _inherits3.default)(MouseSensor, _Sensor)

        /**
                     * MouseSensor constructor.
                     * @constructs MouseSensor
                     * @param {HTMLElement[]|NodeList|HTMLElement} containers - Containers
                     * @param {Object} options - Options
                     */
        function MouseSensor () {
          const containers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : []
          const options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          (0, _classCallCheck3.default)(this, MouseSensor)

          /**
                         * Indicates if mouse button is still down
                         * @property mouseDown
                         * @type {Boolean}
                         */
          const _this = (0, _possibleConstructorReturn3.default)(this, (MouseSensor.__proto__ || Object.getPrototypeOf(MouseSensor)).call(this, containers, options))

          _this.mouseDown = false

          /**
                         * Mouse down timer which will end up triggering the drag start operation
                         * @property mouseDownTimeout
                         * @type {Number}
                         */
          _this.mouseDownTimeout = null

          /**
                         * Indicates if context menu has been opened during drag operation
                         * @property openedContextMenu
                         * @type {Boolean}
                         */
          _this.openedContextMenu = false

          _this[onContextMenuWhileDragging] = _this[onContextMenuWhileDragging].bind(_this)
          _this[onMouseDown] = _this[onMouseDown].bind(_this)
          _this[onMouseMove] = _this[onMouseMove].bind(_this)
          _this[onMouseUp] = _this[onMouseUp].bind(_this)
          return _this
        }

        /**
                     * Attaches sensors event listeners to the DOM
                     */

        (0, _createClass3.default)(MouseSensor, [{
          key: 'attach',
          value: function attach () {
            document.addEventListener('mousedown', this[onMouseDown], true)
          }

          /**
                         * Detaches sensors event listeners to the DOM
                         */

        }, {
          key: 'detach',
          value: function detach () {
            document.removeEventListener('mousedown', this[onMouseDown], true)
          }

          /**
                         * Mouse down handler
                         * @private
                         * @param {Event} event - Mouse down event
                         */

        }, {
          key: onMouseDown,
          value: function value (event) {
            const _this2 = this

            if (event.button !== 0 || event.ctrlKey || event.metaKey) {
              return
            }

            document.addEventListener('mouseup', this[onMouseUp])

            const target = document.elementFromPoint(event.clientX, event.clientY)
            const container = (0, _utils.closest)(target, this.containers)

            if (!container) {
              return
            }

            document.addEventListener('dragstart', preventNativeDragStart)

            this.mouseDown = true

            clearTimeout(this.mouseDownTimeout)
            this.mouseDownTimeout = setTimeout(function () {
              if (!_this2.mouseDown) {
                return
              }

              const dragStartEvent = new _SensorEvent.DragStartSensorEvent({
                clientX: event.clientX,
                clientY: event.clientY,
                target: target,
                container: container,
                originalEvent: event
              })

              _this2.trigger(container, dragStartEvent)

              _this2.currentContainer = container
              _this2.dragging = !dragStartEvent.canceled()

              if (_this2.dragging) {
                document.addEventListener('contextmenu', _this2[onContextMenuWhileDragging])
                document.addEventListener('mousemove', _this2[onMouseMove])
              }
            }, this.options.delay)
          }

          /**
                         * Mouse move handler
                         * @private
                         * @param {Event} event - Mouse move event
                         */

        }, {
          key: onMouseMove,
          value: function value (event) {
            if (!this.dragging) {
              return
            }

            const target = document.elementFromPoint(event.clientX, event.clientY)

            const dragMoveEvent = new _SensorEvent.DragMoveSensorEvent({
              clientX: event.clientX,
              clientY: event.clientY,
              target: target,
              container: this.currentContainer,
              originalEvent: event
            })

            this.trigger(this.currentContainer, dragMoveEvent)
          }

          /**
                         * Mouse up handler
                         * @private
                         * @param {Event} event - Mouse up event
                         */

        }, {
          key: onMouseUp,
          value: function value (event) {
            this.mouseDown = Boolean(this.openedContextMenu)

            if (this.openedContextMenu) {
              this.openedContextMenu = false
              return
            }

            document.removeEventListener('mouseup', this[onMouseUp])
            document.removeEventListener('dragstart', preventNativeDragStart)

            if (!this.dragging) {
              return
            }

            const target = document.elementFromPoint(event.clientX, event.clientY)

            const dragStopEvent = new _SensorEvent.DragStopSensorEvent({
              clientX: event.clientX,
              clientY: event.clientY,
              target: target,
              container: this.currentContainer,
              originalEvent: event
            })

            this.trigger(this.currentContainer, dragStopEvent)

            document.removeEventListener('contextmenu', this[onContextMenuWhileDragging])
            document.removeEventListener('mousemove', this[onMouseMove])

            this.currentContainer = null
            this.dragging = false
          }

          /**
                         * Context menu handler
                         * @private
                         * @param {Event} event - Context menu event
                         */

        }, {
          key: onContextMenuWhileDragging,
          value: function value (event) {
            event.preventDefault()
            this.openedContextMenu = true
          }
        }])
        return MouseSensor
      }(_Sensor3.default))

      exports.default = MouseSensor

      function preventNativeDragStart (event) {
        event.preventDefault()
      }
      /***/
    },
    /* 189 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _MouseSensor = __webpack_require__(188)

      const _MouseSensor2 = _interopRequireDefault(_MouseSensor)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.default = _MouseSensor2.default
      /***/
    },
    /* 190 */
    /***/
    function (module, exports, __webpack_require__) {
      const ITERATOR = __webpack_require__(8)('iterator')
      let SAFE_CLOSING = false

      try {
        const riter = [7][ITERATOR]()
        riter.return = function () { SAFE_CLOSING = true }
        // eslint-disable-next-line no-throw-literal
        Array.from(riter, function () { throw 2 })
      } catch (e) { /* empty */ }

      module.exports = function (exec, skipClosing) {
        if (!skipClosing && !SAFE_CLOSING) return false
        let safe = false
        try {
          const arr = [7]
          const iter = arr[ITERATOR]()
          iter.next = function () { return { done: safe = true } }
          arr[ITERATOR] = function () { return iter }
          exec(arr)
        } catch (e) { /* empty */ }
        return safe
      }
      /***/
    },
    /* 191 */
    /***/
    function (module, exports, __webpack_require__) {
      // getting tag from 19.1.3.6 Object.prototype.toString()
      const cof = __webpack_require__(60)
      const TAG = __webpack_require__(8)('toStringTag')
      // ES3 wrong here
      const ARG = cof(function () { return arguments }()) == 'Arguments'

      // fallback for IE11 Script Access Denied error
      const tryGet = function (it, key) {
        try {
          return it[key]
        } catch (e) { /* empty */ }
      }

      module.exports = function (it) {
        let O, T, B
        return it === undefined ? 'Undefined' : it === null ? 'Null'
        // @@toStringTag case
          : typeof (T = tryGet(O = Object(it), TAG)) === 'string' ? T
          // builtinTag case
            : ARG ? cof(O)
            // ES3 arguments fallback
              : (B = cof(O)) == 'Object' && typeof O.callee === 'function' ? 'Arguments' : B
      }
      /***/
    },
    /* 192 */
    /***/
    function (module, exports, __webpack_require__) {
      const classof = __webpack_require__(191)
      const ITERATOR = __webpack_require__(8)('iterator')
      const Iterators = __webpack_require__(33)
      module.exports = __webpack_require__(6).getIteratorMethod = function (it) {
        if (it != undefined) {
          return it[ITERATOR] ||
                                it['@@iterator'] ||
                                Iterators[classof(it)]
        }
      }
      /***/
    },
    /* 193 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const $defineProperty = __webpack_require__(12)
      const createDesc = __webpack_require__(35)

      module.exports = function (object, index, value) {
        if (index in object) $defineProperty.f(object, index, createDesc(0, value))
        else object[index] = value
      }
      /***/
    },
    /* 194 */
    /***/
    function (module, exports, __webpack_require__) {
      // check on default Array iterator
      const Iterators = __webpack_require__(33)
      const ITERATOR = __webpack_require__(8)('iterator')
      const ArrayProto = Array.prototype

      module.exports = function (it) {
        return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it)
      }
      /***/
    },
    /* 195 */
    /***/
    function (module, exports, __webpack_require__) {
      // call something on iterator step with safe closing on error
      const anObject = __webpack_require__(27)
      module.exports = function (iterator, fn, value, entries) {
        try {
          return entries ? fn(anObject(value)[0], value[1]) : fn(value)
          // 7.4.6 IteratorClose(iterator, completion)
        } catch (e) {
          const ret = iterator.return
          if (ret !== undefined) anObject(ret.call(iterator))
          throw e
        }
      }
      /***/
    },
    /* 196 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const ctx = __webpack_require__(67)
      const $export = __webpack_require__(18)
      const toObject = __webpack_require__(55)
      const call = __webpack_require__(195)
      const isArrayIter = __webpack_require__(194)
      const toLength = __webpack_require__(90)
      const createProperty = __webpack_require__(193)
      const getIterFn = __webpack_require__(192)

      $export($export.S + $export.F * !__webpack_require__(190)(function (iter) { Array.from(iter) }), 'Array', {
        // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
        from: function from (arrayLike /* , mapfn = undefined, thisArg = undefined */) {
          const O = toObject(arrayLike)
          const C = typeof this === 'function' ? this : Array
          const aLen = arguments.length
          let mapfn = aLen > 1 ? arguments[1] : undefined
          const mapping = mapfn !== undefined
          let index = 0
          const iterFn = getIterFn(O)
          let length, result, step, iterator
          if (mapping) mapfn = ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2)
          // if object isn't iterable or it's array with default iterator - use simple case
          if (iterFn != undefined && !(C == Array && isArrayIter(iterFn))) {
            for (iterator = iterFn.call(O), result = new C(); !(step = iterator.next()).done; index++) {
              createProperty(result, index, mapping ? call(iterator, mapfn, [step.value, index], true) : step.value)
            }
          } else {
            length = toLength(O.length)
            for (result = new C(length); length > index; index++) {
              createProperty(result, index, mapping ? mapfn(O[index], index) : O[index])
            }
          }
          result.length = index
          return result
        }
      })
      /***/
    },
    /* 197 */
    /***/
    function (module, exports, __webpack_require__) {
      const document = __webpack_require__(13).document
      module.exports = document && document.documentElement
      /***/
    },
    /* 198 */
    /***/
    function (module, exports, __webpack_require__) {
      const toInteger = __webpack_require__(64)
      const max = Math.max
      const min = Math.min
      module.exports = function (index, length) {
        index = toInteger(index)
        return index < 0 ? max(index + length, 0) : min(index, length)
      }
      /***/
    },
    /* 199 */
    /***/
    function (module, exports, __webpack_require__) {
      // false -> Array#indexOf
      // true  -> Array#includes
      const toIObject = __webpack_require__(15)
      const toLength = __webpack_require__(90)
      const toAbsoluteIndex = __webpack_require__(198)
      module.exports = function (IS_INCLUDES) {
        return function ($this, el, fromIndex) {
          const O = toIObject($this)
          const length = toLength(O.length)
          let index = toAbsoluteIndex(fromIndex, length)
          let value
          // Array#includes uses SameValueZero equality algorithm
          // eslint-disable-next-line no-self-compare
          if (IS_INCLUDES && el != el) {
            while (length > index) {
              value = O[index++]
              // eslint-disable-next-line no-self-compare
              if (value != value) return true
              // Array#indexOf ignores holes, Array#includes - not
            }
          } else {
            for (; length > index; index++) {
              if (IS_INCLUDES || index in O) {
                if (O[index] === el) return IS_INCLUDES || index || 0
              }
            }
          }
          return !IS_INCLUDES && -1
        }
      }
      /***/
    },
    /* 200 */
    /***/
    function (module, exports, __webpack_require__) {
      // fallback for non-array-like ES3 and non-enumerable old V8 strings
      const cof = __webpack_require__(60)
      // eslint-disable-next-line no-prototype-builtins
      module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
        return cof(it) == 'String' ? it.split('') : Object(it)
      }
      /***/
    },
    /* 201 */
    /***/
    function (module, exports, __webpack_require__) {
      const dP = __webpack_require__(12)
      const anObject = __webpack_require__(27)
      const getKeys = __webpack_require__(61)

      module.exports = __webpack_require__(17) ? Object.defineProperties : function defineProperties (O, Properties) {
        anObject(O)
        const keys = getKeys(Properties)
        const length = keys.length
        let i = 0
        let P
        while (length > i) dP.f(O, P = keys[i++], Properties[P])
        return O
      }
      /***/
    },
    /* 202 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const create = __webpack_require__(62)
      const descriptor = __webpack_require__(35)
      const setToStringTag = __webpack_require__(56)
      const IteratorPrototype = {}

      // 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
      __webpack_require__(28)(IteratorPrototype, __webpack_require__(8)('iterator'), function () { return this })

      module.exports = function (Constructor, NAME, next) {
        Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) })
        setToStringTag(Constructor, NAME + ' Iterator')
      }
      /***/
    },
    /* 203 */
    /***/
    function (module, exports, __webpack_require__) {
      const toInteger = __webpack_require__(64)
      const defined = __webpack_require__(63)
      // true  -> String#at
      // false -> String#codePointAt
      module.exports = function (TO_STRING) {
        return function (that, pos) {
          const s = String(defined(that))
          const i = toInteger(pos)
          const l = s.length
          let a, b
          if (i < 0 || i >= l) return TO_STRING ? '' : undefined
          a = s.charCodeAt(i)
          return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
            ? TO_STRING ? s.charAt(i) : a
            : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000
        }
      }
      /***/
    },
    /* 204 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(94)
      __webpack_require__(196)
      module.exports = __webpack_require__(6).Array.from
      /***/
    },
    /* 205 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = { default: __webpack_require__(204), __esModule: true }
      /***/
    },
    /* 206 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _toConsumableArray2 = __webpack_require__(25)

      const _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2)

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * Base sensor class. Extend from this class to create a new or custom sensor
                 * @class Sensor
                 * @module Sensor
                 */
      const Sensor = (function () {
        /**
                     * Sensor constructor.
                     * @constructs Sensor
                     * @param {HTMLElement[]|NodeList|HTMLElement} containers - Containers
                     * @param {Object} options - Options
                     */
        function Sensor () {
          const containers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : []
          const options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          (0, _classCallCheck3.default)(this, Sensor)

          /**
                         * Current containers
                         * @property containers
                         * @type {HTMLElement[]}
                         */
          this.containers = [].concat((0, _toConsumableArray3.default)(containers))

          /**
                         * Current options
                         * @property options
                         * @type {Object}
                         */
          this.options = Object.assign({}, options)

          /**
                         * Current drag state
                         * @property dragging
                         * @type {Boolean}
                         */
          this.dragging = false

          /**
                         * Current container
                         * @property currentContainer
                         * @type {HTMLElement}
                         */
          this.currentContainer = null
        }

        /**
                     * Attaches sensors event listeners to the DOM
                     * @return {Sensor}
                     */

        (0, _createClass3.default)(Sensor, [{
          key: 'attach',
          value: function attach () {
            return this
          }

          /**
                         * Detaches sensors event listeners to the DOM
                         * @return {Sensor}
                         */

        }, {
          key: 'detach',
          value: function detach () {
            return this
          }

          /**
                         * Adds container to this sensor instance
                         * @param {...HTMLElement} containers - Containers you want to add to this sensor
                         * @example draggable.addContainer(document.body)
                         */

        }, {
          key: 'addContainer',
          value: function addContainer () {
            for (var _len = arguments.length, containers = Array(_len), _key = 0; _key < _len; _key++) {
              containers[_key] = arguments[_key]
            }

            this.containers = [].concat((0, _toConsumableArray3.default)(this.containers), containers)
          }

          /**
                         * Removes container from this sensor instance
                         * @param {...HTMLElement} containers - Containers you want to remove from this sensor
                         * @example draggable.removeContainer(document.body)
                         */

        }, {
          key: 'removeContainer',
          value: function removeContainer () {
            for (var _len2 = arguments.length, containers = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
              containers[_key2] = arguments[_key2]
            }

            this.containers = this.containers.filter(function (container) {
              return !containers.includes(container)
            })
          }

          /**
                         * Triggers event on target element
                         * @param {HTMLElement} element - Element to trigger event on
                         * @param {SensorEvent} sensorEvent - Sensor event to trigger
                         */

        }, {
          key: 'trigger',
          value: function trigger (element, sensorEvent) {
            const event = document.createEvent('Event')
            event.detail = sensorEvent
            event.initEvent(sensorEvent.type, true, true)
            element.dispatchEvent(event)
            this.lastEvent = sensorEvent

            return sensorEvent
          }
        }])
        return Sensor
      }())

      exports.default = Sensor
      /***/
    },
    /* 207 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      /**
                 * All draggable plugins inherit from this class.
                 * @abstract
                 * @class AbstractPlugin
                 * @module AbstractPlugin
                 */
      const AbstractPlugin = (function () {
        /**
                     * AbstractPlugin constructor.
                     * @constructs AbstractPlugin
                     * @param {Draggable} draggable - Draggable instance
                     */
        function AbstractPlugin (draggable) {
          (0, _classCallCheck3.default)(this, AbstractPlugin)

          /**
                         * Draggable instance
                         * @property draggable
                         * @type {Draggable}
                         */
          this.draggable = draggable
        }

        /**
                     * Override to add listeners
                     * @abstract
                     */

        (0, _createClass3.default)(AbstractPlugin, [{
          key: 'attach',
          value: function attach () {
            throw new Error('Not Implemented')
          }

          /**
                         * Override to remove listeners
                         * @abstract
                         */

        }, {
          key: 'detach',
          value: function detach () {
            throw new Error('Not Implemented')
          }
        }])
        return AbstractPlugin
      }())

      exports.default = AbstractPlugin
      /***/
    },
    /* 208 */
    /***/
    function (module, exports) {
      module.exports = function (it) {
        if (typeof it !== 'function') throw TypeError(it + ' is not a function!')
        return it
      }
      /***/
    },
    /* 209 */
    /***/
    function (module, exports, __webpack_require__) {
      const $export = __webpack_require__(18)
      // 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
      $export($export.S + $export.F * !__webpack_require__(17), 'Object', { defineProperty: __webpack_require__(12).f })
      /***/
    },
    /* 210 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(209)
      const $Object = __webpack_require__(6).Object
      module.exports = function defineProperty (it, key, desc) {
        return $Object.defineProperty(it, key, desc)
      }
      /***/
    },
    /* 211 */
    /***/
    function (module, exports, __webpack_require__) {
      module.exports = { default: __webpack_require__(210), __esModule: true }
      /***/
    },
    /* 212 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })

      const _classCallCheck2 = __webpack_require__(1)

      const _classCallCheck3 = _interopRequireDefault(_classCallCheck2)

      const _createClass2 = __webpack_require__(0)

      const _createClass3 = _interopRequireDefault(_createClass2)

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      const _canceled = Symbol('canceled')

      /**
                 * All events fired by draggable inherit this class. You can call `cancel()` to
                 * cancel a specific event or you can check if an event has been canceled by
                 * calling `canceled()`.
                 * @abstract
                 * @class AbstractEvent
                 * @module AbstractEvent
                 */

      const AbstractEvent = (function () {
        /**
                     * AbstractEvent constructor.
                     * @constructs AbstractEvent
                     * @param {object} data - Event data
                     */

        /**
                     * Event type
                     * @static
                     * @abstract
                     * @property type
                     * @type {String}
                     */
        function AbstractEvent (data) {
          (0, _classCallCheck3.default)(this, AbstractEvent)

          this[_canceled] = false
          this.data = data
        }

        /**
                     * Read-only type
                     * @abstract
                     * @return {String}
                     */

        /**
                     * Event cancelable
                     * @static
                     * @abstract
                     * @property cancelable
                     * @type {Boolean}
                     */

        (0, _createClass3.default)(AbstractEvent, [{
          key: 'cancel',

          /**
                         * Cancels the event instance
                         * @abstract
                         */
          value: function cancel () {
            this[_canceled] = true
          }

          /**
                         * Check if event has been canceled
                         * @abstract
                         * @return {Boolean}
                         */

        }, {
          key: 'canceled',
          value: function canceled () {
            return Boolean(this[_canceled])
          }

          /**
                         * Returns new event instance with existing event data.
                         * This method allows for overriding of event data.
                         * @param {Object} data
                         * @return {AbstractEvent}
                         */

        }, {
          key: 'clone',
          value: function clone (data) {
            return new this.constructor(Object.assign({}, this.data, data))
          }
        }, {
          key: 'type',
          get: function get () {
            return this.constructor.type
          }

          /**
                         * Read-only cancelable
                         * @abstract
                         * @return {Boolean}
                         */

        }, {
          key: 'cancelable',
          get: function get () {
            return this.constructor.cancelable
          }
        }])
        return AbstractEvent
      }())

      AbstractEvent.type = 'event'
      AbstractEvent.cancelable = false
      exports.default = AbstractEvent
      /***/
    },
    /* 213 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      // https://github.com/tc39/Array.prototype.includes
      const $export = __webpack_require__(31)
      const $includes = __webpack_require__(107)(true)

      $export($export.P, 'Array', {
        includes: function includes (el /* , fromIndex = 0 */) {
          return $includes(this, el, arguments.length > 1 ? arguments[1] : undefined)
        }
      })

      __webpack_require__(99)('includes')
      /***/
    },
    /* 214 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(213)
      module.exports = __webpack_require__(7).Array.includes
      /***/
    },
    /* 215 */
    /***/
    function (module, exports, __webpack_require__) {
      const getKeys = __webpack_require__(29)
      const toIObject = __webpack_require__(19)
      const isEnum = __webpack_require__(38).f
      module.exports = function (isEntries) {
        return function (it) {
          const O = toIObject(it)
          const keys = getKeys(O)
          const length = keys.length
          let i = 0
          const result = []
          let key
          while (length > i) {
            if (isEnum.call(O, key = keys[i++])) {
              result.push(isEntries ? [key, O[key]] : O[key])
            }
          }
          return result
        }
      }
      /***/
    },
    /* 216 */
    /***/
    function (module, exports, __webpack_require__) {
      // https://github.com/tc39/proposal-object-values-entries
      const $export = __webpack_require__(31)
      const $values = __webpack_require__(215)(false)

      $export($export.S, 'Object', {
        values: function values (it) {
          return $values(it)
        }
      })
      /***/
    },
    /* 217 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(216)
      module.exports = __webpack_require__(7).Object.values
      /***/
    },
    /* 218 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      // 19.1.2.1 Object.assign(target, source, ...)
      const getKeys = __webpack_require__(29)
      const gOPS = __webpack_require__(69)
      const pIE = __webpack_require__(38)
      const toObject = __webpack_require__(100)
      const IObject = __webpack_require__(108)
      const $assign = Object.assign

      // should work with symbols and should have deterministic property order (V8 bug)
      module.exports = !$assign || __webpack_require__(42)(function () {
        const A = {}
        const B = {}
        // eslint-disable-next-line no-undef
        const S = Symbol()
        const K = 'abcdefghijklmnopqrst'
        A[S] = 7
        K.split('').forEach(function (k) { B[k] = k })
        return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K
      }) ? function assign (target, source) { // eslint-disable-line no-unused-vars
            const T = toObject(target)
            const aLen = arguments.length
            let index = 1
            const getSymbols = gOPS.f
            const isEnum = pIE.f
            while (aLen > index) {
              const S = IObject(arguments[index++])
              const keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S)
              const length = keys.length
              let j = 0
              var key
              while (length > j) { if (isEnum.call(S, key = keys[j++])) T[key] = S[key] }
            }
            return T
          } : $assign
      /***/
    },
    /* 219 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.3.1 Object.assign(target, source)
      const $export = __webpack_require__(31)

      $export($export.S + $export.F, 'Object', { assign: __webpack_require__(218) })
      /***/
    },
    /* 220 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(219)
      module.exports = __webpack_require__(7).Object.assign
      /***/
    },
    /* 221 */
    /***/
    function (module, exports, __webpack_require__) {
      const ITERATOR = __webpack_require__(4)('iterator')
      let SAFE_CLOSING = false

      try {
        const riter = [7][ITERATOR]()
        riter.return = function () { SAFE_CLOSING = true }
        // eslint-disable-next-line no-throw-literal
        Array.from(riter, function () { throw 2 })
      } catch (e) { /* empty */ }

      module.exports = function (exec, skipClosing) {
        if (!skipClosing && !SAFE_CLOSING) return false
        let safe = false
        try {
          const arr = [7]
          const iter = arr[ITERATOR]()
          iter.next = function () { return { done: safe = true } }
          arr[ITERATOR] = function () { return iter }
          exec(arr)
        } catch (e) { /* empty */ }
        return safe
      }
      /***/
    },
    /* 222 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const global = __webpack_require__(5)
      const dP = __webpack_require__(21)
      const DESCRIPTORS = __webpack_require__(23)
      const SPECIES = __webpack_require__(4)('species')

      module.exports = function (KEY) {
        const C = global[KEY]
        if (DESCRIPTORS && C && !C[SPECIES]) {
          dP.f(C, SPECIES, {
            configurable: true,
            get: function () { return this }
          })
        }
      }
      /***/
    },
    /* 223 */
    /***/
    function (module, exports, __webpack_require__) {
      const redefine = __webpack_require__(30)
      module.exports = function (target, src, safe) {
        for (const key in src) redefine(target, key, src[key], safe)
        return target
      }
      /***/
    },
    /* 224 */
    /***/
    function (module, exports, __webpack_require__) {
      const anObject = __webpack_require__(14)
      const isObject = __webpack_require__(20)
      const newPromiseCapability = __webpack_require__(97)

      module.exports = function (C, x) {
        anObject(C)
        if (isObject(x) && x.constructor === C) return x
        const promiseCapability = newPromiseCapability.f(C)
        const resolve = promiseCapability.resolve
        resolve(x)
        return promiseCapability.promise
      }
      /***/
    },
    /* 225 */
    /***/
    function (module, exports, __webpack_require__) {
      const global = __webpack_require__(5)
      const navigator = global.navigator

      module.exports = navigator && navigator.userAgent || ''
      /***/
    },
    /* 226 */
    /***/
    function (module, exports) {
      module.exports = function (exec) {
        try {
          return { e: false, v: exec() }
        } catch (e) {
          return { e: true, v: e }
        }
      }
      /***/
    },
    /* 227 */
    /***/
    function (module, exports, __webpack_require__) {
      const global = __webpack_require__(5)
      const macrotask = __webpack_require__(98).set
      const Observer = global.MutationObserver || global.WebKitMutationObserver
      const process = global.process
      const Promise = global.Promise
      const isNode = __webpack_require__(39)(process) == 'process'

      module.exports = function () {
        let head, last, notify

        const flush = function () {
          let parent, fn
          if (isNode && (parent = process.domain)) parent.exit()
          while (head) {
            fn = head.fn
            head = head.next
            try {
              fn()
            } catch (e) {
              if (head) notify()
              else last = undefined
              throw e
            }
          }
          last = undefined
          if (parent) parent.enter()
        }

        // Node.js
        if (isNode) {
          notify = function () {
            process.nextTick(flush)
          }
          // browsers with MutationObserver, except iOS Safari - https://github.com/zloirock/core-js/issues/339
        } else if (Observer && !(global.navigator && global.navigator.standalone)) {
          let toggle = true
          const node = document.createTextNode('')
          new Observer(flush).observe(node, { characterData: true }) // eslint-disable-line no-new
          notify = function () {
            node.data = toggle = !toggle
          }
          // environments with maybe non-completely correct, but existent Promise
        } else if (Promise && Promise.resolve) {
          // Promise.resolve without an argument throws an error in LG WebOS 2
          const promise = Promise.resolve(undefined)
          notify = function () {
            promise.then(flush)
          }
          // for other environments - macrotask based on:
          // - setImmediate
          // - MessageChannel
          // - window.postMessag
          // - onreadystatechange
          // - setTimeout
        } else {
          notify = function () {
            // strange IE + webpack dev server bug - use .call(global)
            macrotask.call(global, flush)
          }
        }

        return function (fn) {
          const task = { fn: fn, next: undefined }
          if (last) last.next = task
          if (!head) {
            head = task
            notify()
          }
          last = task
        }
      }
      /***/
    },
    /* 228 */
    /***/
    function (module, exports) {
      // fast apply, http://jsperf.lnkit.com/fast-apply/5
      module.exports = function (fn, args, that) {
        const un = that === undefined
        switch (args.length) {
          case 0:
            return un ? fn()
              : fn.call(that)
          case 1:
            return un ? fn(args[0])
              : fn.call(that, args[0])
          case 2:
            return un ? fn(args[0], args[1])
              : fn.call(that, args[0], args[1])
          case 3:
            return un ? fn(args[0], args[1], args[2])
              : fn.call(that, args[0], args[1], args[2])
          case 4:
            return un ? fn(args[0], args[1], args[2], args[3])
              : fn.call(that, args[0], args[1], args[2], args[3])
        }
        return fn.apply(that, args)
      }
      /***/
    },
    /* 229 */
    /***/
    function (module, exports, __webpack_require__) {
      // 7.3.20 SpeciesConstructor(O, defaultConstructor)
      const anObject = __webpack_require__(14)
      const aFunction = __webpack_require__(47)
      const SPECIES = __webpack_require__(4)('species')
      module.exports = function (O, D) {
        const C = anObject(O).constructor
        let S
        return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S)
      }
      /***/
    },
    /* 230 */
    /***/
    function (module, exports, __webpack_require__) {
      const classof = __webpack_require__(68)
      const ITERATOR = __webpack_require__(4)('iterator')
      const Iterators = __webpack_require__(37)
      module.exports = __webpack_require__(7).getIteratorMethod = function (it) {
        if (it != undefined) {
          return it[ITERATOR] ||
                                it['@@iterator'] ||
                                Iterators[classof(it)]
        }
      }
      /***/
    },
    /* 231 */
    /***/
    function (module, exports, __webpack_require__) {
      // check on default Array iterator
      const Iterators = __webpack_require__(37)
      const ITERATOR = __webpack_require__(4)('iterator')
      const ArrayProto = Array.prototype

      module.exports = function (it) {
        return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it)
      }
      /***/
    },
    /* 232 */
    /***/
    function (module, exports, __webpack_require__) {
      // call something on iterator step with safe closing on error
      const anObject = __webpack_require__(14)
      module.exports = function (iterator, fn, value, entries) {
        try {
          return entries ? fn(anObject(value)[0], value[1]) : fn(value)
          // 7.4.6 IteratorClose(iterator, completion)
        } catch (e) {
          const ret = iterator.return
          if (ret !== undefined) anObject(ret.call(iterator))
          throw e
        }
      }
      /***/
    },
    /* 233 */
    /***/
    function (module, exports, __webpack_require__) {
      const ctx = __webpack_require__(48)
      const call = __webpack_require__(232)
      const isArrayIter = __webpack_require__(231)
      const anObject = __webpack_require__(14)
      const toLength = __webpack_require__(106)
      const getIterFn = __webpack_require__(230)
      const BREAK = {}
      const RETURN = {}
      var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
        const iterFn = ITERATOR ? function () { return iterable } : getIterFn(iterable)
        const f = ctx(fn, that, entries ? 2 : 1)
        let index = 0
        let length, step, iterator, result
        if (typeof iterFn !== 'function') throw TypeError(iterable + ' is not iterable!')
        // fast case for arrays with default iterator
        if (isArrayIter(iterFn)) {
          for (length = toLength(iterable.length); length > index; index++) {
            result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index])
            if (result === BREAK || result === RETURN) return result
          }
        } else {
          for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
            result = call(iterator, f, step.value, entries)
            if (result === BREAK || result === RETURN) return result
          }
        }
      }
      exports.BREAK = BREAK
      exports.RETURN = RETURN
      /***/
    },
    /* 234 */
    /***/
    function (module, exports) {
      module.exports = function (it, Constructor, name, forbiddenField) {
        if (!(it instanceof Constructor) || (forbiddenField !== undefined && forbiddenField in it)) {
          throw TypeError(name + ': incorrect invocation!')
        }
        return it
      }
      /***/
    },
    /* 235 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const LIBRARY = __webpack_require__(40)
      const global = __webpack_require__(5)
      const ctx = __webpack_require__(48)
      const classof = __webpack_require__(68)
      const $export = __webpack_require__(31)
      const isObject = __webpack_require__(20)
      const aFunction = __webpack_require__(47)
      const anInstance = __webpack_require__(234)
      const forOf = __webpack_require__(233)
      const speciesConstructor = __webpack_require__(229)
      const task = __webpack_require__(98).set
      const microtask = __webpack_require__(227)()
      const newPromiseCapabilityModule = __webpack_require__(97)
      const perform = __webpack_require__(226)
      const userAgent = __webpack_require__(225)
      const promiseResolve = __webpack_require__(224)
      const PROMISE = 'Promise'
      const TypeError = global.TypeError
      const process = global.process
      const versions = process && process.versions
      const v8 = versions && versions.v8 || ''
      let $Promise = global[PROMISE]
      const isNode = classof(process) == 'process'
      const empty = function () { /* empty */ }
      let Internal, newGenericPromiseCapability, OwnPromiseCapability, Wrapper
      let newPromiseCapability = newGenericPromiseCapability = newPromiseCapabilityModule.f

      const USE_NATIVE = !!(function () {
        try {
          // correct subclassing with @@species support
          const promise = $Promise.resolve(1)
          const FakePromise = (promise.constructor = {})[__webpack_require__(4)('species')] = function (exec) {
            exec(empty, empty)
          }
          // unhandled rejections tracking support, NodeJS Promise without it fails @@species test
          return (isNode || typeof PromiseRejectionEvent === 'function') &&
                            promise.then(empty) instanceof FakePromise &&
                            // v8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
                            // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
                            // we can't detect it synchronously, so just check versions
                            v8.indexOf('6.6') !== 0 &&
                            userAgent.indexOf('Chrome/66') === -1
        } catch (e) { /* empty */ }
      }())

      // helpers
      const isThenable = function (it) {
        let then
        return isObject(it) && typeof (then = it.then) === 'function' ? then : false
      }
      const notify = function (promise, isReject) {
        if (promise._n) return
        promise._n = true
        const chain = promise._c
        microtask(function () {
          const value = promise._v
          const ok = promise._s == 1
          let i = 0
          const run = function (reaction) {
            const handler = ok ? reaction.ok : reaction.fail
            const resolve = reaction.resolve
            const reject = reaction.reject
            const domain = reaction.domain
            let result, then, exited
            try {
              if (handler) {
                if (!ok) {
                  if (promise._h == 2) onHandleUnhandled(promise)
                  promise._h = 1
                }
                if (handler === true) result = value
                else {
                  if (domain) domain.enter()
                  result = handler(value) // may throw
                  if (domain) {
                    domain.exit()
                    exited = true
                  }
                }
                if (result === reaction.promise) {
                  reject(TypeError('Promise-chain cycle'))
                } else if (then = isThenable(result)) {
                  then.call(result, resolve, reject)
                } else resolve(result)
              } else reject(value)
            } catch (e) {
              if (domain && !exited) domain.exit()
              reject(e)
            }
          }
          while (chain.length > i) run(chain[i++]) // variable length - can't use forEach
          promise._c = []
          promise._n = false
          if (isReject && !promise._h) onUnhandled(promise)
        })
      }
      var onUnhandled = function (promise) {
        task.call(global, function () {
          const value = promise._v
          const unhandled = isUnhandled(promise)
          let result, handler, console
          if (unhandled) {
            result = perform(function () {
              if (isNode) {
                process.emit('unhandledRejection', value, promise)
              } else if (handler = global.onunhandledrejection) {
                handler({ promise: promise, reason: value })
              } else if ((console = global.console) && console.error) {
                console.error('Unhandled promise rejection', value)
              }
            })
            // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
            promise._h = isNode || isUnhandled(promise) ? 2 : 1
          }
          promise._a = undefined
          if (unhandled && result.e) throw result.v
        })
      }
      var isUnhandled = function (promise) {
        return promise._h !== 1 && (promise._a || promise._c).length === 0
      }
      var onHandleUnhandled = function (promise) {
        task.call(global, function () {
          let handler
          if (isNode) {
            process.emit('rejectionHandled', promise)
          } else if (handler = global.onrejectionhandled) {
            handler({ promise: promise, reason: promise._v })
          }
        })
      }
      const $reject = function (value) {
        let promise = this
        if (promise._d) return
        promise._d = true
        promise = promise._w || promise // unwrap
        promise._v = value
        promise._s = 2
        if (!promise._a) promise._a = promise._c.slice()
        notify(promise, true)
      }
      var $resolve = function (value) {
        let promise = this
        let then
        if (promise._d) return
        promise._d = true
        promise = promise._w || promise // unwrap
        try {
          if (promise === value) throw TypeError("Promise can't be resolved itself")
          if (then = isThenable(value)) {
            microtask(function () {
              const wrapper = { _w: promise, _d: false } // wrap
              try {
                then.call(value, ctx($resolve, wrapper, 1), ctx($reject, wrapper, 1))
              } catch (e) {
                $reject.call(wrapper, e)
              }
            })
          } else {
            promise._v = value
            promise._s = 1
            notify(promise, false)
          }
        } catch (e) {
          $reject.call({ _w: promise, _d: false }, e) // wrap
        }
      }

      // constructor polyfill
      if (!USE_NATIVE) {
        // 25.4.3.1 Promise(executor)
        $Promise = function Promise (executor) {
          anInstance(this, $Promise, PROMISE, '_h')
          aFunction(executor)
          Internal.call(this)
          try {
            executor(ctx($resolve, this, 1), ctx($reject, this, 1))
          } catch (err) {
            $reject.call(this, err)
          }
        }
        // eslint-disable-next-line no-unused-vars
        Internal = function Promise (executor) {
          this._c = [] // <- awaiting reactions
          this._a = undefined // <- checked in isUnhandled reactions
          this._s = 0 // <- state
          this._d = false // <- done
          this._v = undefined // <- value
          this._h = 0 // <- rejection state, 0 - default, 1 - handled, 2 - unhandled
          this._n = false // <- notify
        }
        Internal.prototype = __webpack_require__(223)($Promise.prototype, {
          // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
          then: function then (onFulfilled, onRejected) {
            const reaction = newPromiseCapability(speciesConstructor(this, $Promise))
            reaction.ok = typeof onFulfilled === 'function' ? onFulfilled : true
            reaction.fail = typeof onRejected === 'function' && onRejected
            reaction.domain = isNode ? process.domain : undefined
            this._c.push(reaction)
            if (this._a) this._a.push(reaction)
            if (this._s) notify(this, false)
            return reaction.promise
          },
          // 25.4.5.1 Promise.prototype.catch(onRejected)
          catch: function (onRejected) {
            return this.then(undefined, onRejected)
          }
        })
        OwnPromiseCapability = function () {
          const promise = new Internal()
          this.promise = promise
          this.resolve = ctx($resolve, promise, 1)
          this.reject = ctx($reject, promise, 1)
        }
        newPromiseCapabilityModule.f = newPromiseCapability = function (C) {
          return C === $Promise || C === Wrapper
            ? new OwnPromiseCapability(C)
            : newGenericPromiseCapability(C)
        }
      }

      $export($export.G + $export.W + $export.F * !USE_NATIVE, { Promise: $Promise })
      __webpack_require__(46)($Promise, PROMISE)
      __webpack_require__(222)(PROMISE)
      Wrapper = __webpack_require__(7)[PROMISE]

      // statics
      $export($export.S + $export.F * !USE_NATIVE, PROMISE, {
        // 25.4.4.5 Promise.reject(r)
        reject: function reject (r) {
          const capability = newPromiseCapability(this)
          const $$reject = capability.reject
          $$reject(r)
          return capability.promise
        }
      })
      $export($export.S + $export.F * (LIBRARY || !USE_NATIVE), PROMISE, {
        // 25.4.4.6 Promise.resolve(x)
        resolve: function resolve (x) {
          return promiseResolve(LIBRARY && this === Wrapper ? $Promise : this, x)
        }
      })
      $export($export.S + $export.F * !(USE_NATIVE && __webpack_require__(221)(function (iter) {
        $Promise.all(iter).catch(empty)
      })), PROMISE, {
        // 25.4.4.1 Promise.all(iterable)
        all: function all (iterable) {
          const C = this
          const capability = newPromiseCapability(C)
          const resolve = capability.resolve
          const reject = capability.reject
          const result = perform(function () {
            const values = []
            let index = 0
            let remaining = 1
            forOf(iterable, false, function (promise) {
              const $index = index++
              let alreadyCalled = false
              values.push(undefined)
              remaining++
              C.resolve(promise).then(function (value) {
                if (alreadyCalled) return
                alreadyCalled = true
                values[$index] = value
                --remaining || resolve(values)
              }, reject)
            })
            --remaining || resolve(values)
          })
          if (result.e) reject(result.v)
          return capability.promise
        },
        // 25.4.4.4 Promise.race(iterable)
        race: function race (iterable) {
          const C = this
          const capability = newPromiseCapability(C)
          const reject = capability.reject
          const result = perform(function () {
            forOf(iterable, false, function (promise) {
              C.resolve(promise).then(capability.resolve, reject)
            })
          })
          if (result.e) reject(result.v)
          return capability.promise
        }
      })
      /***/
    },
    /* 236 */
    /***/
    function (module, exports) {
      module.exports = function (done, value) {
        return { value: value, done: !!done }
      }
      /***/
    },
    /* 237 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const addToUnscopables = __webpack_require__(99)
      const step = __webpack_require__(236)
      const Iterators = __webpack_require__(37)
      const toIObject = __webpack_require__(19)

      // 22.1.3.4 Array.prototype.entries()
      // 22.1.3.13 Array.prototype.keys()
      // 22.1.3.29 Array.prototype.values()
      // 22.1.3.30 Array.prototype[@@iterator]()
      module.exports = __webpack_require__(101)(Array, 'Array', function (iterated, kind) {
        this._t = toIObject(iterated) // target
        this._i = 0 // next index
        this._k = kind // kind
        // 22.1.5.2.1 %ArrayIteratorPrototype%.next()
      }, function () {
        const O = this._t
        const kind = this._k
        const index = this._i++
        if (!O || index >= O.length) {
          this._t = undefined
          return step(1)
        }
        if (kind == 'keys') return step(0, index)
        if (kind == 'values') return step(0, O[index])
        return step(0, [index, O[index]])
      }, 'values')

      // argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
      Iterators.Arguments = Iterators.Array

      addToUnscopables('keys')
      addToUnscopables('values')
      addToUnscopables('entries')
      /***/
    },
    /* 238 */
    /***/
    function (module, exports, __webpack_require__) {
      const $iterators = __webpack_require__(237)
      const getKeys = __webpack_require__(29)
      const redefine = __webpack_require__(30)
      const global = __webpack_require__(5)
      const hide = __webpack_require__(22)
      const Iterators = __webpack_require__(37)
      const wks = __webpack_require__(4)
      const ITERATOR = wks('iterator')
      const TO_STRING_TAG = wks('toStringTag')
      const ArrayValues = Iterators.Array

      const DOMIterables = {
        CSSRuleList: true, // TODO: Not spec compliant, should be false.
        CSSStyleDeclaration: false,
        CSSValueList: false,
        ClientRectList: false,
        DOMRectList: false,
        DOMStringList: false,
        DOMTokenList: true,
        DataTransferItemList: false,
        FileList: false,
        HTMLAllCollection: false,
        HTMLCollection: false,
        HTMLFormElement: false,
        HTMLSelectElement: false,
        MediaList: true, // TODO: Not spec compliant, should be false.
        MimeTypeArray: false,
        NamedNodeMap: false,
        NodeList: true,
        PaintRequestList: false,
        Plugin: false,
        PluginArray: false,
        SVGLengthList: false,
        SVGNumberList: false,
        SVGPathSegList: false,
        SVGPointList: false,
        SVGStringList: false,
        SVGTransformList: false,
        SourceBufferList: false,
        StyleSheetList: true, // TODO: Not spec compliant, should be false.
        TextTrackCueList: false,
        TextTrackList: false,
        TouchList: false
      }

      for (let collections = getKeys(DOMIterables), i = 0; i < collections.length; i++) {
        const NAME = collections[i]
        const explicit = DOMIterables[NAME]
        const Collection = global[NAME]
        const proto = Collection && Collection.prototype
        var key
        if (proto) {
          if (!proto[ITERATOR]) hide(proto, ITERATOR, ArrayValues)
          if (!proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME)
          Iterators[NAME] = ArrayValues
          if (explicit) {
            for (key in $iterators) { if (!proto[key]) redefine(proto, key, $iterators[key], true) }
          }
        }
      }
      /***/
    },
    /* 239 */
    /***/
    function (module, exports, __webpack_require__) {
      // 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
      const has = __webpack_require__(24)
      const toObject = __webpack_require__(100)
      const IE_PROTO = __webpack_require__(71)('IE_PROTO')
      const ObjectProto = Object.prototype

      module.exports = Object.getPrototypeOf || function (O) {
        O = toObject(O)
        if (has(O, IE_PROTO)) return O[IE_PROTO]
        if (typeof O.constructor === 'function' && O instanceof O.constructor) {
          return O.constructor.prototype
        }
        return O instanceof Object ? ObjectProto : null
      }
      /***/
    },
    /* 240 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const create = __webpack_require__(105)
      const descriptor = __webpack_require__(49)
      const setToStringTag = __webpack_require__(46)
      const IteratorPrototype = {}

      // 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
      __webpack_require__(22)(IteratorPrototype, __webpack_require__(4)('iterator'), function () { return this })

      module.exports = function (Constructor, NAME, next) {
        Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) })
        setToStringTag(Constructor, NAME + ' Iterator')
      }
      /***/
    },
    /* 241 */
    /***/
    function (module, exports, __webpack_require__) {
      const toInteger = __webpack_require__(72)
      const defined = __webpack_require__(73)
      // true  -> String#at
      // false -> String#codePointAt
      module.exports = function (TO_STRING) {
        return function (that, pos) {
          const s = String(defined(that))
          const i = toInteger(pos)
          const l = s.length
          let a, b
          if (i < 0 || i >= l) return TO_STRING ? '' : undefined
          a = s.charCodeAt(i)
          return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
            ? TO_STRING ? s.charAt(i) : a
            : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000
        }
      }
      /***/
    },
    /* 242 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      const $at = __webpack_require__(241)(true)

      // 21.1.3.27 String.prototype[@@iterator]()
      __webpack_require__(101)(String, 'String', function (iterated) {
        this._t = String(iterated) // target
        this._i = 0 // next index
        // 21.1.5.2.1 %StringIteratorPrototype%.next()
      }, function () {
        const O = this._t
        const index = this._i
        let point
        if (index >= O.length) return { value: undefined, done: true }
        point = $at(O, index)
        this._i += point.length
        return { value: point, done: false }
      })
      /***/
    },
    /* 243 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(102)
      __webpack_require__(242)
      __webpack_require__(238)
      __webpack_require__(235)
      module.exports = __webpack_require__(7).Promise
      /***/
    },
    /* 244 */
    /***/
    function (module, exports, __webpack_require__) {
      const pIE = __webpack_require__(38)
      const createDesc = __webpack_require__(49)
      const toIObject = __webpack_require__(19)
      const toPrimitive = __webpack_require__(75)
      const has = __webpack_require__(24)
      const IE8_DOM_DEFINE = __webpack_require__(111)
      const gOPD = Object.getOwnPropertyDescriptor

      exports.f = __webpack_require__(23) ? gOPD : function getOwnPropertyDescriptor (O, P) {
        O = toIObject(O)
        P = toPrimitive(P, true)
        if (IE8_DOM_DEFINE) {
          try {
            return gOPD(O, P)
          } catch (e) { /* empty */ }
        }
        if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P])
      }
      /***/
    },
    /* 245 */
    /***/
    function (module, exports, __webpack_require__) {
      // fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
      const toIObject = __webpack_require__(19)
      const gOPN = __webpack_require__(103).f
      const toString = {}.toString

      const windowNames = typeof window === 'object' && window && Object.getOwnPropertyNames
        ? Object.getOwnPropertyNames(window) : []

      const getWindowNames = function (it) {
        try {
          return gOPN(it)
        } catch (e) {
          return windowNames.slice()
        }
      }

      module.exports.f = function getOwnPropertyNames (it) {
        return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it))
      }
      /***/
    },
    /* 246 */
    /***/
    function (module, exports, __webpack_require__) {
      const dP = __webpack_require__(21)
      const anObject = __webpack_require__(14)
      const getKeys = __webpack_require__(29)

      module.exports = __webpack_require__(23) ? Object.defineProperties : function defineProperties (O, Properties) {
        anObject(O)
        const keys = getKeys(Properties)
        const length = keys.length
        let i = 0
        let P
        while (length > i) dP.f(O, P = keys[i++], Properties[P])
        return O
      }
      /***/
    },
    /* 247 */
    /***/
    function (module, exports, __webpack_require__) {
      // 7.2.2 IsArray(argument)
      const cof = __webpack_require__(39)
      module.exports = Array.isArray || function isArray (arg) {
        return cof(arg) == 'Array'
      }
      /***/
    },
    /* 248 */
    /***/
    function (module, exports, __webpack_require__) {
      const toInteger = __webpack_require__(72)
      const max = Math.max
      const min = Math.min
      module.exports = function (index, length) {
        index = toInteger(index)
        return index < 0 ? max(index + length, 0) : min(index, length)
      }
      /***/
    },
    /* 249 */
    /***/
    function (module, exports, __webpack_require__) {
      // all enumerable object keys, includes symbols
      const getKeys = __webpack_require__(29)
      const gOPS = __webpack_require__(69)
      const pIE = __webpack_require__(38)
      module.exports = function (it) {
        const result = getKeys(it)
        const getSymbols = gOPS.f
        if (getSymbols) {
          const symbols = getSymbols(it)
          const isEnum = pIE.f
          let i = 0
          let key
          while (symbols.length > i) { if (isEnum.call(it, key = symbols[i++])) result.push(key) }
        }
        return result
      }
      /***/
    },
    /* 250 */
    /***/
    function (module, exports, __webpack_require__) {
      const global = __webpack_require__(5)
      const core = __webpack_require__(7)
      const LIBRARY = __webpack_require__(40)
      const wksExt = __webpack_require__(110)
      const defineProperty = __webpack_require__(21).f
      module.exports = function (name) {
        const $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {})
        if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) })
      }
      /***/
    },
    /* 251 */
    /***/
    function (module, exports, __webpack_require__) {
      const META = __webpack_require__(41)('meta')
      const isObject = __webpack_require__(20)
      const has = __webpack_require__(24)
      const setDesc = __webpack_require__(21).f
      let id = 0
      const isExtensible = Object.isExtensible || function () {
        return true
      }
      const FREEZE = !__webpack_require__(42)(function () {
        return isExtensible(Object.preventExtensions({}))
      })
      const setMeta = function (it) {
        setDesc(it, META, {
          value: {
            i: 'O' + ++id, // object ID
            w: {} // weak collections IDs
          }
        })
      }
      const fastKey = function (it, create) {
        // return primitive with prefix
        if (!isObject(it)) return typeof it === 'symbol' ? it : (typeof it === 'string' ? 'S' : 'P') + it
        if (!has(it, META)) {
          // can't set metadata to uncaught frozen object
          if (!isExtensible(it)) return 'F'
          // not necessary to add metadata
          if (!create) return 'E'
          // add missing metadata
          setMeta(it)
          // return object ID
        }
        return it[META].i
      }
      const getWeak = function (it, create) {
        if (!has(it, META)) {
          // can't set metadata to uncaught frozen object
          if (!isExtensible(it)) return true
          // not necessary to add metadata
          if (!create) return false
          // add missing metadata
          setMeta(it)
          // return hash weak collections IDs
        }
        return it[META].w
      }
      // add metadata on freeze-family methods calling
      const onFreeze = function (it) {
        if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it)
        return it
      }
      var meta = module.exports = {
        KEY: META,
        NEED: false,
        fastKey: fastKey,
        getWeak: getWeak,
        onFreeze: onFreeze
      }
      /***/
    },
    /* 252 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      // ECMAScript 6 symbols shim
      const global = __webpack_require__(5)
      const has = __webpack_require__(24)
      const DESCRIPTORS = __webpack_require__(23)
      const $export = __webpack_require__(31)
      const redefine = __webpack_require__(30)
      const META = __webpack_require__(251).KEY
      const $fails = __webpack_require__(42)
      const shared = __webpack_require__(74)
      const setToStringTag = __webpack_require__(46)
      const uid = __webpack_require__(41)
      const wks = __webpack_require__(4)
      const wksExt = __webpack_require__(110)
      const wksDefine = __webpack_require__(250)
      const enumKeys = __webpack_require__(249)
      const isArray = __webpack_require__(247)
      const anObject = __webpack_require__(14)
      const isObject = __webpack_require__(20)
      const toIObject = __webpack_require__(19)
      const toPrimitive = __webpack_require__(75)
      const createDesc = __webpack_require__(49)
      const _create = __webpack_require__(105)
      const gOPNExt = __webpack_require__(245)
      const $GOPD = __webpack_require__(244)
      const $DP = __webpack_require__(21)
      const $keys = __webpack_require__(29)
      const gOPD = $GOPD.f
      const dP = $DP.f
      const gOPN = gOPNExt.f
      let $Symbol = global.Symbol
      const $JSON = global.JSON
      const _stringify = $JSON && $JSON.stringify
      const PROTOTYPE = 'prototype'
      const HIDDEN = wks('_hidden')
      const TO_PRIMITIVE = wks('toPrimitive')
      const isEnum = {}.propertyIsEnumerable
      const SymbolRegistry = shared('symbol-registry')
      const AllSymbols = shared('symbols')
      const OPSymbols = shared('op-symbols')
      const ObjectProto = Object[PROTOTYPE]
      const USE_NATIVE = typeof $Symbol === 'function'
      const QObject = global.QObject
      // Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
      let setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild

      // fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
      const setSymbolDesc = DESCRIPTORS && $fails(function () {
        return _create(dP({}, 'a', {
          get: function () { return dP(this, 'a', { value: 7 }).a }
        })).a != 7
      }) ? function (it, key, D) {
            const protoDesc = gOPD(ObjectProto, key)
            if (protoDesc) delete ObjectProto[key]
            dP(it, key, D)
            if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc)
          } : dP

      const wrap = function (tag) {
        const sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE])
        sym._k = tag
        return sym
      }

      const isSymbol = USE_NATIVE && typeof $Symbol.iterator === 'symbol' ? function (it) {
        return typeof it === 'symbol'
      } : function (it) {
        return it instanceof $Symbol
      }

      var $defineProperty = function defineProperty (it, key, D) {
        if (it === ObjectProto) $defineProperty(OPSymbols, key, D)
        anObject(it)
        key = toPrimitive(key, true)
        anObject(D)
        if (has(AllSymbols, key)) {
          if (!D.enumerable) {
            if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}))
            it[HIDDEN][key] = true
          } else {
            if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false
            D = _create(D, { enumerable: createDesc(0, false) })
          }
          return setSymbolDesc(it, key, D)
        }
        return dP(it, key, D)
      }
      const $defineProperties = function defineProperties (it, P) {
        anObject(it)
        const keys = enumKeys(P = toIObject(P))
        let i = 0
        const l = keys.length
        let key
        while (l > i) $defineProperty(it, key = keys[i++], P[key])
        return it
      }
      const $create = function create (it, P) {
        return P === undefined ? _create(it) : $defineProperties(_create(it), P)
      }
      const $propertyIsEnumerable = function propertyIsEnumerable (key) {
        const E = isEnum.call(this, key = toPrimitive(key, true))
        if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false
        return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true
      }
      const $getOwnPropertyDescriptor = function getOwnPropertyDescriptor (it, key) {
        it = toIObject(it)
        key = toPrimitive(key, true)
        if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return
        const D = gOPD(it, key)
        if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true
        return D
      }
      const $getOwnPropertyNames = function getOwnPropertyNames (it) {
        const names = gOPN(toIObject(it))
        const result = []
        let i = 0
        let key
        while (names.length > i) {
          if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key)
        }
        return result
      }
      const $getOwnPropertySymbols = function getOwnPropertySymbols (it) {
        const IS_OP = it === ObjectProto
        const names = gOPN(IS_OP ? OPSymbols : toIObject(it))
        const result = []
        let i = 0
        let key
        while (names.length > i) {
          if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key])
        }
        return result
      }

      // 19.4.1.1 Symbol([description])
      if (!USE_NATIVE) {
        $Symbol = function Symbol () {
          if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!')
          const tag = uid(arguments.length > 0 ? arguments[0] : undefined)
          var $set = function (value) {
            if (this === ObjectProto) $set.call(OPSymbols, value)
            if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false
            setSymbolDesc(this, tag, createDesc(1, value))
          }
          if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set })
          return wrap(tag)
        }
        redefine($Symbol[PROTOTYPE], 'toString', function toString () {
          return this._k
        })

        $GOPD.f = $getOwnPropertyDescriptor
        $DP.f = $defineProperty
        __webpack_require__(103).f = gOPNExt.f = $getOwnPropertyNames
        __webpack_require__(38).f = $propertyIsEnumerable
        __webpack_require__(69).f = $getOwnPropertySymbols

        if (DESCRIPTORS && !__webpack_require__(40)) {
          redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true)
        }

        wksExt.f = function (name) {
          return wrap(wks(name))
        }
      }

      $export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol })

      for (let es6Symbols = (
        // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
          'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
        ).split(','), j = 0; es6Symbols.length > j;) wks(es6Symbols[j++])

      for (let wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++])

      $export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
        // 19.4.2.1 Symbol.for(key)
        for: function (key) {
          return has(SymbolRegistry, key += '')
            ? SymbolRegistry[key]
            : SymbolRegistry[key] = $Symbol(key)
        },
        // 19.4.2.5 Symbol.keyFor(sym)
        keyFor: function keyFor (sym) {
          if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!')
          for (const key in SymbolRegistry) { if (SymbolRegistry[key] === sym) return key }
        },
        useSetter: function () { setter = true },
        useSimple: function () { setter = false }
      })

      $export($export.S + $export.F * !USE_NATIVE, 'Object', {
        // 19.1.2.2 Object.create(O [, Properties])
        create: $create,
        // 19.1.2.4 Object.defineProperty(O, P, Attributes)
        defineProperty: $defineProperty,
        // 19.1.2.3 Object.defineProperties(O, Properties)
        defineProperties: $defineProperties,
        // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
        getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
        // 19.1.2.7 Object.getOwnPropertyNames(O)
        getOwnPropertyNames: $getOwnPropertyNames,
        // 19.1.2.8 Object.getOwnPropertySymbols(O)
        getOwnPropertySymbols: $getOwnPropertySymbols
      })

      // 24.3.2 JSON.stringify(value [, replacer [, space]])
      $JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
        const S = $Symbol()
        // MS Edge converts symbol values to JSON as {}
        // WebKit converts symbol values to JSON as null
        // V8 throws on boxed symbols
        return _stringify([S]) !== '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}'
      })), 'JSON', {
        stringify: function stringify (it) {
          const args = [it]
          let i = 1
          let replacer, $replacer
          while (arguments.length > i) args.push(arguments[i++])
          $replacer = replacer = args[1]
          if (!isObject(replacer) && it === undefined || isSymbol(it)) return // IE8 returns string on undefined
          if (!isArray(replacer)) {
            replacer = function (key, value) {
              if (typeof $replacer === 'function') value = $replacer.call(this, key, value)
              if (!isSymbol(value)) return value
            }
          }
          args[1] = replacer
          return _stringify.apply($JSON, args)
        }
      })

      // 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
      $Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(22)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf)
      // 19.4.3.5 Symbol.prototype[@@toStringTag]
      setToStringTag($Symbol, 'Symbol')
      // 20.2.1.9 Math[@@toStringTag]
      setToStringTag(Math, 'Math', true)
      // 24.3.3 JSON[@@toStringTag]
      setToStringTag(global.JSON, 'JSON', true)
      /***/
    },
    /* 253 */
    /***/
    function (module, exports, __webpack_require__) {
      __webpack_require__(252)
      __webpack_require__(102)
      module.exports = __webpack_require__(7).Symbol
      /***/
    },
    /* 254 */
    /***/
    function (module, exports, __webpack_require__) {
      'use strict'

      Object.defineProperty(exports, '__esModule', {
        value: true
      })
      exports.Plugins = exports.Sensors = exports.Sortable = exports.Swappable = exports.Droppable = exports.Draggable = exports.BasePlugin = exports.BaseEvent = undefined

      __webpack_require__(253)

      __webpack_require__(243)

      __webpack_require__(220)

      __webpack_require__(217)

      __webpack_require__(214)

      const _AbstractEvent = __webpack_require__(9)

      const _AbstractEvent2 = _interopRequireDefault(_AbstractEvent)

      const _AbstractPlugin = __webpack_require__(10)

      const _AbstractPlugin2 = _interopRequireDefault(_AbstractPlugin)

      const _Sensors = __webpack_require__(65)

      const Sensors = _interopRequireWildcard(_Sensors)

      const _Plugins = __webpack_require__(153)

      const Plugins = _interopRequireWildcard(_Plugins)

      const _Draggable = __webpack_require__(43)

      const _Draggable2 = _interopRequireDefault(_Draggable)

      const _Droppable = __webpack_require__(126)

      const _Droppable2 = _interopRequireDefault(_Droppable)

      const _Swappable = __webpack_require__(117)

      const _Swappable2 = _interopRequireDefault(_Swappable)

      const _Sortable = __webpack_require__(114)

      const _Sortable2 = _interopRequireDefault(_Sortable)

      function _interopRequireWildcard (obj) {
        if (obj && obj.__esModule) { return obj } else {
          const newObj = {}; if (obj != null) { for (const key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key] } }
          newObj.default = obj; return newObj
        }
      }

      function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

      exports.BaseEvent = _AbstractEvent2.default
      exports.BasePlugin = _AbstractPlugin2.default
      exports.Draggable = _Draggable2.default
      exports.Droppable = _Droppable2.default
      exports.Swappable = _Swappable2.default
      exports.Sortable = _Sortable2.default
      exports.Sensors = Sensors
      exports.Plugins = Plugins
      /***/
    }
    /******/
  ])
})
