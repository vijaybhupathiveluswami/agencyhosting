<script type="text/x-handlebars-template" id="template">

   {{#each this}}
   <tr>
      <td>{{user.mobile}}</td>
      <td>{{order.app_name}}</td>
      <td>{{order.customer_id}}</td>
      <td>{{order.bill_amount}}</td>
      <td>{{order.coins}}</td>
      <td>{{order.payment_gw}}</td>
      <td>{{created_at}}</td>      
      <td>
      {{#switch payment_status}}
      {{#case 0}} <span class="badge badge-primary">REQUESTED{{/case}}
      {{#case 1}} <span class="badge badge-info">QUEUED {{/case}}
      {{#case 2}} <span class="badge badge-warning">PROCESSING {{/case}}
      {{#case 3}} <span class="badge badge-secondary">VALIDATION {{/case}}
      {{#case 4}} <span class="badge badge-danger">FAILED {{/case}}
      {{#case 5}} <span class="badge badge-success">SUCCESS{{/case}} 
      {{#case 6}} <span class="badge badge-secondary">REFUND {{/case}}
      {{/switch}}
      </span>      
      </td>
      <td>{{order.fulfilled_app_name}}</td>
      <td>{{order.fc_status}}</td>
      <td>
      {{#switch product_status}}
      {{#case 0}} <span class="badge badge-primary">REQUESTED{{/case}}
      {{#case 1}} <span class="badge badge-info">QUEUED {{/case}}
      {{#case 2}} <span class="badge badge-warning">PROCESSING {{/case}}
      {{#case 3}} <span class="badge badge-secondary">VALIDATION {{/case}}
      {{#case 4}} <span class="badge badge-danger">FAILED {{/case}}
      {{#case 5}} <span class="badge badge-success">SUCCESS{{/case}} 
      {{#case 6}} <span class="badge badge-secondary">REFUND {{/case}}
      {{/switch}}
      </span>           
      </td>
      <td>{{order.order_delivery_date}}</td>
      <td>{{order.notes}}</td>
      <td style="word-break: break-all">{{order.order_id}}</td>
   </tr>
   {{/each}}
</script>