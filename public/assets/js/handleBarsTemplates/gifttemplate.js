<script type="text/x-handlebars-template" id="template">

   {{#each this}}
   <tr>
      <td>{{created_at}}</td>
      <td>{{user_id}}</td>
      <td>{{user.name}}</td>
      <td>{{gift_value}}</td>
      <td>{{receiver_id}}</td>
      <td>{{receiver.name}}</td>
      <td>{{gift_name}}</td>
      <td>{{gift_value}}</td>
   </tr>
   {{/each}}
</script>

<script type="text/x-handlebars-template" id="templateHead">
            <th class="text-center">Date</th>
            <th class="text-center">Sender ID</th>
            <th class="text-center">Sender Name</th>
            <th class="text-center">Gift Value</th>
            <th class="text-center">Receiver ID	</th>
            <th class="text-center">Receiver Name	</th>
            <th class="text-center">Gift Name	</th>
            <th class="text-center">Gift Value</th>
</script>
<script type="text/x-handlebars-template" id="sendHead">
            <th class="text-center">Date</th>
            <th class="text-center">Sender ID</th>
            <th class="text-center">Sender Name</th>
            <th class="text-center">Gift Value</th>
</script>
<script type="text/x-handlebars-template" id="receiveHead">
            <th class="text-center">Date</th>
            <th class="text-center">Receiver ID	</th>
            <th class="text-center">Receiver Name	</th>
            <th class="text-center">Gift Value</th>
</script>

<script type="text/x-handlebars-template" id="sendTB">
{{#each this}}
<tr>
   <td>{{created_at}}</td>
   <td>{{user_id}}</td>
   <td>{{user.name}}</td>
   <td>{{gift_value}}</td>
</tr>
{{/each}}
</script>

<script type="text/x-handlebars-template" id="receiveTB">
{{#each this}}
<tr>
   <td>{{created_at}}</td>
   <td>{{receiver_id}}</td>
   <td>{{receiver.name}}</td>
   <td>{{gift_value}}</td>
</tr>
{{/each}}

</script>