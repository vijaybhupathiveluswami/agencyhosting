<script type="text/x-handlebars-template" id="template">

   {{#each this}}       
            <tr class="btn-reveal-trigger">
              <td class="align-middle">{{inc1 @index}}</td>                    
              <td class="align-middle">{{app.app_name}}</td>
              <td class="align-middle text-info font-weight-extra-bold">{{opening_stock}}</td>
              <td class="align-middle text-success font-weight-semi-bold">{{purchase_stock}}</td>
              <td class="align-middle text-danger">{{recharge_stock}}</td>
              <td class="align-middle text-success font-weight-semi-bold">{{adj_plus}}</td>
              <td class="align-middle text-danger">{{adj_minus}}</td>           
              <td class="align-middle text-primary font-weight-extra-bold">{{closing_stock}}</td>         
            </tr>
   {{/each}}
</script>