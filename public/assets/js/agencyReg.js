var dataTable; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    $.ajax({
        url: "/v1/agencyFollowers",
        method: 'get',
        dataType: 'json',
        data: { type: 'create' },
        success: function(response) {
            console.log(response);
            var data = response['body']['data'];
            for (var i = 0; i < data.length; i++) {
                console.log(data[i])
                $('#body').append('<tr><td><img class="rounded-circle" src="' + data[i]['selfie'] + '" width="70" alt="' + data[i]['agencyName'] + '" onclick = "profile(' + data[i]['id'] + ')" / > </td><td>' + data[i]['agencyName'] + '</td><td>' + data[i]['name'] + '</td><td><i class="fa fa-edit" aria-hidden="true" onclick="edit(' + data[i]['id'] + ')"></i></td><td><i class="fa fa-trash" aria-hidden="true" onclick="myFunction(' + data[i]['id'] + ')"></i> </td></tr>')

            }
            dataTable = $('#example').DataTable({});
        }
    });
});

function myFunction(id) {
    var tempJson = {
        id: id
    }
    $.ajax({
        url: '/v1/agencyFollowersDelete',
        method: 'post',
        data: tempJson,
        success: function() {
            $.ajax({
                url: "/v1/agencyFollowers",
                method: 'get',
                dataType: 'json',
                data: { type: 'create' },
                success: function(response) {
                    console.log(response);
                    var data = response['body']['data'];
                    $('#body').html('')
                    if ($.fn.DataTable.isDataTable('#example')) {
                        $('#example').DataTable().clear().destroy();
                    }
                    for (var i = 0; i < data.length; i++) {
                        console.log(data[i])
                        $('#body').append('<tr><td><img class="rounded-circle" src="' + data[i]['selfie'] + '" width="70" alt="' + data[i]['agencyName'] + '" onclick = "profile(' + data[i]['id'] + ')" / > </td><td>' + data[i]['agencyName'] + '</td><td>' + data[i]['name'] + '</td><td><i class="fa fa-edit" aria-hidden="true" onclick="edit(' + data[i]['id'] + ')"></i></td><td><i class="fa fa-trash" aria-hidden="true" onclick="myFunction(' + data[i]['id'] + ')"></i> </td></tr>')
                    }

                    // ... skipped ...

                    $('#example').DataTable({});
                }
            });
        }
    });
}




function newForm() {
    alert('newForm')
    $("#submitButton").attr("onclick", 'create()');
    $('#ModalLoginForm').modal()
        // $.showModal({ title: "Hello World!", body: "A very simple modal dialog without buttons." })
}

function createUpdate(id, type) {

    alert('entered into create update')
    alert(id, type)
    var formData = new FormData();
    formData.append('id', id)
    formData.append('type', type)
    formData.append('dreamliveID', $("#dreamliveid").val());
    formData.append('name', $("#name").val());
    formData.append('agencyName', $("#agencyname").val());
    formData.append('agencyCode', $("#agencycode").val());
    formData.append('email', $("#email").val());
    formData.append('contact', $("#whatsapp").val());
    formData.append('address', $("#address").val());
    formData.append('city', $("#city").val());
    formData.append('state', $("#state").val());
    formData.append('country', $("#country").val());
    formData.append('pinCode', $("#pincode").val());
    formData.append('userName', $("#username").val());
    formData.append('password', $("#password").val());
    formData.append('idType', $('input[name="adharid"]:checked').val());
    formData.append('nationalId', $('#adharid').val());
    formData.append('nationalIdFront', $("#nationalidfront").prop("files")[0])
    formData.append('nationalIdBack', $("#nationalidback").prop("files")[0])
    formData.append('selfiePic', $("#selfiepic").prop("files")[0])
    formData.append('selfieNational', $("#selfieNational").prop("files")[0])
    formData.append('agencyPic', $("#agencyPic").prop("files")[0])


    console.log(formData)
    $('#ModalLoginForm').modal('hide')
    $.ajax({
        url: "/v1/updateFollowers",
        type: "POST", //send it through get method
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function() {
            $.ajax({
                url: "/v1/agencyFollowers",
                method: 'get',
                dataType: 'json',
                data: { type: 'create' },
                success: function(response) {
                    console.log(response);
                    var data = response['body']['data'];
                    $('#body').html('')
                    if ($.fn.DataTable.isDataTable('#example')) {
                        $('#example').DataTable().clear().destroy();
                    }
                    for (var i = 0; i < data.length; i++) {
                        console.log(data[i])
                        $('#body').append('<tr><td><img class="rounded-circle" src="' + data[i]['selfie'] + '" width="70" alt="' + data[i]['agencyName'] + '" onclick = "profile(' + data[i]['id'] + ')" / > </td><td>' + data[i]['agencyName'] + '</td><td>' + data[i]['name'] + '</td><td><i class="fa fa-edit" aria-hidden="true" onclick="edit(' + data[i]['id'] + ')"></i></td><td><i class="fa fa-trash" aria-hidden="true" onclick="myFunction(' + data[i]['id'] + ')"></i> </td></tr>')
                    }

                    // ... skipped ...

                    $('#example').DataTable({});
                }
            });
        }

    });
}





$.getJSON("../public/assets/json/countryStates.json", function(data) {
    var countryList = []
    $.each(data, function(index, value) {
        var temp = {
            id: index,
            text: value.country
        }
        countryList.push(temp)
    });

    $("#country").select2({
        theme: "bootstrap4",
        placeholder: 'Select Country',
        dropdownParent: $("#dropSelect2"),
        minimumInputLength: 3,
        data: countryList
    });
});


var country = $("#country").val();
console.log(country)

function loadState() {
    $.getJSON("../public/assets/json/countryStates.json", function(data) {
        var states = []
        var country = $("#country").val();
        console.log(country)
        $.each(data[country]['states'], function(index, value) {
            var temp = {
                id: index,
                text: value
            }
            states.push(temp)
        });

        $("#state").html('')
        $("#state").select2({
            theme: "bootstrap4",
            placeholder: 'Select State',
            dropdownParent: $("#dropSelect2"),
            data: states
        });
    });
}


function create() {
    const dreamId = $("#dreamliveid").val()
    const agencyCode = $("#agencycode").val()
    const adhaar = $('#adharid').val()
    const pan = $('#panid').val()

    // var update = 'createUpdate(0,"create")'
    // $("#submitButton").attr("onclick", update);
    // $("#submitButton").prop('value', 'Create');
    // $('#ModalLoginForm').modal()


    $.ajax({
        url: "/v1/agencyFollowersValidate",
        method: 'get',
        dataType: 'json',
        data: { dreamId: dreamId, agencyCode: agencyCode, adhaar: adhaar, pan: pan },
        success: function(response) {
            createUpdate(0, 'create')
            alert('hai')
        }
    });

}

function edit(id) {
    const dreamId = $("#dreamliveid").val()
    const agencyCode = $("#agencycode").val()
    const adhaar = $('#adharid').val()
    const pan = $('#panid').val()

    $.ajax({
        url: "/v1/agencyFollowersValidate",
        method: 'get',
        dataType: 'json',
        data: { dreamId: dreamId, agencyCode: agencyCode, adhaar: adhaar, pan: pan },
        success: function(response) {
            alert('kkkk');
            createUpdate(id, "edited")

        }
    });

}



// function profile(id) {
//     $.ajax({
//         url: '/v1/agencyFollowers',
//         type: 'get',
//         data: tempJson,
//         $('#profilemodal').html(''),
//         success: function(response) {
//             let data = response.body.data[0]
//             let text = '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title" id="myModalLabel">More About ' + data['agencyName'] + '</h4></div><div class="modal-body"><div class="card text-white bg-dark"> <center>        <img src="' + data['agencyPic'] + '"name="aboutme" width="140" height="140" border="0" class="img-circle"> <h3>' + data['agencyName'] + ' <small>' + data['country'] + '</small></h3>   </center>               </div>                    <div class="card text-black bg-light">                        <h5> Agency Code :</h5><h6>'
//             data['agencyCode']
//             '</h6>                    <h5> Email :</h5><h6>' + data['email'] + '</h6>                    <h5> Contact :</h5><h6>' + data['contact'] + '</h6>                    <h5> Address :</h5><h6>' + data['address'] + '<br>' + data['city'] + data['state'] + data['country'] + data['pincode'] + '</h6>                      </div>        </div>                <div class="modal-footer">                   <button class="btn btn-danger" onclick="closeModal()">Close</button>              </div>            </div>       </div>   </div>'
//             $('#profilemodal').append(text)
//         }
//     })
// }

// function closemodal() {
//     $('myModal').modal('hide')
// }



$('#pancard').on(function(event) {
    var regExp = /[A-Z]{5}[0-9]{4}[A-Z]{1}/
    var txtpan = $(this).val();
    if (txtpan.length == 10) {
        if (txtpan.match(regExp)) {
            alert('PAN match found');
        } else {
            alert('Not a valid PAN number');
            event.preventDefault();
        }
    } else {
        alert('Please enter 10 digits for a valid PAN number');
        event.preventDefault();
    }

});


$('#adhaarcard').on(function() {
    var regex = /^\d{4}\s\d{4}\s\d{4}$/g
    if (!regex.test($("#txtPanNo").val())) {
        alert("Please Enter Valid Aadhar card No.");
    }
})

function profile(id) {
    $.ajax({
        url: "/v1/profile",
        method: 'get',
        dataType: 'json',
        data: { id: id },
        success: function(response) {
            var data = response.body
            console.log(data)
            $.showModal({
                title: 'Profile',
                body: '<div class="card text-white bg-dark">' +
                    '<center>' +
                    '<img src="' + data['agency_pic'] + ' "name="     aboutme " width="140 " height=" 140 " border="  0 " class=" img - circle "></a>' +
                    '<h3>' + data['agency_name'] + ' <small>' + data['country'] + '</small></h3> ' +
                    ' </center></div><div class="card text-black bg-light">' +
                    '<h5> Agency Code :</h5><h6>' + data['agency_code'] + '</h6>' +
                    '<h5> Email :</h5><h6>' + data['email'] + '</h6>' +
                    '<h5> Contact :</h5><h6>' + data['contact'] + '</h6>' +
                    '<h5> Address :</h5><h6>' + data['address'] + '<br>' + data['state'] + data['country '] + data[' pincode '] + ' </h6>' +
                    '</div></div>',
                footer: '<button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>',
                // onCreate: function(modal) {
                //     // create event handler for form submit and handle values
                //     $(modal.element).on("click", "button[type='submit']", function(event) {
                //         event.preventDefault()
                //         var $form = $(modal.element).find("form")
                //         $.showAlert({
                //             title: "Result",
                //             body: "<b>text:</b> " + $form.find("#text").val() + "<br/>" +
                //                 "<b>select:</b> " + $form.find("#select").val() + "<br/>" +
                //                 "<b>textarea:</b> " + $form.find("#textarea").val()
                //         })
                //         modal.hide()
                //     })
                // }
            })
        }
    });

}