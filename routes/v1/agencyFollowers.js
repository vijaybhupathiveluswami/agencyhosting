const models = require('../../models/init-models').initModels()

function routes (fastify, options, done) {
  const response = {}
  fastify.get('/agencyFollowers', async ({ query, headers }, reply) => {
    console.log(query)

    let agencyFollowers = []

    response.body = {}
    response.message = 'Success'
    response.status = 0

    switch (query.type) {
      case 'create':
        agencyFollowers = await models.agency.findAll({ raw: true })
        break
      case 'edit':
        agencyFollowers = await models.agency.findAll({ raw: true, where: { id: query.id } })
        break
    }

    const tempArray = []
    for (const element of agencyFollowers) {
      const tempJson = {
        id: element.id,
        name: element.agency_name,
        dreamliveID: element.dreamlive_id,
        contact: element.contact,
        email: element.email,
        nationalID: element.national_id,
        address: element.address,
        city: element.city,
        state: element.state,
        country: element.country,
        pincode: element.pincode,
        nationalIdType: element.id_type,
        nationalIdfront: element.national_id_pic,
        nationalIdback: element.nation_id_pic_back,
        selfie: element.selfie_pic,
        selfieNational: element.selfie_national_pic,
        userName: element.username,
        password: element.password,
        agencyName: element.agency_name,
        agencyCode: element.agency_code
      }
      tempArray.push(tempJson)
    }
    response.body.data = tempArray
    console.log(response)
    return response
  })

  fastify.post('/agencyFollowersDelete', async (req, reply) => {
    const body = req.body
    response.body = {}
    response.message = 'Deleted Successfully'
    response.status = 0
    console.log(body)
    console.log(response)
    // console
    await models.agency.destroy({
      where: {
        id: body.id
      }
    })
    console.log(response)
    return response
  })
  fastify.get('/profile', async ({ query }, reply) => {
    const response = {}
    response.body = {}
    response.message = 'Success'
    response.status = 0

    const dreamData = await models.agency.findAll({
      raw: true,
      where: {
        id: query.id
      }
    })
    response.body = dreamData[0]
    return response
  })
  fastify.get('/agencyFollowersValidate', async ({ query, headers }, reply) => {
    console.log(query)

    response.body = {}
    response.message = 'Success'
    response.status = 0
    const dreamData = await models.agency.findAll({
      raw: true,
      where: {
        dreamlive_id: query.dreamId
      }
    })
    if (dreamData.length > 0) {
      response.body.dreamId = true
    }
    const agency = await models.agency.findAll({
      raw: true,
      where: {
        agency_code: query.agencyCode
      }
    })
    if (agency.length > 0) {
      response.body.agency = true
    }
    const adhaar = await models.agency.findAll({
      raw: true,
      where: {
        adhaar_number: query.adhaar
      }
    })
    if (adhaar.length > 0) {
      response.body.adhaar = true
    }
    const pan = await models.agency.findAll({
      raw: true,
      where: {
        pan_number: query.pan
      }
    })
    if (pan.length > 0) {
      response.body.pan = true
    }

    console.log(response)
    return response
  })

  done()
}
module.exports = routes
module.exports.autoPrefix = '/v1'
