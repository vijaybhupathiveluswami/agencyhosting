// const { replayData } = require('../../common/response_data')
const models = require('../../models/init-models').initModels()
// const moment = require('moment')
// const { s3bucketBuffer } = require('../../common/s3bucket_buffer')
const fs = require('fs')
// const { webp } = require('../../common/image_verfiy_s3bucket')
const { verifyImage } = require('../../common/verify_image')
// const moment = require('moment')
const { webp } = require('../../common/webp')

function routes (fastify, options, done) {
  fastify.post('/updateHost', async (req, reply) => {
    const response = {}
    response.body = {}
    const body = req.body

    console.log(body.type)
    const tempJson = {
      dreamlive_id: body.dreamliveID,
      agency_name: body.agencyName,
      agency_code: body.agencyCode,
      email: body.email,
      contact: body.contact,
      address: body.address,
      city: body.city,
      state: body.state,
      country: body.country,
      pincode: body.pinCode,
      username: body.userName,
      pan_number: '',

      password: body.password,
      id_type: body.idType,
      adhaar_number: body.nationalId,

      agent_name: body.name
    }
    console.log(tempJson)
    let imageStart = ''
    let nationalBack = ''
    let selfiePics = ''
    let selfieNational = ''
    let agencyPic = ''

    if (body.nationalIdFront !== undefined) {
      imageStart = body.dreamliveID + '_1' + '_' + new Date().getTime() + '.png'
      fs.writeFile(imageStart, body.nationalIdFront[0].data, (err) => {
        if (err) return console.error(err)
        console.log('file saved to')
      })
      const responseData = await verifyImage(imageStart)

      const array = (responseData.body.label.text).split('\n')
      console.log(array)

      if (array.indexOf(body.nationalId) === -1) {
        fs.unlinkSync(imageStart, (err) => {
          if (err) {
            throw err
          }
          console.log('File is deleted.')
        })
        response.message = 'The national id and proof id are different front'
        return response
      }

      const urls = await webp(imageStart, body.dreamliveID, '/image', '/agencyHosting')
      tempJson.national_id_pic = urls
    }

    if (body.nationalIdBack !== undefined) {
      nationalBack = body.dreamliveID + '_2' + '_' + new Date().getTime() + '.png'
      fs.writeFile(nationalBack, body.nationalIdBack[0].data, (err) => {
        if (err) return console.error(err)
        console.log('file saved to')
      })
      const responseData = await verifyImage(nationalBack)

      const array = (responseData.body.label.text).split('\n')
      console.log(array)

      if (array.indexOf(body.nationalId) === -1) {
        fs.unlinkSync(nationalBack, (err) => {
          if (err) {
            throw err
          }
          console.log('File is deleted.')
        })
        response.message = 'The national id and proof id are different back'
        return response
      }
      const urls = await webp(nationalBack, body.dreamliveID, '/image', '/agencyHosting')
      tempJson.nation_id_pic_back = urls
    }

    if (body.selfiePic !== undefined) {
      selfiePics = body.dreamliveID + '_3' + '_' + new Date().getTime() + '.png'
      fs.writeFile(selfiePics, body.selfiePic[0].data, (err) => {
        if (err) return console.error(err)
        console.log('file saved to')
      })
      const urls = await webp(selfiePics, body.dreamliveID, '/image', '/agencyHosting')
      tempJson.selfie_pic = urls
    }
    if (body.agencyPic !== undefined) {
      agencyPic = body.dreamliveID + '_3' + '_' + new Date().getTime() + '.png'
      fs.writeFile(agencyPic, body.selfiePic[0].data, (err) => {
        if (err) return console.error(err)
        console.log('file saved to')
      })
      const urls = await webp(agencyPic, body.dreamliveID, '/image', '/agencyHosting')
      tempJson.selfie = urls
    }

    if (body.selfieNational !== undefined) {
      selfieNational = body.dreamliveID + '_4' + '_' + new Date().getTime() + '.png'
      fs.writeFile(selfieNational, body.nationalIdBack[0].data, (err) => {
        if (err) return console.error(err)
        console.log('file saved to')
      })
      const responseData = await verifyImage(selfieNational)

      const array = (responseData.body.label.text).split('\n')
      console.log(array)

      if (array.indexOf(body.nationalId) === -1) {
        fs.unlinkSync(selfieNational, (err) => {
          if (err) {
            throw err
          }
          console.log('File is deleted.')
        })
        response.message = 'The Id is different.'
        return response
      }
      const urls = await webp(selfieNational, body.dreamliveID, '/image', '/agencyHosting')
      tempJson.selfie_national_pic = urls
    }

    fs.unlinkSync(selfieNational, (err) => {
      if (err) {
        throw err
      }
      console.log('File is deleted.')
    })
    fs.unlinkSync(selfiePics, (err) => {
      if (err) {
        throw err
      }
      console.log('File is deleted.')
    })
    fs.unlinkSync(nationalBack, (err) => {
      if (err) {
        throw err
      }
      console.log('File is deleted.')
    })
    fs.unlinkSync(imageStart, (err) => {
      if (err) {
        throw err
      }
      console.log('File is deleted.')
    })
    fs.unlinkSync(agencyPic, (err) => {
      if (err) {
        throw err
      }
      console.log('File is deleted.')
    })

    if (body.type === 'edited') {
      models.agency.update(tempJson, { where: { id: body.id } })
      response.message = 'Successfully Updated'
    } else if (body.type === 'create') {
      models.agency.create(tempJson)
      response.message = 'Successfully Created'
    }
    response.body = tempJson
    return response
  })
  done()
}
module.exports = routes
module.exports.autoPrefix = '/v1'
