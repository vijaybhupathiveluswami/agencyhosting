function routes (fastify, options, done) {
  fastify.get('/', (req, reply) => {
    reply.view('/login')
  })

  fastify.get('/formmodel', (req, reply) => {
    reply.view('/formmodel')
  })

  fastify.get('/form2', (req, reply) => {
    reply.view('/form2')
  })
  fastify.get('/agencyFollowerss', (req, reply) => {
    reply.view('/agency_followers')
  })

  fastify.get('/hostFollowers', (req, reply) => {
    reply.view('/agency_host')
  })
  fastify.get('/profile', (req, reply) => {
    reply.view('/profile')
  })

  done()
}
module.exports = routes
module.exports.autoPrefix = '/'
