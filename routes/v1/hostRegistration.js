const models = require('../../models/init-models').initModels
// const moment = require('moment')
// const { s3bucketBuffer } = require('../../common/s3bucket_buffer')
// const fs = require('fs')
const { Op } = require('sequelize')
// const { verifyImage } = require('../../common/verify_image')

function routes (fastify, options, done) {
  const response = {}
  fastify.get('/hostRegistration', async (query, reply) => {
    console.log(query)

    let host = []

    response.body = {}
    response.message = 'Success'
    response.status = 0

    switch (query.type) {
      case 'create':
        host = await models.agencyHost.findAll({ raw: true })
        break
      case 'edit':
        host = await models.agencyHost.findAll({ raw: true, where: { id: query.id } })
        break
    }
    console.log('++++++++++++++++++')
    console.log(host)
    console.log('_________--------')

    const tempArray = []
    for (const element of host) {
      const tempJson = {
        id: element.id,
        name: element.name,
        dreamliveID: element.dreamlive_id,
        contact: element.contact,
        email: element.email,
        nationalId: element.national_id,
        address: element.address,
        city: element.city,
        state: element.state,
        country: element.country,
        paymentType: element.payment_type,
        nationalIdType: element.id_type,
        nationalIdfront: element.national_id_pic,
        nationalIdback: element.nation_id_pic_back,
        selfie: element.selfie_pic,
        selfieNational: element.selfie_national_pic,

        agencyName: element.agency_name,
        agencyCode: element.agency_code
      }
      tempArray.push(tempJson)
    }
    response.body.data = tempArray
    console.log(response)
    return response
  })

  fastify.post('/hostDelete', async (req, reply) => {
    const body = req.body
    response.body = {}
    response.message = 'Deleted Successfully'
    response.status = 0
    console.log(body)
    console.log(response)
    // console
    await models.agencyHost.destroy({
      where: {
        id: body.id
      }
    })
    console.log(response)
    return response
  })

  fastify.get('/agentSearch', async (req, reply) => {
    const tempJson = {
      agency_code: {
        [Op.like]: '%' + req.query.search + '%'
      }
    }
    const host = []
    console.log('}}}}{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}')
    const hostRegistration = await models.agency.findAll({
      raw: true,
      where: tempJson,
      attributes: [
        ['agency_code', 'id'],
        ['agency_name', 'text']
      ]
    })

    for (const element of hostRegistration) {
      const text = element.id + '(' + element.text + ')'
      const temp = {
        id: element.id,
        text: text
      }
      host.push(temp)
    }

    console.log(hostRegistration)
    console.log('}}}}{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}')

    return host
  })

  done()
}
module.exports = routes
module.exports.autoPrefix = '/v1'
