const models = require('../../models/init-models').initModels()

function routes (fastify, options, done) {
  fastify.get('/', async (req, reply) => {
    reply.view('/login', { loginPage: true })
  })

  fastify.post('/login', async (req, reply) => {
    const md5 = require('md5')
    const body = req.body
    const userName = body.username
    const password = md5(body.password)
    console.log(userName)
    console.log(password)
    const userData = await models.role.findAll({
      where: {
        name: userName,
        password: password
      },
      raw: true,
      nest: true
    })
    console.log(userData)
    if (userData.length > 0) {
      reply.view('/agency_followers')
    } else {
      reply.view('/login', { loginPage: true, loginError: 'Invalid username or password' })
    }
  })
  done()
}

module.exports = routes
module.exports.autoPrefix = '/v1'
